angular.module('administration')
// Set default value to an input/select field
.directive('ngInitial', function($parse) {
  return {
    restrict: "A",
    compile: function($element, $attrs) {
      var initialValue = $attrs.value || $element.val();
      return {
          pre: function($scope, $element, $attrs) {
              $parse($attrs.ngModel).assign($scope, initialValue);
          }
      }
    }
  }
})
// Directive to create group menue
.directive('menuitem', ['$document', function($document) {
  return {
    restrict: "A",
    link: function(scope, element, attr) {

      // get the elements
      var children = element.children();
      var link = $(children[0]);
      var list = $(children[1]);

      // add the event on the click on the header
      link.on('click', function(event) {
        if (!element.hasClass("active")) {
          element.addClass("active");
          list.addClass("in");
          var otherElements = element.siblings();
          otherElements.removeClass("active");
          otherElements.children("ul").removeClass("in");
        } else {
          element.removeClass("active");
          list.removeClass("in");
        }
      });

      // Add the arrow at the link
      link.append('<span class="fa arrow"></span>');

      // add the classes to the list
      list.addClass("nav nav-second-level collapse");

    }
  };
}])
// Display a confirm box before executing the action
.directive('confirm', function ($interpolate) {
  return {
    priority: 100,
    restrict: 'A',
    link: {
      pre: function (scope, element, attrs) {
        var msg;
        if(attrs.confirm)
          msg = $interpolate(attrs.confirm)(scope);
        else
          msg = "Are you sure?";

        element.bind('click', function (event) {
          if (!confirm(msg)) {
            event.stopImmediatePropagation();
            event.preventDefault;
          }
        });
      }
    }
  };
})
.directive("compileHtml", function($parse, $sce, $compile) {
  return {
    restrict: "A",
    link: function (scope, element, attributes) {

      var expression = $sce.parseAsHtml(attributes.compileHtml);

      var getResult = function () {
        return expression(scope);
      };

      scope.$watch(getResult, function (newValue) {
        var linker = $compile(newValue);
        element.html(linker(scope));
      });
    }
  }
})

/*
  Github: https://github.com/Mischi/angularjs-imageupload-directive
  Preview of image
*/
.directive('image', function($q) {
  'use strict'

  var URL = window.URL || window.webkitURL;

  var getResizeArea = function () {
    var resizeAreaId = 'fileupload-resize-area';

    var resizeArea = document.getElementById(resizeAreaId);

    if (!resizeArea) {
      resizeArea = document.createElement('canvas');
      resizeArea.id = resizeAreaId;
      resizeArea.style.visibility = 'hidden';
      document.body.appendChild(resizeArea);
    }

    return resizeArea;
  }

  var resizeImage = function (origImage, options) {
    var maxHeight = options.resizeMaxHeight || 300;
    var maxWidth = options.resizeMaxWidth || 250;
    var quality = options.resizeQuality || 0.7;
    var type = options.resizeType || 'image/jpg';

    var canvas = getResizeArea();

    var height = origImage.height;
    var width = origImage.width;

    // calculate the width and height, constraining the proportions
    if (width > height) {
      if (width > maxWidth) {
        height = Math.round(height *= maxWidth / width);
        width = maxWidth;
      }
    } else {
      if (height > maxHeight) {
        width = Math.round(width *= maxHeight / height);
        height = maxHeight;
      }
    }

    canvas.width = width;
    canvas.height = height;

    //draw image on canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(origImage, 0, 0, width, height);

    // get the data from canvas as 70% jpg (or specified type).
    return canvas.toDataURL(type, quality);
  };

  var createImage = function(url, callback) {
    var image = new Image();
    image.onload = function() {
      callback(image);
    };
    image.src = url;
  };

  var fileToDataURL = function (file) {
    var deferred = $q.defer();
    var reader = new FileReader();
    reader.onload = function (e) {
      deferred.resolve(e.target.result);
    };
    reader.readAsDataURL(file);
    return deferred.promise;
  };


  return {
    restrict: 'A',
    scope: {
      image: '=',
      resizeMaxHeight: '@?',
      resizeMaxWidth: '@?',
      resizeQuality: '@?',
      resizeType: '@?',
    },
    link: function postLink(scope, element, attrs, ctrl) {

      var doResizing = function(imageResult, callback) {
        createImage(imageResult.url, function(image) {
          var dataURL = resizeImage(image, scope);
          imageResult.resized = {
            dataURL: dataURL,
            type: dataURL.match(/:(.+\/.+);/)[1],
          };
          callback(imageResult);
        });
      };

      var applyScope = function(imageResult) {
        scope.$apply(function() {
          //console.log(imageResult);
          if(attrs.multiple)
            scope.image.push(imageResult);
          else
            scope.image = imageResult; 
        });
      };


      element.bind('change', function (evt) {
        //when multiple always return an array of images
        if(attrs.multiple)
          scope.image = [];

        var files = evt.target.files;
        for(var i = 0; i < files.length; i++) {
          //create a result object for each file in files
          var imageResult = {
            file: files[i],
            url: URL.createObjectURL(files[i])
          };

          fileToDataURL(files[i]).then(function (dataURL) {
            imageResult.dataURL = dataURL;
          });

          if(scope.resizeMaxHeight || scope.resizeMaxWidth) { //resize image
            doResizing(imageResult, function(imageResult) {
              applyScope(imageResult);
            });
          } else { //no resizing
            applyScope(imageResult);
          }
        }
      });
    }
  };
})
.directive("passwordVerify", function() {
  return {
    require: "ngModel",
    scope: {
      passwordVerify: '='
    },
    link: function(scope, element, attrs, ctrl) {
      scope.$watch(function() {
        var combined;

        if (scope.passwordVerify || ctrl.$viewValue) {
          combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
        }                    
        return combined;
      }, function(value) {
        if (value) {
          ctrl.$parsers.unshift(function(viewValue) {
          var origin = scope.passwordVerify;
          if (origin !== viewValue) {
            ctrl.$setValidity("passwordVerify", false);
            return undefined;
          } else {
            ctrl.$setValidity("passwordVerify", true);
            return viewValue;
          }
          });
        }
      });
    }
  };
});
