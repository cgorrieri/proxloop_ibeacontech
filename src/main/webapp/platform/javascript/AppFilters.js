angular.module('administration')
.filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
})
.filter('getByAttribute', function() {
  return function(input, attr, value) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (input[i][attr] == value) {
        return input[i];
      }
    }
    return null;
  }
})
.filter('getListByAttribute', function() {
  return function(input, attr, value) {
    var list = [];
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (input[i][attr] == value) {
        list.push(input[i]);
      }
    }
    return list;
  }
});