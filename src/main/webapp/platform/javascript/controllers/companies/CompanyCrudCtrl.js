angular.module('administration').controller('CompanyCrudCtrl',
  function ($scope,$rootScope,AlertFact,$state,$stateParams,companies,$modal) {
    $scope.displayLoading = false;
    $scope.edit = ($stateParams.edit == 1);
    $scope.add = false;
    $scope.objectId = $stateParams.id;
    $scope.object = {};

    $scope.init=function() {
      if($state.$current.name != 'company-add-one') {
        if($state.$current.name == 'companies-manage-mine') {
          $scope.objectId = $rootScope.user.companyCode;
        }
        $scope.displayLoading = true;
        $scope.retrieve();
      } else {
        $scope.add = true;
        $scope.object.loops = 1;
      }
    }

    $scope.retrieve = function() {
      companies.get($scope.objectId)
      .then(
        function(resp){
          $scope.object = resp.data;
          $scope.object_backup = angular.copy($scope.object);
          $scope.displayLoading = false;
        },
        function(error) {
          console.warn("TODO: error management on get company");
        });
      //});
    }

    $scope.insert = function() {      
      /*gapi.client.administration.awards.insert($scope.object).execute(function(resp){
        AlertFact.addAlert("success", "The award has been added successfully !");
        $state.go('awards-details-one', {id:resp.id, edit:0});
      });*/
    }

    $scope.update = function() {
      companies.update($scope.object)
      .then(
        function(resp){
          AlertFact.addAlert("success", "The company has been updated successfully !");
          if($state.$current.name == 'companies-manage-mine') {
            console.log(resp.data);
            $rootScope.$broadcast('company_updated', resp.data);
          }
          $scope.edit = false;
          $scope.init();
        });
    }

    $scope.reset = function() {
      angular.copy($scope.object_backup, $scope.object);
    }

    $scope.cancel = function() {
      $scope.edit = false;
      angular.copy($scope.object_backup, $scope.object);
    }

    $scope.callback = function(selectedItem) {
      $scope.galleryModal.$promise.then($scope.galleryModal.hide);
      $scope.object.logoUrl = selectedItem;
    }

    $scope.galleryModal = $modal({
        templateUrl: 'templates/gallery.modal.html',
        controller: 'ModalGalleryCtrl',
        resolve: {
          callback: function() {return $scope.callback;}
        },
        show:false
      });

    $scope.openGallery = function() {
      $scope.galleryModal.$promise.then($scope.galleryModal.show);
    }

    $scope.init();
});