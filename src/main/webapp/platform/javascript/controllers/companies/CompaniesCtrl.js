angular.module('administration').controller('CompaniesCtrl',
  function ($scope,$rootScope,cloudendpoint,AlertFact,companies) {
    // Hide all the app until the we know if the user is authorized
    $scope.companies = [];

    $scope.displayAddForm = false;

    $scope.retrieveCompanies = function() {
      companies.list().then(function(response) {
        $scope.companies = response;
      });
    }

    $scope.insertCompany = function() {
      newCompany = {
        code : $scope.companyCode,
        name : $scope.companyName
      }
      console.info('insertCompany: newCompany = ', newCompany);
      companies.insert(newCompany).then(
        function(resp){ // success
          $scope.retrieveCompanies();
        },
        function(error) { // error
          $scope.addCompanyErrors = [error.data];
        });
    }

    $scope.deleteCompany = function(companyCode) {
      companies.delete(companyCode).then(
        function(resp){ // success
          AlertFact.addAlert("success", "The company has been deleted successfully !");
          $scope.retrieveCompanies();
        },
        function(error) { // error
          $scope.deleteCompanyErrors = [error.data];
        });
    }

    $scope.retrieveCompanies();
});