angular.module('administration')
.controller('NotificationsCtrl',
function ($scope,$rootScope,$sce,cloudendpoint,AlertFact,$state,NotificationsServices) {
  $scope.displayAddForm = false;

  $scope.init=function() {
    $scope.retrieveNotifications();
  }

  $scope.retrieveNotifications = function() {
    NotificationsServices.listForCompany($rootScope.user.companyCode)
    .then(
      function(resp){
        $scope.notifications = resp;
      });
  };

  $scope.translateType = function(notificationType) {
    var type = '';
    var text = '';
    switch(notificationType) {
      case 'TemplateBasedWebView':
        type = 'success';
        text = 'Template';
      break;
      case 'CustomWebUrl':
        type = 'info';
        text = 'Custom URL';
        break;
    }
    return $sce.trustAsHtml('<span class="label label-'+type+'">'+text+'</span>');
  }

  $scope.editNotification = function(notificationId) {
    // Redirect to edit page
    $state.go('notifications-details-one', {id:notificationId, edit:1});
  }

  $scope.viewNotificationDetails = function(notificationId) {
    $state.go('notifications-details-one', {id:notificationId, edit:0});
  }

  $scope.deleteNotification = function(notificationId) {
    NotificationsServices.delete(notificationId)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The notification has been deleted successfully !");
        $scope.retrieveNotifications();
      },
      function(error){
        //AlertFact.addAlert("error", decodeErrors(resp.error).message);
      });
  }

  $scope.init();
});