angular.module('administration')
.controller('NotificationCtrl',
function ($scope,$rootScope,AlertFact,$state,$stateParams,FacilitiesServices,NotificationsServices,$modal) {

  $scope.init=function() {
    FacilitiesServices.listForCompany($rootScope.user.companyCode)
    .then(
      function(resp){
        $scope.facilities = resp;
        if($state.$current.name != 'notifications-add-one') {
          $scope.edit = ($stateParams.edit == 1);
          $scope.objectId = $stateParams.id;
          $scope.displayLoading = true;
          $scope.retrieve();
        } else {
          $scope.add = true;
          $scope.object = {};
        }
      });
  }

  $scope.retrieve = function() {
    NotificationsServices.get($scope.objectId)
    .then(
      function(resp){
        $scope.displayLoading = false;
        // get the object from the response
        $scope.object = resp.data;
        // facility id is set as string to be interpreted in the HTML as a value of the select
        $scope.object.facilityId = $scope.object.facilityId ? ""+$scope.object.facilityId:undefined;
        // backup of the object in case of reset
        $scope.object_backup = angular.copy(resp.data);
        // Set the scroll in the phone preview
        // TODO: to fix
        $scope.iscroll = new IScroll('#phone-preview-container');
        $scope.iscroll.refresh();
      });
  }

  $scope.insert = function() {
    var notif = angular.copy($scope.object);
    notif.companyCode = $rootScope.user.companyCode;
    notif.notificationType = NotificationType.IMBEDDED;

    NotificationsServices.insert(notif)
    .then(
      function(resp){
        $scope.notificationErrors = [];
        AlertFact.addAlert("success", "The notifications has been added successfully !");
        $state.go('notifications-details-one', {id:resp.data.id, edit:0});
      },
      function(error) {
        $scope.notificationErrors = error.data.errors;
      });
  }

  $scope.update = function() {
    var notif = angular.copy($scope.object);
    // The facility id must be a number
    if(notif.facilityId) {
      notif.facilityId = parseInt(notif.facilityId);
    }
    NotificationsServices.update(notif)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The notification has been updated successfully !");
        $scope.edit = false;
        $scope.notificationErrors = [];
        $scope.displayLoading = true;
        $scope.retrieve();
      },
      function(error) {
        $scope.notificationErrors = error.data.errors;
      });
  }

  $scope.reset = function() {
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.cancel = function() {
    $scope.edit = false;
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.submit = function() {
    if($scope.edit) $scope.update();
    if($scope.add) $scope.insert();
    $scope.notificationForm.$setPristine();
  }

  $scope.$watch('object.shortDescription', function(d) {
    console.log('refresh');
    $scope.iscroll.refresh();
  })


  $scope.callback = function(selectedItem) {
    $scope.galleryModal.$promise.then($scope.galleryModal.hide);
    $scope.object.customWebUrl = selectedItem;
  }

  $scope.galleryModal = $modal({
      templateUrl: 'templates/gallery.modal.html',
      controller: 'ModalGalleryCtrl',
      resolve: {
        callback: function() {return $scope.callback;}
      },
      show:false
    });

  $scope.openGallery = function() {
    $scope.galleryModal.$promise.then($scope.galleryModal.show);
  }

  $scope.init();
});