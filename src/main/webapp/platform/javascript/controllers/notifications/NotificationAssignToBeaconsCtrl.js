// TEMPLATE: templates/notifications/notification_assign_to_beacons.html

angular.module('administration')
.controller('NotificationAssignToBeaconsCtrl',
function ($scope,$rootScope,$sce,$filter,AlertFact,$state,ExecuteParallel,NotificationsServices,BeaconsServices) {
  $scope.notifications = [];
  $scope.beacons = [];

  $scope.displayLoading = true;
  $scope.canSave = false;

  $scope.init=function() {
    // We want to retrieve both the notifications and the beacons before doing anything
    ExecuteParallel([$scope.retrieveNotifications,$scope.retrieveBeacons]).then(function() {
      $scope.displayLoading = false;
    });
  };

  $scope.retrieveNotifications = function(defer) {
    console.error("HERE");
    //cloudendpoint.doCall().then(function() {
      NotificationsServices.listForCompany($rootScope.user.companyCode)
      .then(
        function(resp){
          // Set url as trusted to display it in the iframe
          $scope.notifications = resp.map(function(notif) {
            /*var url = "";
           
            url = notif.templateImplement.displayUrl;
            // } 
            // else if (notif.notificationType == NotificationType.CUSTOM_URL) {
            //   url = notif.customWebUrl;
            // }
            notif.trustedUrl = $sce.trustAsResourceUrl(url);
*/
            return notif;
          });
          if(defer) defer.resolve();
        });
    //});
  };

  $scope.retrieveBeacons = function(defer) {
    BeaconsServices.list({companyCode:$rootScope.user.companyCode})
    .then(
      function(resp){
        $scope.beacons = resp.data;
        if(defer) defer.resolve();
      });
  };


  $scope.assign = function() {
    var selectedBeacons = $scope.getSelectedBeaconsId();
    NotificationsServices.assignToBeacons($scope.notification.id, selectedBeacons)
    .then(
      function(resp) {
        // Reset the form
        AlertFact.addAlert("success", "The association has been added successfully !");
        $scope.notification = undefined;
        $scope.isSelectAll = true;
        $scope.isSelectAll = false;
      },
      function(error) {
        // TODO
      });
  }


  // BEACON SELECTION MANAGEMENT
  $scope.$watch('isSelectAll', function() {
    for (beacon of $scope.beacons) {
      beacon.selected = $scope.isSelectAll;
    }
    $scope.checkCanSave();
  });

  // Get the list of beacons id that are selected 
  $scope.getSelectedBeaconsId = function () {
    var selectedBeacons = [];
    for (beacon of $scope.beacons) {
      if(beacon.selected)
        selectedBeacons.push(beacon.id);
    }
    return selectedBeacons;
  }

  // Check if all the conditions are ok to save the assignement
  $scope.checkCanSave = function () {
    $scope.canSave = notification && $scope.getSelectedBeaconsId().length > 0;
  }

  $scope.init();
});