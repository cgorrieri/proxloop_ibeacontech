angular.module('administration').controller('RegisterInvitation',
  function ($scope,$rootScope,$log,AlertFact,$http,$state,$stateParams,companies,AuthenticationService) {
    
  $scope.registerObject = {};


  $http.get(appLocation +"/public/invitations/"+$stateParams.code+"/"+$stateParams.email)
    .success(function(result) {
      $log.debug("Get invitation:", result);
      var invitation = result;
      // Setup variables for a better understanding in the template
      invitation.isCompanyCreation = invitation.action == "CREATE_COMPANY";
      invitation.isFacilityCreation = invitation.action == "CREATE_FACILITY";
      invitation.isUserCreation = invitation.action == "CREATE_USER";

      $log.debug(invitation);
      $scope.registerObject.email = invitation.email;
      $scope.registerObject.receiverFirstName = invitation.receiverFirstName;
      if(invitation.scope) {
        $scope.company = invitation.scope.company;

        if(invitation.scope.userPermissions) {
          $log.debug("has permissions");
          var permissions = invitation.scope.userPermissions;
          var roles = {};

          if(permissions["COMPANY_ADMIN"])
            roles.companyRole = "admin";
          else if(permissions["COMPANY_EDITOR"])
            roles.companyRole = "editor";
          else if(permissions["COMPANY_VIEWER"])
            roles.companyRole = "viewer";
          else roles.companyRole = "none";

          if(permissions["COMPANY_NOTIFICATION_ADMIN"])
            roles.companyNotificationRole = "admin";
          else if(permissions["COMPANY_NOTIFICATION_EDITOR"])
            roles.companyNotificationRole = "editor";
          else if(permissions["COMPANY_NOTIFICATION_VIEWER"])
            roles.companyNotificationRole = "viewer";
          else roles.companyNotificationRole = "none";

          if(permissions["FACILITY_ADMIN"])
            roles.facilityRole = "admin";
          else if(permissions["FACILITY_EDITOR"])
            roles.facilityRole = "editor";
          else if(permissions["FACILITY_VIEWER"])
            roles.facilityRole = "viewer";
          else roles.facilityRole = "none";

          if(permissions["FACILITY_NOTIFICATION_ADMIN"])
            roles.facilityNotificationRole = "admin";
          else if(permissions["FACILITY_NOTIFICATION_EDITOR"])
            roles.facilityNotificationRole = "editor";
          else if(permissions["FACILITY_NOTIFICATION_VIEWER"])
            roles.facilityNotificationRole = "viewer";
          else roles.facilityNotificationRole = "none";

          if(permissions["BEACON_ADMIN"])
            roles.beaconRole = "admin";
          else roles.beaconRole = "none";

          if(permissions["USER_ADMIN"])
            roles.userRole = "admin";
          else roles.userRole = "none";

          if(permissions["INVITATION_ADMIN"])
            roles.invitationRole = "admin";
          else roles.invitationRole = "none";


          $scope.roles = roles;
        }
      }

      $scope.invitation = invitation;
    })
	  .error(function(error) {
      $scope.errors = error.errors;
	  });


  $scope.register = function() {
    $http.post(appLocation +"/public/fulfill-invitation/"+$stateParams.code, $scope.registerObject)
    .success(function(result) {
      $log.debug("Post invitation:", result);
      AuthenticationService.login($scope.registerObject.username, $scope.registerObject.password)
      .then(
        function(response) {
          $state.go('home');
        },
        function(error) {
          // TODO treat errors
        });
    })
    .error(function(error) {
      $scope.errors = error.errors;
    });
  }

});