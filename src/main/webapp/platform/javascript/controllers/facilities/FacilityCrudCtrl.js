angular.module('administration')
.controller('FacilityCrudCtrl',
function ($scope,$rootScope,AlertFact,$state,$stateParams,FacilitiesServices,$modal) {
  $scope.displayLoading = false;
  $scope.edit = ($stateParams.edit == 1);
  $scope.add = false;
  $scope.objectId = $stateParams.id;
  $scope.object = {};

  var displayError = function(error) {
    if(error.data.code && error.data.code == 9999) {
      error.data.message = "An internal error occured !";
    }
    $scope.errors = [error.data];
  }

  $scope.init=function() {
    if($state.$current.name != 'facilities-add-one') {
      $scope.displayLoading = true;
      $scope.retrieve();
    } else {
      $scope.add = true;
    }
  }

  $scope.retrieve = function() {
    $rootScope
    FacilitiesServices.get($scope.objectId)
    .then(
      function(resp){
        $scope.object = resp.data;
        $scope.object_backup = angular.copy($scope.object);
        $scope.displayLoading = false;
      },
      function(error) {
        $scope.displayLoading = false;
        console.warn("TODO: error management on get company");
      });
    //});
  }

  $scope.insert = function() {
    var facility = angular.copy($scope.object);
    facility.companyCode = $rootScope.user.companyCode;
    console.log("To insert ",facility);
    FacilitiesServices.insert(facility)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The award has been added successfully !");
        $state.go('facilities-details-one', {id:resp.data.id, edit:0});
      }, displayError);
  }

  $scope.update = function() {
    FacilitiesServices.update($scope.object)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The facility has been updated successfully !");
        $scope.edit = false;
        $scope.init();
      }, displayError);
  }

  $scope.reset = function() {
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.cancel = function() {
    $scope.edit = false;
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.callback = function(selectedItem) {
    $scope.galleryModal.$promise.then($scope.galleryModal.hide);
    $scope.object.imageUrl = selectedItem;
  }

  $scope.galleryModal = $modal({
      templateUrl: 'templates/gallery.modal.html',
      controller: 'ModalGalleryCtrl',
      resolve: {
        callback: function() {return $scope.callback;}
      },
      show:false
    });

  $scope.openGallery = function() {
    $scope.galleryModal.$promise.then($scope.galleryModal.show);
  }

  $scope.init();
});