angular.module('administration')
.controller('FacilitiesCtrl',
function ($scope,$rootScope,AlertFact,$state,FacilitiesServices) {
  // Hide all the app until the we know if the user is authorized
  $scope.facilities = [];

  $scope.displayAddForm = false;

  $scope.init=function() {
    $scope.retrieveFacilities();
  }

  $scope.retrieveFacilities = function() {
    FacilitiesServices.listForCompany($rootScope.user.companyCode)
    .then(
      function(resp){
        $scope.facilities = resp;
      });
  };

  $scope.edit = function(id) {
    // Redirect to edit page
    $state.go('facilities-details-one', {id:id, edit:1});
  }

  $scope.delete = function(id) {
    FacilitiesServices.delete(id)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The facility has been deleted successfully !");
        $scope.retrieveFacilities();
      });
  }

  $scope.init();
});