// TEMPLATE: templates/notifications/notification_crud.html

angular.module('administration').controller('NotificationCtrl',
  function ($scope,$rootScope,$sce,cloudendpoint,AlertFact,$state,$stateParams,$timeout,$http,$q,$filter) {
    $scope.displayLoading = false;
    $scope.edit = ($stateParams.edit == 1);
    $scope.add = false;
    $scope.objectId = $stateParams.id;

    $scope.templatesSelect = [
      {value: NotificationType.TEMPALTE, text: "Template: pre-existing template to create your notification"},
      {value: NotificationType.CUSTOM_URL, text: "Custom URL: link to an existing web page"}
    ];
    console.log("HERE");
    console.log($scope.templatesSelect);

    $scope.initgapi=function() {
      cloudendpoint.init().then(
        function(){
          /* SUCCESS */
          $timeout(function() {
            $scope.retrieveTemplates( function() {
              if($state.$current.name != 'notifications-add-one') {
                $scope.displayLoading = true;
                $scope.retrieve();
              } else {
                $scope.add = true;
                $scope.$apply();
              }
            })
          }, 0);
      },
      function(){
        /* ERROR */
        // TODO: Display special page or retry 
        alert('error init cloud endpoint api')
      });
    }

    $scope.retrieve = function() {
      //cloudendpoint.doCall().then(function() {
        //var resp = {"id":"5668600916475904","name":"Notif 1","shortDescription":"Message 1","notificationType":"TemplateBasedWebView","webView":{"htmlContent":"<div class=\"template\" id=\"template-3\" ng-show=\"template==3\" style=\"color: rgb(1, 46, 86); background-color: rgb(0, 128, 151);\">\n                      <div class=\"top-container secondary-container\" style=\"background-color: rgb(120, 162, 47);\">\n                          <img class=\"image\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEAYABgAAD/4RKGRXhpZgAATU0AKgAAAAgA…qoWgaiqKileBlZfGYJTHykh1GxHOo+8gYVje0jnjbWHRSDXyIr5Y/Z166aRnCgZH+rh1//9k=\">\n                      </div>\n                      <div class=\"middle-container\">\n                          <h1 class=\"ng-binding\">Helloo</h1>\n                      </div>\n                      <div class=\"bottom-container secondary-container\" style=\"background-color: rgb(120, 162, 47);\">\n                          <h2 class=\"ng-binding\">Salute</h2>\n                      </div>\n                      <div id=\"call-to-action-button\">\n                          <a href=\"http://www.hoteldragomanni.com/imgoffers/free_wi_fi_spot.gif\" style=\"color: rgb(224, 216, 234); border-color: rgb(224, 216, 234);\">Button text</a>\n                      </div>\n                  </div>","cssUrl":"https://pelagic-region-637.appspot.com/web/css/interface_templates.css"},"kind":"administration#notificationsItem","etag":"\"684jx-jVc7lmjTfMdT-8-P5LQm8/_wQUZICCmzjToDbu5qUIlDDSXRA\"","result":{"id":"5668600916475904","name":"Notif 1","shortDescription":"Message 1","notificationType":"TemplateBasedWebView","webView":{"htmlContent":"<div class=\"template\" id=\"template-3\" ng-show=\"template==3\" style=\"color: rgb(1, 46, 86); background-color: rgb(0, 128, 151);\">\n                      <div class=\"top-container secondary-container\" style=\"background-color: rgb(120, 162, 47);\">\n                          <img class=\"image\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEAYABgAAD/4RKGRXhpZgAATU0AKgAAAAgA…qoWgaiqKileBlZfGYJTHykh1GxHOo+8gYVje0jnjbWHRSDXyIr5Y/Z166aRnCgZH+rh1//9k=\">\n                      </div>\n                      <div class=\"middle-container\">\n                          <h1 class=\"ng-binding\">Helloo</h1>\n                      </div>\n                      <div class=\"bottom-container secondary-container\" style=\"background-color: rgb(120, 162, 47);\">\n                          <h2 class=\"ng-binding\">Salute</h2>\n                      </div>\n                      <div id=\"call-to-action-button\">\n                          <a href=\"http://www.hoteldragomanni.com/imgoffers/free_wi_fi_spot.gif\" style=\"color: rgb(224, 216, 234); border-color: rgb(224, 216, 234);\">Button text</a>\n                      </div>\n                  </div>","cssUrl":"https://pelagic-region-637.appspot.com/web/css/interface_templates.css"},"kind":"administration#notificationsItem","etag":"\"684jx-jVc7lmjTfMdT-8-P5LQm8/_wQUZICCmzjToDbu5qUIlDDSXRA\""}};
        gapi.client.administration.notifications.get({id:$scope.objectId}).execute(function(resp){
          $scope.object = resp;
          $scope.object_backup = JSON.parse(JSON.stringify( resp ));
          $scope.displayLoading = false;

          // If the notification is base on template
          // We need to match the values
          if($scope.object.notificationType == NotificationType.TEMPALTE) {
            // Select the template associated to this notification
            $scope.template = $filter('getById')($scope.templates, $scope.object.templateImplement.templateId);
            // Populate the attribute of the template
            for(attribute of $scope.object.templateImplement.values) {
              // Get the template attribute to have the type
              templateAttr = $filter('getByAttribute')($scope.template.attributes, 'attribute', attribute.name);
              // if it is an image the value goes in the url sub attribute
              if(templateAttr.type == "image") {
                $scope.template[attribute.name] = {url: attribute.value};
              } else {
                $scope.template[attribute.name] = attribute.value;
              }
            }
          }
          $scope.$apply();
        });
      //});
    }

    $scope.insert = function() {
      console.log('insert');
      // Copy object to modify it
      var notif = JSON.parse(JSON.stringify( $scope.object ));
      notif.templateImplement = {templateId: $scope.template.id, values: []};

      // temp object for images
      var imagesAttributes = [];

      // Copy each attribute in the object except the images
      for (var attribute of $scope.template.attributes) {
        var attrObject = {name:attribute.attribute, value:$scope.template[attribute.attribute]};
        if(attribute.type == 'image') {
          imagesAttributes.push(attrObject);
        } else {
          notif.templateImplement.values.push(attrObject);
        }
      }
      
      // Insert first without the images to check if the insert is correct
      gapi.client.administration.notifications.insert(notif).execute(function(resp){
        notif = resp;
        // Upload all images
        var promises = imagesAttributes.map( function (image) {
          var deffered = $q.defer();

          $scope.uploadImage(image.value.file, 
            function(data){
              notif.templateImplement.values.push({name:image.name, value:data});
              deffered.resolve(data);
            },
            function(error){
              console.error("Rollback to do because failure in upload images");
              deffered.reject();
            }
          );
          return deffered.promise;
        });
        
        // Wait for all uploads to be done
        $q.all(promises).then(function(datas) {
          // Update the notification by adding the pictures to the template attributes
          gapi.client.administration.notifications.update(notif).execute(function(resp){
            $state.go('notifications-details-one', {id:resp.id, edit:0});
          });
        })
      });
    }

    $scope.reset = function() {
      $scope.object = $scope.object_backup;
    }

    $scope.cancel = function() {
      $scope.edit = false;
      $scope.object = $scope.object_backup;
    }

    $scope.trustedCustomUrl = undefined;
    $scope.previewCustomUrl = function() {
      $scope.trustedCustomUrl = $sce.trustAsResourceUrl($scope.object.customWebUrl);
    }

    // TEMPLATE MANAGEMENT
    $scope.retrieveTemplates = function(success) {
      gapi.client.administration.templates.list().execute(function(resp){
        $scope.templates = resp.items.map(function (template) {
          template.htmlContent = $sce.trustAsHtml(template.htmlContent);
          return template;
        });
        console.log($scope.templates);
        $scope.$apply();
        success();
      });
    }

    $scope.uploadImage = function(file, success, error) {
      var fd = new FormData();
      //Take the first selected file
      fd.append("file", file);

      $http.post(appLocation+'/serve', fd, {
        headers: {"Content-type": undefined},
        transformRequest: angular.identity
      }).success(success).error(error);
    }


    $scope.initgapi();
});