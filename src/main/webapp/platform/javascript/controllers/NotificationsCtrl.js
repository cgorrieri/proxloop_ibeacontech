// TEMPLATE: templates/notifications/notifications.html

angular.module('administration').controller('NotificationsCtrl',['$scope','$sce','cloudendpoint','AlertFact','$state',
  function ($scope,$sce,cloudendpoint,AlertFact,$state) {
    $scope.notifications = [];


    $scope.displayAddForm = false;

    $scope.initgapi=function() {
      cloudendpoint.init().then(
        function(){
          /* SUCCESS */
          $scope.retrieveNotifications();
      },
      function(){
        /* ERROR */
        // TODO: Display special page or retry 
        alert('error init cloud endpoint api')
      });
    }

    $scope.retrieveNotifications = function() {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.notifications.list().execute(function(resp){
          $scope.notifications = resp.items;
          $scope.$apply();
        });
      //});
    };

    $scope.translateType = function(notificationType) {
      var type = '';
      var text = '';
      switch(notificationType) {
        case 'TemplateBasedWebView':
          type = 'success';
          text = 'Template';
        break;
        case 'CustomWebUrl':
          type = 'info';
          text = 'Custom URL';
          break;
      }
      return $sce.trustAsHtml('<span class="label label-'+type+'">'+text+'</span>');
    }

    $scope.editNotification = function(notificationId) {
      // Redirect to edit page
      $state.go('notifications-details-one', {id:notificationId, edit:1});
    }

    $scope.viewNotificationDetails = function(notificationId) {
      $state.go('notifications-details-one', {id:notificationId, edit:0});
    }

    $scope.deleteNotification = function(notificationId) {
      gapi.client.administration.notifications.delete({id:notificationId}).execute(function(resp){
        // display the errors if any
        if(resp.error != undefined) {
          // Currently only one error is returned
          AlertFact.addAlert("error", decodeErrors(resp.error).message);
          $scope.$apply();
        }
        // Close the add form, reset the errors and re retrieve the companies
        else {
          AlertFact.addAlert("success", "The facility has been deleted successfully !");
          $scope.retrieveNotifications();
        }
      });
    }

    $scope.initgapi();
}]);