angular.module('administration')
.controller('LeftBarreCtrl',
function($scope,$rootScope,companies) {

	$rootScope.$on('company_updated', function(event, company) {
		console.log(company);
		$scope.company = company;
	});

	var init = function() {
		companies.get($rootScope.user.companyCode)
		.then(function(response) {
			$scope.company = response.data;
		})
	}
	init();
});