angular.module('administration')
.controller('BeaconsStatisticsCtrl',
function ($scope,$rootScope,$state,AlertFact,BeaconsServices,StatisticsServices) {
  $scope.init=function() {
    $scope.beacons = [];

    // init dates
    var endDate = moment();
    endDate.minutes(0).seconds(0).add(1, 'hours');
    $scope.endDate = endDate.toDate();
    var startDate = moment($scope.endDate);
    startDate.subtract(1, 'days');
    $scope.startDate = startDate.toDate();

    $scope.defaultData = [{date:$scope.startDate,visits:0},{date:$scope.endDate,visits:0}];
    $scope.retrieveBeacons();
    $scope.initGraph();
  }

  $scope.retrieveBeacons = function() {
    BeaconsServices.list({companyCode:$rootScope.user.companyCode})
    .then(
      function(resp){
        $scope.beacons = resp.data;
      });
  };

  // Get the list of beacons id that are selected 
  $scope.getSelectedBeaconsId = function () {
    var selectedBeacons = [];
    for (beacon of $scope.beacons) {
      if(beacon.selected)
        selectedBeacons.push(beacon.id);
    }
    return selectedBeacons;
  }

  var transformData = function(data) {
    if(data == undefined || data.length == 0) return;
    var formatedData = [];
    // If first data is more than 10s far from the begin date
    if(($scope.startDate.getTime()/1000) < (data[0].beginTime - 10)) {
      formatedData.push({date:$scope.startDate,visits:0},{date:new Date(data[0].beginTime*1000),visits:0});
    }
    // Build kind of bars for the data
    for(var i = 0; i < data.length; i++) {
      var d = data[i];   
      if(i>0) {
        var previous = data[i-1];
        // If there is a gap of at least 10 seconds
        if(d.beginTime > (previous.endTime + 10)) {
          var beginDate = new Date((previous.endTime) * 1000);
          var endDate = new Date((d.beginTime) * 1000);
          formatedData.push({date:beginDate,visits:0},{date:endDate,visits:0});
        }
      }
      var beginDate = new Date(d.beginTime * 1000);
      var endDate = new Date(d.endTime * 1000);
      formatedData.push({date:beginDate,visits:d.hits},{date:endDate,visits:d.hits});
    }
    // If last data is more than 10s before the end date
    if(($scope.endDate.getTime()/1000) > (data[data.length-1].endTime + 10)) {
      formatedData.push({date:new Date(data[data.length-1].endTime*1000),visits:0},{date:$scope.endDate,visits:0});
    }
    return formatedData;
  }

  $scope.displayStats = function() {
    var selectedBeaconsId = $scope.getSelectedBeaconsId();
    var startDate = moment($scope.startDate).format();
    var endDate = moment($scope.endDate).format();
    StatisticsServices.getForBeacon(selectedBeaconsId[0], startDate, endDate)
    .then(
      function(response) {
        $scope.updateGraphData(transformData(response.data));
      },
      function(error) {
        $scope.updateGraphData($scope.defaultData);
      });
  }

  $scope.initGraph = function() {
    $scope.chart = AmCharts.makeChart("chartdiv", {
      "type": "serial",
      "theme": "light",
      "marginRight": 80,
      "dataProvider": $scope.defaultData,
      "valueAxes": [{
        "position": "left",
        "title": "Global beacon access" 
      }],
      "graphs": [{
        "id": "g1",
        "fillAlphas": 0.4,
        "valueField": "visits",
        "balloonText": "<div style='margin:5px; font-size:19px;'>Visits:<b>[[value]]</b></div>"
      }],
      "chartScrollbar": {
        "graph": "g1",
        "scrollbarHeight": 80,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount": true,
        "color": "#AAAAAA"
      },
      "chartCursor": {
        "categoryBalloonDateFormat": "JJ:NN, DD MMMM",
        "cursorPosition": "mouse"
      },
      "categoryField": "date",
      "categoryAxis": {
        "minPeriod": "mm",
        "parseDates": true
      },
      "export": {
        "enabled": true
      }
    });
  }

  $scope.updateGraphData = function(data) {
    $scope.chart.dataProvider = data;
    $scope.chart.validateData();
  }

  $scope.init();
});