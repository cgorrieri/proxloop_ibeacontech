angular.module('administration')
.controller('BeaconsCtrl',
function ($scope,$rootScope,$state,AlertFact,BeaconsServices) {
  $scope.beacons = [];

  $scope.displayAddForm = false;

  $scope.init=function() {
    $scope.retrieveBeacons();
  }

  $scope.retrieveBeacons = function() {
    BeaconsServices.list({companyCode:$rootScope.user.companyCode})
    .then(
      function(resp){
        $scope.beacons = resp.data;
      });
  };

  $scope.getBatteryLevelType = function(batteryLevel) {
    if(batteryLevel < 20) 
      return 'danger';
    else if (batteryLevel < 50)
      return 'warning';
    else return 'success';
  }

  $scope.edit = function(beaconId) {
    // Redirect to edit page
    $state.go('beacons-details-one', {id:beaconId, edit:1});
  }

  $scope.details = function(beaconId) {
    $state.go('beacons-details-one', {id:beaconId, edit:0});
  }

  $scope.delete = function(beaconId) {
    BeaconsServices.delete(beaconId)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The beacon has been deleted successfully !");
        $scope.retrieveBeacons();
      },
      function(error){
        //AlertFact.addAlert("error", decodeErrors(resp.error).message);
      });
  }

  $scope.init();
});