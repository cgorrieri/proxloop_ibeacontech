angular.module('administration')
.controller('BeaconsAssignNotificationsCtrl',
function ($scope,$rootScope,$sce,$filter,AlertFact,$state,ExecuteParallel,NotificationsServices,BeaconsServices) {
  $scope.notifications = [];
  $scope.beacons = [];
  $scope.startDate = new Date();
  $scope.endDate = new Date();

  $scope.displayLoading = true;
  $scope.canSave = false;

  $scope.init=function() {
    // We want to retrieve both the notifications and the beacons before doing anything
    ExecuteParallel([$scope.retrieveNotifications,$scope.retrieveBeacons]).then(function() {
      $scope.displayLoading = false;
    });
  };

  $scope.retrieveNotifications = function(defer) {
    //cloudendpoint.doCall().then(function() {
      NotificationsServices.listForCompany($rootScope.user.companyCode)
      .then(
        function(resp){
          // Set url as trusted to display it in the iframe
          $scope.notifications = resp.map(function(notif) {
            /*var url = "";
           
            url = notif.templateImplement.displayUrl;
            // } 
            // else if (notif.notificationType == NotificationType.CUSTOM_URL) {
            //   url = notif.customWebUrl;
            // }
            notif.trustedUrl = $sce.trustAsResourceUrl(url);
*/
            return notif;
          });
          if(defer) defer.resolve();
        });
    //});
  };

  $scope.retrieveBeacons = function(defer) {
    BeaconsServices.list({companyCode:$rootScope.user.companyCode})
    .then(
      function(resp){
        $scope.beacons = resp.data;
        if(defer) defer.resolve();
      });
  };

  var sortByStartDate = function() {
    $scope.notificationsAssigned.sort(function(a, b) {
      return a.startDate.localeCompare(b.startDate);
    });
  }

  $scope.$watch('beacon', function(){
    $scope.getAssignments();
  });

  // Get assignments for the current beacon in the cloud
  $scope.getAssignments = function() {
    BeaconsServices.getNotificationsAssignments($scope.beacon.id)
    .then(
      function(resp){
        var assignements = angular.copy(resp.data);
        $scope.notificationsAssigned = assignements.map(function(assignment) {
          var notif = $filter('getById')($scope.notifications, assignment.notificationId);
          assignment.notificationName = notif.name;
          return assignment;
        });
        sortByStartDate();
      });
  }
  // Update assignments for the current beacon in the cloud
  $scope.updateAssignments = function() {
    BeaconsServices.updateNotificationsAssignments($scope.beacon.id, $scope.notificationsAssigned)
    .then(
      function(resp) {
        AlertFact.addAlert("success", "Notifications assignment has been done successfully !");
        $scope.getAssignments();
      },
      function(error) {
        AlertFact.addAlert("error", "An error occured !");
      });
  }

  // Add the current assignment in the list
  $scope.addAssignment = function() {
    $scope.notificationsAssigned.push({
      notificationId: $scope.notification.id,
      notificationName: $scope.notification.name,
      startDate: moment($scope.startDate).format(),
      endDate: moment($scope.endDate).format()
    });
    sortByStartDate();
  }


  $scope.assign = function() {
    var selectedBeacons = $scope.getSelectedBeaconsId();
    NotificationsServices.assignToBeacons($scope.notification.id, selectedBeacons)
    .then(
      function(resp) {
        // Reset the form
        AlertFact.addAlert("success", "The association has been added successfully !");
        $scope.notification = undefined;
        $scope.isSelectAll = true;
        $scope.isSelectAll = false;
      },
      function(error) {
        // TODO
      });
  }


  // BEACON SELECTION MANAGEMENT
  $scope.$watch('isSelectAll', function() {
    for (beacon of $scope.beacons) {
      beacon.selected = $scope.isSelectAll;
    }
    $scope.checkCanSave();
  });

  // Check if all the conditions are ok to save the assignement
  $scope.checkCanSave = function () {
    $scope.canSave = false;
  }

  $scope.init();
});