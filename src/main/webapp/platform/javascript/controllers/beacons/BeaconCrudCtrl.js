angular.module('administration')
.controller('BeaconCrudCtrl',
function ($scope,$rootScope,AlertFact,$state,$stateParams,FacilitiesServices,BeaconsServices) {
  $scope.displayLoading = false;
  $scope.edit = ($stateParams.edit == 1);
  $scope.add = false;
  $scope.objectId = $stateParams.id;
  $scope.object = {};

  $scope.init=function() {
    FacilitiesServices.listForCompany($rootScope.user.companyCode)
    .then(
      function(resp){
        $scope.facilities = resp;
        if($state.$current.name != 'beacons-add-one') {
          $scope.displayLoading = true;
          $scope.retrieve();
        } else {
          $scope.add = true;
        }
      });
  }

  $scope.retrieve = function() {
    BeaconsServices.get($scope.objectId)
    .then(
      function(resp){
        $scope.object = resp.data;
        $scope.object.facilityId = $scope.object.facilityId ? ""+$scope.object.facilityId:undefined;
        $scope.object_backup = angular.copy(resp);
        $scope.displayLoading = false;
      });
  }

  $scope.insert = function() {
    var beacon = angular.copy($scope.object);
    beacon.companyId = $rootScope.user.companyCode;
    
    BeaconsServices.insert(beacon)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The beacon has been added successfully !");
        $state.go('beacons-details-one', {id:resp.data.id, edit:0});
      });
  }

  $scope.update = function() {
    var beacon = angular.copy($scope.object);
    // The facility id must be a number
    if(beacon.facilityId) {
      beacon.facilityId = parseInt(beacon.facilityId);
    }
    BeaconsServices.update(beacon)
    .then(
      function(resp){
        AlertFact.addAlert("success", "The beacon has been updated successfully !");
        $scope.edit = false;
        $scope.init();
      });
  }

  $scope.reset = function() {
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.cancel = function() {
    $scope.edit = false;
    angular.copy($scope.object_backup, $scope.object);
  }

  $scope.init();
});