angular.module('administration')
.controller('UsersCtrl',
function ($scope,$rootScope,AlertFact,$log,$http) {
  // Hide all the app until the we know if the user is authorized
  $scope.users = [];

  var getUsers = function() {
    $http.get(appLocation+"/management/users?company="+$rootScope.user.companyCode)
    .success(function(result) {
      $scope.users = result;
    })
    .error(function(error) {
      angular.forEach(error.errors, function(value, key) {
        AlertFact.addAlert("", value.message);
      });
    });
  }

  getUsers();

  $scope.delete = function(username) {
    $http.delete(appLocation+"/management/users/"+username)
    .success(function(result) {
      getuUers();
      AlertFact.addAlert("success", "The user has been deleted");
    })
    .error(function(error) {
      angular.forEach(error.errors, function(value, key) {
        AlertFact.addAlert("", value.message);
      });
    });
  }

});