angular.module('administration')
.controller('InvitationsCtrl',
function ($scope,$rootScope,AlertFact,$log,$http) {
  // Hide all the app until the we know if the user is authorized
  $scope.invitations = [];

  var getInvitations = function() {
    $http.get(appLocation+"/management/invitations?company="+$rootScope.user.companyCode)
    .success(function(result) {
      $scope.invitations = result;
    })
    .error(function(error) {
      AlertFact.addAlert("error", error.errors[0].message);
    });
  }

  getInvitations();

  $scope.resendInvitation = function(code) {
    $http.put(appLocation+"/management/resend-invitation/"+code)
    .success(function(result) {
      AlertFact.addAlert("success", "The invitation has been resent");
    })
    .error(function(error) {
      AlertFact.addAlert("error", error.errors[0].message);
    });
  }

  $scope.delete = function(code) {
    $http.delete(appLocation+"/management/invitations/"+code)
    .success(function(result) {
      getInvitations();
      AlertFact.addAlert("success", "The invitation has been deleted");
    })
    .error(function(error) {
      AlertFact.addAlert("error", error.errors[0].message);
    });
  }

});