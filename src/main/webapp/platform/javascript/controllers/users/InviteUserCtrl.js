angular.module('administration')
.controller('InviteUserCtrl',
function ($scope,$rootScope,$log,$http,AlertFact,$state) {

  // Init rights
  $scope.roles = {};
  $scope.roles.companyRole = "none";
  $scope.roles.companyNotificationRole = "none";
  $scope.roles.facilityRole = "none";
  $scope.roles.facilityNotificationRole = "none";
  $scope.roles.beaconRole = "none";
  $scope.roles.userRole = "none";
  $scope.roles.invitationRole = "none";

  $scope.invitation = {};

  $scope.inviteUser = function() {
    // build permissions
    var permissions = {};

    if($scope.roles.companyRole != "none")
      permissions["COMPANY_"+$scope.roles.companyRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode}];
    if($scope.roles.companyNotificationRole != "none")
      permissions["COMPANY_NOTIFICATION_"+$scope.roles.companyNotificationRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode, notificationId:"*"}];
    if($scope.roles.facilityRole != "none")
      permissions["FACILITY_"+$scope.roles.facilityRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode, facilityId:"*"}];
    if($scope.roles.facilityNotificationRole != "none")
      permissions["FACILITY_NOTIFICATION_"+$scope.roles.facilityNotificationRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode, notificationId:"*"}];
    if($scope.roles.beaconRole != "none")
      permissions["BEACON_"+$scope.roles.beaconRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode}, {companyCode:$rootScope.user.companyCode, notificationId:"*"}];
    if($scope.roles.userRole != "none")
      permissions["USER_"+$scope.roles.userRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode}];
    if($scope.roles.invitationRole != "none")
      permissions["INVITATION_"+$scope.roles.invitationRole.toUpperCase()] = [{companyCode:$rootScope.user.companyCode}];

    // build invitation
    // email has been provided and checkled befre this step
    $scope.invitation.action = "CREATE_USER";
    $scope.invitation.scope = {
      company: {code:$rootScope.user.companyCode},
      userPermissions: permissions
    }

    $log.debug($scope.invitation);
    $http.post(appLocation+"/management/invitations", $scope.invitation)
    .success(function(result) {
      // Go to list of invitation
      $state.go("invitations-all");
    })
    .error(function(error) {
      $log.info(error);
    })
  }
  
});