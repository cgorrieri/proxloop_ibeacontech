angular.module('administration').controller('ModalGalleryCtrl',
  function ($scope,$controller,callback) {

    angular.extend(this, $controller('GalleryCtrl', {$scope: $scope}));

    $scope.setSelectAction(function(selected) {
      callback(selected.publicUrl);
    });
});