angular.module('administration').controller('FacilitiesCtrl',['$scope','$rootScope','cloudendpoint','AlertFact',
  function ($scope,$rootScope,cloudendpoint,AlertFact) {
    // Hide all the app until the we know if the user is authorized
    $scope.facilities = [];
    $scope.companies = [];

    $scope.displayAddForm = false;

    $scope.initgapi=function() {
      cloudendpoint.init().then(
        function(){
          /* SUCCESS */
          $scope.retrieveFacilities();
          $scope.retrieveCompanies();
      },
      function(){
        /* ERROR */
        // TODO: Display special page or retry 
        alert('error init cloud endpoint api')
      });
    }

    $scope.retrieveCompanies = function() {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.companies.list().execute(function(resp){
          $scope.companies = resp.items;
          $scope.$apply();
        });
      //});
    };

    $scope.retrieveFacilities = function() {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.facilities.list().execute(function(resp){
          $scope.facilities = resp.items;
          $scope.$apply();
        });
      //});
    };

    $scope.insertFacility = function() {
      newFacility = {
        companyCode : $scope.facilityCompanyCode,
        code : $scope.facilityCode,
        name : $scope.facilityName,
        /* TODO: add address in the form */ physicalAddress : { country: 'FR' }
      }
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.facilities.insert(newFacility).execute(function(resp){
          // display the errors if any
          if(resp.error != undefined) {
            // Currently only one error is returned
            $scope.addFacilityErrors = [decodeErrors(resp.error)];
            $scope.$apply();
          }
          // Close the add form, reset the errors and re retrieve the companies
          else {
            $scope.$apply(function() {
              AlertFact.addAlert("success", "The facility has been added successfully !");
              $scope.displayAddForm = false;
              $scope.addFacilityErrors = [];
              $scope.retrieveFacilities();
            });
          }
        });
      //});
    }

    $scope.deleteFacility = function(facility) {
      
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.facilities.delete({companyCode : facility.companyCode, code : facility.code}).execute(function(resp){
          // display the errors if any
          if(resp.error != undefined) {
            // Currently only one error is returned
            $scope.deleteFacilityErrors = [decodeErrors(resp.error)];
            $scope.$apply();
          }
          // Close the add form, reset the errors and re retrieve the companies
          else {
            AlertFact.addAlert("success", "The facility has been deleted successfully !");
            $scope.retrieveFacilities();
          }
        });
      //});
    }

    $scope.initgapi();
}]);