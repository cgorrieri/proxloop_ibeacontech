angular.module('administration').controller('GalleryCtrl',
  function ($scope,$rootScope,AlertFact,$http) {

    $scope.setSelectAction = function(callback) {
      $scope.selectCallback = callback;
      $scope.isSelectable = true;
    }

    $scope.validateSelection = function() {
      if(!$scope.isSelectable) return;
      $scope.selectCallback($scope.selectedPicture);
    }

    // Load all picture in the folder of the company
    var getCompanyPictures = function() {
      $http.get(appLocation+"/management/pictures/"+$rootScope.user.companyCode+"/") 
        .success(function(result) {
          console.log("GetCompanyPictures.get -> OK ", result);
          $scope.pictures = result;
        })
        .error(function(error) {
          console.log("GetCompanyPictures.get -> KO ", error);
          // TODO: add error management
        });
    }


    $scope.refresh = function() {
      getCompanyPictures();
    }

    $scope.delete = function(pictureIndex) {
      var picture = $scope.pictures[pictureIndex];
      $http.delete(appLocation+"/management/pictures/"+picture.resourceId) 
        .success(function(result) {
          console.log("GetCompanyPictures.delete -> OK ", result);
          $scope.refresh();
        })
        .error(function(error) {
          console.log("GetCompanyPictures.delete -> KO ", error);
          // TODO: add error management
        });
    }

    $scope.select = function(picture) {
      if(!$scope.isSelectable) return;
      if($scope.selectedPicture) {
        $scope.selectedPicture.selected = false;
      }
      $scope.selectedPicture = picture;
      picture.selected = true;

    }
    

    /************************
        UPLOAD OF PICTURE
     ************************/
    var uploadImage = function(file, success, error) {
      var fd = new FormData();
      // Prefix is the company
      fd.append("prefix", $rootScope.user.companyCode + "/");
      //Take the first selected file
      fd.append("file", file);

      $http.post(appLocation+'/management/pictures/upload', fd, {
        headers: {"Content-type": undefined},
        transformRequest: angular.identity
      }).success(success).error(error);
    }

    $scope.upload = function() {
      uploadImage($scope.image.file,
        function(data){
          $scope.refresh();
          // Reset upload file
          $scope.image = undefined
        },
        function(error){
          // TODO display error
        }
      );
    }

    $scope.refresh();
});