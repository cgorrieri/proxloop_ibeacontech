angular.module('administration')
.controller('AppCtrl',
function ($rootScope, $scope, $state, $log, AuthenticationService) {

  // Hide all the app until we know if the user is authorized
  $scope.showApp = false;
  $scope.showLogin = false;
  $scope.requireLogin = true;

  var enableApp = function() {
    $scope.showApp = true;
    $scope.showLogin = false;
    $scope.loginError = undefined;
  }

  var displayLoginError = function() {
    $scope.loginError = "Incorrect username or passord !"
  }

  $scope.loginForm = {};

  $scope.collapseMenu = false;

  $scope.login = function() {
    var username = $scope.loginForm.username;
    var password = $scope.loginForm.password;

    $scope.loading = true;
    AuthenticationService.login(username, password)
    .then(
      function(response) {
        $scope.loading = false;
        enableApp();
      },
      function(error) {
        $scope.showLogin = true;
        displayLoginError();
      });
  }

  $scope.logout = function() {
    AuthenticationService.logout();
    $scope.showApp = false;
    $scope.showLogin = true;
  }

  // When the state change or initialized
  $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams){ 
      // If the state is in the white list, we can display it
      if(toState.isWhite) {
        $scope.requireLogin = false;
      }
      // Else the user must be logged in to access the view
      else {
        $scope.requireLogin = true;
        AuthenticationService.isLogged()
        .then(
          function(success) {
            enableApp();
          },
          function(error){
            $scope.showLogin = true;
            $scope.showApp = false;
          });
      }
    });

});