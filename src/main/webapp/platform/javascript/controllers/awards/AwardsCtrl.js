angular.module('administration').controller('AwardsCtrl',['$scope','$sce','cloudendpoint','AlertFact','$state',
  function ($scope,$sce,cloudendpoint,AlertFact,$state) {
    $scope.awards = [];
    $scope.displayLoading = true;

    $scope.init=function() {
      $scope.retrieveAwards();
    }

    $scope.retrieveAwards = function() {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.awards.list().execute(function(resp){
          $scope.awards = resp.items;
          $scope.displayLoading = false;
          $scope.$apply();
        });
      //});
    };

    $scope.edit = function(awardId) {
      // Redirect to edit page
      $state.go('awards-details-one', {id:awardId, edit:1});
    }

    $scope.detail = function(awardId) {
      $state.go('awards-details-one', {id:awardId, edit:0});
    }

    $scope.delete = function(awardId) {
      gapi.client.administration.awards.delete({id:awardId}).execute(function(resp){
        // display the errors if any
        if(resp.error != undefined) {
          // Currently only one error is returned
          AlertFact.addAlert("error", decodeErrors(resp.error).message);
          $scope.$apply();
        }
        else {
          AlertFact.addAlert("success", "The award has been deleted successfully !");
          $scope.retrieveAwards();
        }
      });
    }

    $scope.init();
}]);