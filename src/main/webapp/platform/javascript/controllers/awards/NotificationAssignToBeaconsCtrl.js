// TEMPLATE: templates/notifications/notification_assign_to_beacons.html

angular.module('administration').controller('NotificationAssignToBeaconsCtrl',
  function ($scope,$sce,$filter,cloudendpoint,AlertFact,$state,ExecuteParallel) {
    $scope.notifications = [];
    $scope.beacons = [];

    $scope.displayLoading = true;
    $scope.canSave = false;

    $scope.init=function() {
      // We want to retrieve both the notifications and the beacons before doing anything
      ExecuteParallel([$scope.retrieveNotifications,$scope.retrieveBeacons]).then(function() {
        $scope.displayLoading = false;
      });
    };

    $scope.retrieveNotifications = function(defer) {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.notifications.list().execute(function(resp){
          // Set url as trusted to display it in the iframe
          $scope.notifications = resp.items.map(function(notif) {
            var url = "";
            if(notif.notificationType == NotificationType.TEMPALTE) {
              url = notif.templateImplement.displayUrl;
            } 
            else if (notif.notificationType == NotificationType.CUSTOM_URL) {
              url = notif.customWebUrl;
            }
            notif.trustedUrl = $sce.trustAsResourceUrl(url);

            return notif;
          });
          if(defer) defer.resolve();
        });
      //});
    };

    $scope.retrieveBeacons = function(defer) {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.beacons.list().execute(function(resp){
          $scope.beacons = resp.items;
          if(defer) defer.resolve();
        });
      //});
    };


    $scope.assign = function() {
      var selectedBeacons = []
      for (beacon of $scope.beacons) {
        if(beacon.selected)
          selectedBeacons.push(beacon.mac);
      }
      console.log(selectedBeacons);
      gapi.client.administration.notifications.assignToBeacons(
      {"notificationId": $scope.notification.id, "list":selectedBeacons})
      .execute(function(resp) {
        // display the errors if any
        if(resp.error != undefined) {
          // Currently only one error is returned
          $scope.errors = [decodeErrors(resp.error)];
        }
        // Reset the form
        else {
          AlertFact.addAlert("success", "The association has been added successfully !");
          $scope.notification = undefined;
          $scope.isSelectAll = true;
          $scope.isSelectAll = false;
        }
        $scope.$apply();
      });
    }


    // BEACON SELECTION MANAGEMENT
    $scope.$watch('isSelectAll', function() {
      for (beacon of $scope.beacons) {
        beacon.selected = $scope.isSelectAll;
      }
      $scope.checkCanSave();
    });

    // Get the list of beacons id that are selected 
    $scope.getSelectedBeaconsId = function () {
      var selectedBeacons = [];
      for (beacon of $scope.beacons) {
        if(beacon.selected)
          selectedBeacons.push(beacon.mac);
      }
      return selectedBeacons;
    }

    // Check if all the conditions are ok to save the assignement
    $scope.checkCanSave = function () {
      $scope.canSave = notification && $scope.getSelectedBeaconsId().length > 0;
    }

    $scope.init();
});