angular.module('administration').controller('AwardCtrl',
  function ($scope,$rootScope,$sce,cloudendpoint,AlertFact,$state,$stateParams,$timeout,$http,$q,$filter) {
    $scope.displayLoading = false;
    $scope.edit = ($stateParams.edit == 1);
    $scope.add = false;
    $scope.objectId = $stateParams.id;
    $scope.object = {};

    $scope.init=function() {
      if($state.$current.name != 'awards-add-one') {
        $scope.displayLoading = true;
        $scope.retrieve();
      } else {
        $scope.add = true;
        $scope.object.loops = 1;
      }
    }

    $scope.retrieve = function() {
      //cloudendpoint.doCall().then(function() {
        gapi.client.administration.awards.get({id:$scope.objectId}).execute(function(resp){
          $scope.object = resp;
          $scope.object_backup = angular.copy(resp);
          $scope.displayLoading = false;

          $scope.$apply();
        });
      //});
    }

    $scope.insert = function() {      
      gapi.client.administration.awards.insert($scope.object).execute(function(resp){
        AlertFact.addAlert("success", "The award has been added successfully !");
        $state.go('awards-details-one', {id:resp.id, edit:0});
      });
    }

    $scope.update = function() {
      gapi.client.administration.awards.update($scope.object).execute(function(resp){
        AlertFact.addAlert("success", "The award has been updated successfully !");
        $scope.edit = false;
        $scope.init();
      });
    }

    $scope.reset = function() {
      angular.copy($scope.object_backup, $scope.object);
    }

    $scope.cancel = function() {
      $scope.edit = false;
      angular.copy($scope.object_backup, $scope.object);
    }

    $scope.init();
});