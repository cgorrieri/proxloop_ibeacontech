angular.module('administration')
.factory('AuthenticationService',
function ($http, $cookies, $rootScope, $q) {
    var LOGIN_URL =     appLocation + '/management/users/authenticate';
    var REGISTER_URL =  appLocation + '/users/management/register';
    var USER_KEY = "current_user";

    var getAuthorization = function(username, password) {
        // the authorization is based on the basic HTTP one
        return "Basic " + btoa(username + ":" + password)
    }

    var setCredentials = function (user, authorization) {
        // Save the user and it's authorization in the context
        $rootScope.user = angular.copy(user);
        $rootScope.user.authorization = authorization

        // Set the default http autheorization for all new http request
        $http.defaults.headers.common['Authorization'] = authorization;
        // Add a cookie to be able to retrieve the user even after refresh of the page
        $cookies.putObject(USER_KEY, $rootScope.user);
    };

    var clearCredentials = function () {
        $rootScope.user = {};
        $cookies.remove(USER_KEY);
        $http.defaults.headers.common.Authorization = 'Basic ';
    };

    var internalLoggin = function(authorization) {
        return $http.get(LOGIN_URL, { headers: {'Authorization' : authorization}})
        .success(function (response) {
            console.log("Authentication succeed:",response);

            console.log("Save credentials in context");
            setCredentials(response,authorization);

            return response;
        })
        .error(function (error) {
            console.log("Authentication failed:",error);
            return $q.reject(error);
        });
    }

    var service = {};

    // Return if the user is already logged
    service.isLogged = function() {
        // If user already exist in the context, just return it
        if($rootScope.user && $rootScope.user.authorization) {
            var deffered = $q.defer();
            deffered.resolve($rootScope.user);
            return deffered.promise;
        }

        // Otherwise if there is a user in the cookies, we try to anthenticate him
        var user = $cookies.getObject(USER_KEY);
        if(user) {
            console.debug("Current user in cookies", user);
            return internalLoggin(user.authorization);
        }
        // Else the user is not authenticate and will have to log in
        else {
            var deffered = $q.defer();
            deffered.reject("NotLogged");
            return deffered.promise;
        }
    }

    service.login = function (username, password) {
        var authorization = getAuthorization(username, password);
        return internalLoggin(authorization);
    };

    service.register = function (registration) {
        return $http.post(REGISTER_URL, registration)
        .success(function (response) {
            console.log("Registration succeed:",response);

            console.log("Save credentials in context");
            var authorization = getAuthorization(registration.username, registration.password);
            setCredentials(response,authorization);

            return response;
        })
        .error(function (error) {
            console.log("Registration failed:",error);
            return $q.reject(error);
        });
    };

    service.logout = function() {
        clearCredentials();
    }

    return service;
});