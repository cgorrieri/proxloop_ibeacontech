angular.module('administration')
.factory('companies', function ($http, $q) {
  var resourceName = "companies";
  var BASE_URL = appLocation+"/management/companies/";
  var resource = {};

  resource.list = function() {
    return $http.get(BASE_URL)
            .then(function(response) {
              console.info(resourceName+".list -> resp:", response);
              return response.data;
            });
  }

  resource.get = function(id) {
    return $http.get(BASE_URL+id)
      .success(function(response) { 
        console.info(resourceName+".get -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".get -> KO ", error);
        return $q.reject();
      });
  }

  resource.insert = function(resource) {
    return $http.post(BASE_URL, resource)
      .success(function(response) { 
        console.info(resourceName+".insert -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".insert -> KO ", error);
        return $q.reject();
      });
  }

  resource.update = function(resource) {
    return $http.put(BASE_URL+resource.code, resource)
      .success(function(response) { 
        console.info(resourceName+".update -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".update -> KO ", error);
        return $q.reject();
      });
  }

  resource.delete = function(id) {
    return $http.delete(BASE_URL+id)
      .success(function(response) { 
        console.info(resourceName+".delete -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".delete -> KO ", error);
        return $q.reject();
      });
  }
  
  return resource;
});