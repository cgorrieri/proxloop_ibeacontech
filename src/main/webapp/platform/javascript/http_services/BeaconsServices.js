angular.module('administration')
.factory('BeaconsServices',
function ($http, $q) {
  var resourceName = "beacons";
  var BASE_URL = appLocation+"/management/beacons/";
  var resource = {};

  resource.list = function(params) {
    params = params || {};
    return $http.post(BASE_URL+"search", params)
      .success(function(response) {
        console.info(resourceName+".list -> resp:", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".list -> KO ", error);
        $q.reject();
      });
  }

  resource.get = function(id) {
    return $http.get(encodeURI(BASE_URL+id))
      .success(function(response) { 
        console.info(resourceName+".get -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".get -> KO ", error);
        $q.reject();
      });
  }

  resource.insert = function(resource) {
    return $http.post(BASE_URL, resource)
      .success(function(response) { 
        console.info(resourceName+".insert -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".insert -> KO ", error);
        $q.reject();
      });
  }

  resource.update = function(resource) {
    console.log(resource);
    return $http.put(BASE_URL+resource.mac, resource)
      .success(function(response) { 
        console.info(resourceName+".update -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".update -> KO ", error);
        $q.reject();
      });
  }

  resource.delete = function(id) {
    return $http.delete(BASE_URL+id)
      .success(function(response) { 
        console.info(resourceName+".delete -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".delete -> KO ", error);
        $q.reject();
      });
  }

  resource.getNotificationsAssignments = function(id) {
    return $http.get(BASE_URL+id+"/assign")
      .success(function(response) { 
        console.info(resourceName+".getNotificationsAssignments -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".getNotificationsAssignments -> KO ", error);
        $q.reject();
      });
  }

  resource.updateNotificationsAssignments = function(id, assignments) {
    return $http.post(BASE_URL+id+"/assign", assignments)
      .success(function(response) { 
        console.info(resourceName+".updateNotificationsAssignments -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".updateNotificationsAssignments -> KO ", error);
        $q.reject();
      });
  }
  
  return resource;
});