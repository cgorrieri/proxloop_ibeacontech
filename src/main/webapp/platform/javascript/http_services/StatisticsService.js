angular.module('administration')
.factory('StatisticsServices',
function ($http, $q) {
  var resourceName = "statistics";
  var BASE_URL = appLocation+"/management/statistics/";
  var resource = {};

  // beginDate and endDate must be at ISO format 
  resource.getForBeacon = function(beaconId,beginDate,endDate) {
    
    return $http.get(encodeURI(BASE_URL+"beacon/"+beaconId+"/"+beginDate+"/"+endDate))
      .success(function(response) { 
        console.info(resourceName+".getForBeacon -> OK ", response);
        return response.data;
      })
      .error(function(error) {
        console.info(resourceName+".getForBeacon -> KO ", error);
        return $q.reject();
      });
  }

  return resource;
});