angular.module('administration').factory('cloudendpoint',
function ($q, $timeout) {
  var cloudendpoint = {};
  
  cloudendpoint.isLoaded = false;

  cloudendpoint.ROOT = appLocation + '/_ah/api';

  cloudendpoint.init = function() {
    //if (cloudendpoint.isLoaded) return;
    var administrationDefer=$q.defer();
    var oauthloaddefer=$q.defer();
    var oauthDefer=$q.defer();

    cloudendpoint.loadApi(administrationDefer, oauthDefer);

    var chain=$q.all([administrationDefer.promise,oauthDefer.promise]);
    return chain;
  };

  cloudendpoint.loadApi = function(administrationDefer, oauthDefer) {
    if(gapi.client) {
      gapi.client.load('administration', 'v1', function() {
        administrationDefer.resolve(gapi);
      }, cloudendpoint.ROOT);
      gapi.client.load('oauth2', 'v2', function(){
        oauthDefer.resolve(gapi);
      });
    } else {
      console.warn("gapi.client not initialized: delayed init of google api");
      $timeout(function() {
        cloudendpoint.loadApi(administrationDefer, oauthDefer);
      }, 50);
    }
  };

  cloudendpoint.doCall = function(callback) {
    var p=$q.defer();
    gapi.auth.authorize({client_id: 'clientid', scope: 'https://www.googleapis.com/auth/userinfo.email', immediate: true}, function(){
        var request = gapi.client.oauth2.userinfo.get().execute(function(resp) {
          if (!resp.code) {
            p.resolve(gapi);
          } else {
            p.reject(gapi);
          }
        });
    });
    return p.promise;
  };

  cloudendpoint.sigin = function(displayWindow, callback) {
    gapi.auth.authorize({client_id: '300823059214-q9iq43jn54dbklktamuu77g8val29ams.apps.googleusercontent.com', scope: 'https://www.googleapis.com/auth/userinfo.email', immediate: displayWindow}, callback);
  };

  return cloudendpoint;
});