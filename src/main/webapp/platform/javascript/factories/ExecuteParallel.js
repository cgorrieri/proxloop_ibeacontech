angular.module('administration')
// Execute a list of function in parallel
// The functions must take in parameter a defer object
.factory('ExecuteParallel', function($q){
  
  return function(functions){
    
    var promises = functions.map( function(funct){
      
      var deffered  = $q.defer();
      
      funct(deffered);
      
      return deffered.promise;

    })
    
    return $q.all(promises);
  }
  
});