angular.module('administration').factory('AlertFact', function ($alert) {
  return {
    addAlert: function(type, message, time) {
      time = typeof time !== 'undefined' ? time : 3;
      $alert({title: message, placement: 'top', type: type, keyboard: true, show: true, duration:time});
    }
  };
});