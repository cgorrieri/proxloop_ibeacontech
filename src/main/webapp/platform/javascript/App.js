// Environments
var production = "https://proxloop-inc.appspot.com";
var test = "https://pelagic-region-637.appspot.com";
var local = "http://localhost:8080";
var appLocation = local;

angular.module('administration', ['ui.router','ngSanitize','ngCookies',/*'textAngular',*/'ngMessages','mgcrea.ngStrap','mgcrea.ngStrap.modal'])

.config(function($datepickerProvider) {
  angular.extend($datepickerProvider.defaults, {
    dateFormat: 'dd/MM/yyyy',
    startWeek: 1,
    iconLeft: 'fa fa-chevron-left',
    iconRight: 'fa fa-chevron-right'
  });
})

// Config routes
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/');

  $stateProvider
  .state('home', {
    url:'/',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  })

  // COMPANIES
  .state('companies-manage-all', {
    url:'/companies/manage/all',
    templateUrl: 'templates/companies/companies.html',
    controller: 'CompaniesCtrl'
  })
  .state('companies-manage-mine', {
    url:'/my-company',
    templateUrl: 'templates/companies/company_crud.html',
    controller: 'CompanyCrudCtrl'
  })

  // INVITATIONS
  .state('invitations-all', {
    url:'/invitations/all',
    templateUrl: 'templates/invitations/invitations.html',
    controller: 'InvitationsCtrl'
  })
  .state('invitations-new', {
    url:'/invitations/new',
    templateUrl: 'templates/invitations/new_invitation.html',
    controller: 'InviteUserCtrl'
  })

  // FACILITIES
  .state('facilities-manage-all', {
    url:'/facilities/manage/all',
    templateUrl: 'templates/facilities/facilities.html',
    controller: 'FacilitiesCtrl'
  })
  .state('facilities-details-one', {
    url:'/facilities/details/:id/:edit',
    templateUrl: 'templates/facilities/facility_crud.html',
    controller: 'FacilityCrudCtrl'
  })
  .state('facilities-add-one', {
    url:'/facilities/add',
    templateUrl: 'templates/facilities/facility_crud.html',
    controller: 'FacilityCrudCtrl'
  })

  // BEACONS
  .state('beacons-manage-all', {
    url:'/beacons/manage/all',
    templateUrl: 'templates/beacons/beacons.html',
    controller: 'BeaconsCtrl'
  })
  .state('beacons-details-one', {
    url:'/beacons/details/:id/:edit',
    templateUrl: 'templates/beacons/beacon_crud.html',
    controller: 'BeaconCrudCtrl'
  })
  .state('beacons-add-one', {
    url:'/beacons/add',
    templateUrl: 'templates/beacons/beacon_crud.html',
    controller: 'BeaconCrudCtrl'
  })
  .state('statistics-beacons', {
    url:'/statistics/beacons',
    templateUrl: 'templates/statistics/beacons.html',
    controller: 'BeaconsStatisticsCtrl'
  })
  .state('beacons-assign-notifications', {
    url:'/beacons/assign-notifications',
    templateUrl: 'templates/beacons/assign_notifications_to_beacons.html',
    controller: 'BeaconsAssignNotificationsCtrl'
  })
  
  // NOTIFICATION
  .state('notifications-manage-all', {
    url:'/notifications/manage/all',
    templateUrl: 'templates/notifications/notifications.html',
    controller: 'NotificationsCtrl'
  })
  .state('notifications-details-one', {
    url:'/notifications/details/:id/:edit',
    templateUrl: 'templates/notifications/notification_crud.html',
    controller: 'NotificationCtrl'
  })
  .state('notifications-add-one', {
    url:'/notifications/add',
    templateUrl: 'templates/notifications/notification_crud.html',
    controller: 'NotificationCtrl'
  })
  .state('notifications-assign', {
    url:'/notifications/assign-to-beacons',
    templateUrl: 'templates/notifications/notification_assign_to_beacons.html',
    controller: 'NotificationAssignToBeaconsCtrl'
  })
  
  /*// AWARDS
  .state('awards-manage-all', {
    url:'/awards/manage/all',
    templateUrl: 'templates/awards/awards.html',
    controller: 'AwardsCtrl'
  })
  .state('awards-details-one', {
    url:'/awards/details/:id/:edit',
    templateUrl: 'templates/awards/award_crud.html',
    controller: 'AwardCtrl'
  })
  .state('awards-add-one', {
    url:'/awards/add',
    templateUrl: 'templates/awards/award_crud.html',
    controller: 'AwardCtrl'
  })*/

  .state('gallery', {
    url:'/gallery',
    templateUrl: 'templates/gallery.html',
    controller: 'GalleryCtrl'
  })

  // SPECIFIC
  .state('register-invitation', {
    url:'/register-invitation/:code',
    templateUrl: 'templates/specific/register-invitation.html',
    controller: 'RegisterInvitation',
    isWhite: true
  })
}]);