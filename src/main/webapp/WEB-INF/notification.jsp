<%@page import="com.google.appengine.repackaged.com.google.gson.Gson"%>
<%@page import="com.ibeacons._01_controllers.administration.TemplateCatalogController"%>
<%@page import="com.ibeacons.pom.PomTemplate"%>
<%@page import="com.ibeacons._03_dao.basic_data.NotificationDaoImpl"%>
<%@page import="com.ibeacons.dom.notifications.DomNotification"%>
<%@page import="com.ibeacons.dom.notifications.DomTemplateImplement"%>
<%@page import="com.ibeacons.servlets.CrossOriginServletHelper" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
    //CrossOriginServletHelper.checkOrigin(this, request, response);
	String notificationId = request.getParameter("id");
	DomNotification notification = NotificationDaoImpl.getInstance().getNotification(Long.parseLong(notificationId));
	DomTemplateImplement webView = notification.getTemplateImplement();
	// Get the template
	PomTemplate theTemplate = null; 
	for(PomTemplate pt : TemplateCatalogController.templates) {
		if(pt.id == webView.templateId) {
			theTemplate = pt;
			break;
		}
	}
	Gson gson = new Gson();
	String attributes = gson.toJson(webView.values);
	String templateAttributes = gson.toJson(theTemplate.attributes);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
	<style type="text/css">
        <%= theTemplate.cssContent %>
	</style>
	<link rel="stylesheet" href="/font-awesome/css/font-awesome.css">


    <!-- JQuery -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    
    <!-- Angular -->
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular-sanitize.js"></script>
    <style type="text/css">
        
                .template h2 {color:red}
                
    </style>
    <script type="text/javascript">

    var app = angular.module('app', ['ngSanitize']);
    app.directive("compileHtml", function($parse, $sce, $compile) {
    return {
        restrict: "A",
        link: function (scope, element, attributes) {

          var expression = $sce.parseAsHtml(attributes.compileHtml);

          var getResult = function () {
            return expression(scope);
          };

          scope.$watch(getResult, function (newValue) {
            var linker = $compile(newValue);
            element.html(linker(scope));
          });
        }
    }
    });

    var URL = window.URL || window.webkitURL;

    app.controller('TheController',
      function ($scope, $sce, $timeout) {

        $scope.template = {};

        $scope.html = $sce.trustAsHtml('<%= theTemplate.htmlContent %>');


        var attributes = <%= attributes %>;
        var attributesObj = {};
        for (var attribute of attributes) {
            attributesObj[attribute.name] = attribute.value;
        }
        
        var templateAttributes = <%= templateAttributes %>;
        
        for (templateAttr of templateAttributes) {
            var attrName = templateAttr.attribute;
            var value = attributesObj[attrName];

            if(templateAttr.type == "image") {
                $scope.template[attrName] = {url: value};
            } else {
                $scope.template[attrName] = value;
            }
        }
        console.log($scope.template);
    });

    </script>
</head>
<body ng-app="app" ng-controller="TheController" >
<div class="template" compile-html="html">
</div>

</body>
</html>