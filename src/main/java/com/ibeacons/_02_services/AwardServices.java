package com.ibeacons._02_services;

import java.util.List;

import com.ibeacons._03_dao.basic_data.AwardDao;
import com.ibeacons._03_dao.basic_data.AwardDaoImpl;
import com.ibeacons.dom.DomAward;
import com.ibeacons.dom.user_data.DomManagementUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.tools.AwardHelper;
import com.ibeacons.tools.Validator;

public class AwardServices {
	private static final int AWARD_TITLE_MAX_LENGTH = 25;
	private static final int AWARD_DESCRIPTION_MAX_LENGTH = 200;
	
	private AwardDao awardDao;
	
	public AwardServices() {
		awardDao = AwardDaoImpl.getInstance();
	}
	
	/**
	 * Return a award by its Id
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public DomAward getAward(long id) throws ActionException {
		try {
			return awardDao.getAward(id);
		}catch (com.googlecode.objectify.NotFoundException e) {
			throw new ActionException(ErrorCode.Award_ItemDoesNotExist);
		}
	}
	
	/**
	 * Get all companies
	 * @return
	 */
	public List<DomAward> getAwards(DomManagementUser user) {
		// TODO better filter according to company or facility level
		return awardDao.getAwards(user.getCompanyCode());
	}
	
	/**
	 * Add a new award
	 * @param award
	 * @return
	 */
	public DomAward addAward(DomAward award) {
		commonCheck(award);
		// Insert and return the result
		return awardDao.insertAward(award);
	}
	
	/**
	 * Update an existed award
	 * @param award
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public DomAward updateAward(DomAward award) throws ActionException {
		// To update the item must exist
		AwardHelper.checkAwardExists(award.id);
		commonCheck(award);
		return awardDao.updateAward(award);
	}
	
	/**
	 * Delete an existed award
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public boolean deleteAward(long id) {
		AwardHelper.checkAwardExists(id);
		return awardDao.deleteAward(id);
	}
	
	/**
	 * Check common fields for create and update
	 * @param award
	 * @throws ActionException
	 */
	private void commonCheck(DomAward award) throws ActionException {
		if(! Validator.validateText(award.title, AWARD_TITLE_MAX_LENGTH)) throw new ActionException(ErrorCode.Award_InvalidTitle);
		if(! Validator.validateText(award.description, AWARD_DESCRIPTION_MAX_LENGTH)) throw new ActionException(ErrorCode.Award_InvalidDescription);
	}
	
}
