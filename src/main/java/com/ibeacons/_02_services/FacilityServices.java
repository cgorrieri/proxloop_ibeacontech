package com.ibeacons._02_services;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.Permission;
import com.ibeacons._03_dao.basic_data.BeaconDaoImpl;
import com.ibeacons._03_dao.basic_data.FacilityDao;
import com.ibeacons._03_dao.basic_data.FacilityDaoImpl;
import com.ibeacons._03_dao.basic_data.NotificationDaoImpl;
import com.ibeacons.dom.DomAddress;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomBeaconSearchParameters;
import com.ibeacons.tools.CompanyHelper;
import com.ibeacons.tools.Validator;

@Service
public class FacilityServices {
	private static final int FACILITY_NAME_MAX_LENGTH = 20;
	
	@Inject
	private PermissionService permissionService;
	
	private FacilityDao facilityDao = FacilityDaoImpl.getInstance();

	public DomFacility getFacility(Long id) throws ActionException {
		//CompanyHelper.checkCompany(companyCode);
		try {
			return facilityDao.getFacility(id);
		}catch (com.googlecode.objectify.NotFoundException e) {
			throw new ActionException(ErrorCode.Facility_ItemDoesNotExist);
		}
	}

	public List<DomFacility> getFacilities() {
		return facilityDao.getFacilities();
	}

	public List<DomFacility> getFacilities(String companyCode) throws ActionException {
		CompanyHelper.checkCompany(companyCode);
		permissionService.checkPermission(Permission.RETRIEVE_COMPANY);
//		List<RoleData> values = permissionService.getValuesForPermission(Permission.RETRIEVE_FACILITY);
//		List<Long> facilityIds = new ArrayList<Long>();
//		if(values != null)
//			for(String value : values) {
//				String[] tokens = value.split(UserRole.SEPARATOR);
//				
//				// If a value is about this company
//				if(tokens[UserRole.COMPANY_LEVEL].equals(companyCode)) {
//					
//					// If the permission is well set with a facility level
//					if(tokens.length > UserRole.FACILITY_LEVEL) {
//						// If all facilities are ok to retrieve, retrieve all now
//						if(UserRole.WILDCARD.equals(tokens[UserRole.FACILITY_LEVEL])) {
//							return facilityDao.getFacilities(companyCode);
//						}
//						// Otherwise add the current id in the list
//						else {
//							facilityIds.add(Long.decode(tokens[UserRole.FACILITY_LEVEL]));
//						}
//					}
//				}
//			}
//		
//		// If the user cannot retrieve at least one facility, return.
//		if(facilityIds.isEmpty()) return new ArrayList<DomFacility>();
		
		// Otherwise retrieve the restricted list of facilities
		return facilityDao.getFacilities(companyCode);
	}

	public DomFacility addFacility(DomFacility facility) throws ActionException {
		checkCompany(facility);
		// Check if the item does not exist
		// TODO look for name

		checkCommon(facility);
		return facilityDao.insertFacility(facility);
	}

	public DomFacility updateFacility(DomFacility facility) throws ActionException {
		checkCompany(facility);
		if(!facilityDao.doesFacilityExist(facility.getId()))
			throw new ActionException(ErrorCode.Facility_ItemDoesNotExist);

		checkCommon(facility);
		return facilityDao.updateFacility(facility);
	}

	public boolean deleteFacility(Long id) throws ActionException {
		DomFacility facility;
		try {
			facility = facilityDao.getFacility(id);
		} catch (NotFoundException e) {
			throw new ActionException(ErrorCode.Facility_ItemDoesNotExist);
		}
		
		String companyCode = facility.getCompany().key().getName();
		
		// Check if there is any beacon left
		PomBeaconSearchParameters params = new PomBeaconSearchParameters();
		params.companyCode = companyCode;
		params.facilityId = id;
		List<DomBeacon> beacons = BeaconDaoImpl.getInstance().getAllBeacons(params);
		if(!beacons.isEmpty()) {
			throw new ActionException(ErrorCode.Facility_BeaconsStillExist);
		}
		
		// Check if there is any notification left
		// If there is no facility left, only notification at company level remain
		List<DomNotification> notifications = NotificationDaoImpl.getInstance().getNotifications(companyCode, id);
		if(!notifications.isEmpty()) {
			throw new ActionException(ErrorCode.Facility_NotificationsStillExist);
		}
		
		return facilityDao.deleteFacility(id);
	}

	private void checkCompany(DomFacility facility) throws ActionException {
		CompanyHelper.checkCompany(facility.getCompany().getKey().getName());
	}
	
	private void checkCommon(DomFacility facility) throws ActionException {
		// TODO: add checks on common fields
		if(!Validator.validateText(facility.getName(), FACILITY_NAME_MAX_LENGTH)) throw new ActionException(ErrorCode.Facility_InvalidName);
		
		if(facility.getAddress() == null)
			throw new ActionException(ErrorCode.Facility_NoAddress);
		else {
			DomAddress address = facility.getAddress();
			if(!StringUtils.isAsciiPrintable(address.streetAddress)) throw new ActionException(ErrorCode.Facility_InvalidAddress);
			if(!StringUtils.isNumeric(address.postalCode)) throw new ActionException(ErrorCode.Facility_InvalidPostalCode);
			if(!StringUtils.isAsciiPrintable(address.city)) throw new ActionException(ErrorCode.Facility_InvalidCity);
			if(!StringUtils.isAsciiPrintable(address.country)) throw new ActionException(ErrorCode.Facility_InvalidCountry);
		}
	}
}
