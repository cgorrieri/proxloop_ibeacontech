package com.ibeacons._02_services;

import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.ibeacons.Permission;
import com.ibeacons.RoleData;
import com.ibeacons.UserRole;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;

/**
 * Permission service that check if the current user has the correct permission.
 * @author Cyril Gorrieri
 *
 */
@Service
public class PermissionService {
	private DomSecurityUser getSecurityUser() {
		return (DomSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	
	/**
	 * Throw an {@link ActionException} with code {@link ErrorCode#User_NotAuthorized}
	 */
	public void throwPermissionException() throws ActionException {
		throw new ActionException(ErrorCode.User_NotAuthorized);
	}
	
	/**
	 * Return whether the user has the authority or not
	 * @param authority
	 * @return
	 */
	public boolean hasAuthority(String authority) {
		return getSecurityUser().getAuthorities().contains(authority);
	}
	
	/**
	 * Same as {@link #hasAuthority(String)} but throw an exception instead
	 * @param authority
	 * @return
	 */
	public void checkAuthority(String authority) {
		if(!hasAuthority(authority)) throwPermissionException();
	}
	
	/**
	 * Return whether the user has the role or not
	 * @param role
	 * @return
	 */
	public boolean hasRole(UserRole role) {
		return getSecurityUser().getRoles().containsKey(role);
	}
	
	/**
	 * Same as {@link #hasAuthority(String)} but throw an exception instead
	 * @param role
	 * @return
	 */
	public void checkRole(UserRole role) {
		if(!hasRole(role)) throwPermissionException();
	}
	
	/**
	 * Return whether the user has the permission, without looking at the values
	 * @param permissionName
	 * @return
	 */
	public boolean hasPermission(Permission permissionName) {
		return getSecurityUser().getPermissions().containsKey(permissionName);
	}
	
	/**
	 * Same as {@link #hasPermission(String)} but throw an exception instead
	 * @param permissionName
	 */
	public void checkPermission(Permission permissionName) {
		if(!hasPermission(permissionName)) {
			throwPermissionException();
		}
	}
	
	/**
	 * Get all values linked to the given permission
	 * @param permissionName
	 * @return A list of values or null if the user does not have the permission at all
	 */
	public List<RoleData> getValuesForPermission(Permission permissionName) {
		return getSecurityUser().getPermissions().get(permissionName);
	}

	/**
	 * Check the permission on a company level
	 * @param permissionName
	 * @param companyCode
	 * @return True if the user has the permission, false otherwise
	 */
	public boolean hasPermissionOnCompany(Permission permissionName, String companyCode) {
		List<RoleData> values = getSecurityUser().getPermissions().get(permissionName);
		if(values == null) return false;
		
		for(RoleData value : values) {
			if(RoleData.ALL.equals(value.companyCode) || companyCode.equals(value.companyCode)) {
				return true;
			}
		}
		// If we are there it means the user has no permission for this company
		return false;
	}

	/**
	 * Check the permission on a company level
	 * @param permissionName
	 * @param companyCode
	 * @throws ActionException if the user does not have the permission, with the ErrorCode.User_NotAuthorized
	 */
	public void checkPermissionOnCompany(Permission permissionName, String companyCode) {
		if(!hasPermissionOnCompany(permissionName, companyCode)) {
			throwPermissionException();
		}
	}
	
	/**
	 * Check the permission on a company notification level
	 * @param permissionName
	 * @param companyCode
	 * @param notificationId
	 * @return True if the user has the permisison, false otherwise
	 */
	public boolean hasPermissionOnCompanyNotification(Permission permissionName, String companyCode, Long notificationId) {
		List<RoleData> values = getSecurityUser().getPermissions().get(permissionName);
		if(values == null) return false;
		
		String notificationIdStr = String.valueOf(notificationId);
		
		for(RoleData value : values) {
			if((RoleData.ALL.equals(value.companyCode) || companyCode.equals(value.companyCode))
					&& value.facilityId == null
					&& (RoleData.ALL.equals(value.notificationId) || notificationIdStr.equals(value.notificationId))) {
				return true;
			}
		}
		// If we are there it means the user has no permission for this company notification
		return false;
	}

	/**
	 * Check the permission on a company notification level
	 * @param permissionName
	 * @param companyCode
	 * @param notificationId
	 * @throws ActionException if the user does not have the permission, with the ErrorCode.User_NotAuthorized
	 */
	public void checkPermissionOnCompanyNotificaiton(Permission permissionName, String companyCode, Long notificationId) {
		if(!hasPermissionOnCompanyNotification(permissionName, companyCode, notificationId)) {
			throwPermissionException();
		}
	}
	
	/**
	 * Check the permission on a company facility level
	 * @param permissionName
	 * @param companyCode
	 * @param facilityId
	 * @return True if the user has the permission, false otherwise
	 */
	public boolean hasPermissionOnCompanyFacility(Permission permissionName, String companyCode, Long facilityId) {
		List<RoleData> values = getSecurityUser().getPermissions().get(permissionName);
		if(values == null) return false;
		
		String facilityIdStr = String.valueOf(facilityId);
		
		for(RoleData value : values) {
			if((RoleData.ALL.equals(value.companyCode) || companyCode.equals(value.companyCode))
					&& (RoleData.ALL.equals(value.facilityId) || facilityIdStr.equals(value.facilityId))) {
				return true;
			}
		}
		// If we are there it means the user has no permission for this company notification
		return false;
	}

	/**
	 * Check the permission on a company facility level
	 * @param permissionName
	 * @param companyCode
	 * @param facilityId
	 * @throws ActionException if the user does not have the permission, with the ErrorCode.User_NotAuthorized
	 */
	public void checkPermissionOnCompanyFacility(Permission permissionName, String companyCode, Long facilityId) {
		if(!hasPermissionOnCompanyFacility(permissionName, companyCode, facilityId)) {
			throwPermissionException();
		}
	}
	

	/**
	 * Check the permission on a company facility level
	 * @param permissionName
	 * @param companyCode
	 * @param facilityId
	 * @param notificationId
	 * @return True if the user has the permission, false otherwise
	 */
	public boolean hasPermissionOnCompanyFacilityNotification(Permission permissionName, String companyCode, Long facilityId, Long notificationId) {
		List<RoleData> values = getSecurityUser().getPermissions().get(permissionName);
		if(values == null) return false;
		
		String facilityIdStr = String.valueOf(facilityId);
		
		for(RoleData value : values) {
			if((RoleData.ALL.equals(value.companyCode) || companyCode.equals(value.companyCode))
					&& (RoleData.ALL.equals(value.facilityId) || facilityIdStr.equals(value.facilityId))
					&& (RoleData.ALL.equals(value.notificationId) || facilityIdStr.equals(value.notificationId))) {
				return true;
			}
		}
		// If we are there it means the user has no permission for this company notification
		return false;
	}

	/**
	 * Check the permission on a company facility notification level
	 * @param permissionName
	 * @param companyCode
	 * @param facilityId
	 * @param notificationId
	 * @throws ActionException if the user does not have the permission, with the ErrorCode.User_NotAuthorized
	 */
	public void checkPermissionOnCompanyFacilityNotification(Permission permissionName, String companyCode, Long facilityId, Long notificationId) {
		if(!hasPermissionOnCompanyFacilityNotification(permissionName, companyCode, facilityId, notificationId)) {
			throwPermissionException();
		}
	}
}