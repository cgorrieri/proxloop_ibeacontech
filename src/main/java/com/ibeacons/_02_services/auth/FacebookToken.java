package com.ibeacons._02_services.auth;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class FacebookToken extends AbstractAuthenticationToken {
	private static final long serialVersionUID = 1768989408932307456L;
	
	private String name;
	
	public FacebookToken(String facebookId) {
		super(null);
		this.name = facebookId;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isAuthenticated() {
		return true;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return null;
	}

}
