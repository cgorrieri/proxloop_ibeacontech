package com.ibeacons._02_services.auth;

import java.util.Collection;
import java.util.logging.Logger;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons.dom.auth.DomSecurityUser;


@Component
public class SocialUserAuthenticationProvider implements AuthenticationProvider {
	private final Logger logger = Logger.getLogger(SocialUserAuthenticationProvider.class.getCanonicalName());
	
    private SecurityUserDao userCatalog = SecurityUserDaoImpl.getInstance();

    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    	logger.info("authenticate:"+authentication);
		DomSecurityUser user;

		// Get the user
		try {
			user = userCatalog.getFacebookUser(authentication.getName());
			logger.info("user found");
		} catch(NotFoundException e) {
			throw new BadCredentialsException("Bad credentials");
		}
		
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
 
        return new UsernamePasswordAuthenticationToken(user, "", authorities);
    }

    public final boolean supports(Class<?> authentication) {
    	return authentication.equals(FacebookToken.class);
    }
}