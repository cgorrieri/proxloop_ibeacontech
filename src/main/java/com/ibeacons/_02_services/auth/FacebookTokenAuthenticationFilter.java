package com.ibeacons._02_services.auth;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.types.User;

public class FacebookTokenAuthenticationFilter extends OncePerRequestFilter {

    private static final Logger logger = Logger.getLogger(FacebookTokenAuthenticationFilter.class.getCanonicalName());
    
    public final String HEADER_SECURITY_TOKEN = "X-FacebookToken";
    
    private AuthenticationManager authenticationManager;
    
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
    	this.authenticationManager = authenticationManager;
    }
    
    private RequestMatcher requiresAuthenticationRequestMatcher;
    
    public final void setRequiresAuthenticationRequestMatcher(String requestMatcher) {
		this.requiresAuthenticationRequestMatcher = new AntPathRequestMatcher(requestMatcher);
	}

    /**
     * Attempt to authenticate user with Facebook token
     */
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
    	String token = request.getHeader(HEADER_SECURITY_TOKEN);
        if(token == null) {
        	logger.info("No Facebook token found");
        	return null;
        }
        
        Authentication userAuthenticationToken = authUserByToken(token);
        if(userAuthenticationToken == null) throw new AuthenticationServiceException(MessageFormat.format("Error | {0}", "Bad Facebook Token"));
        
        return authenticationManager.authenticate(userAuthenticationToken);
    }
 
 
    /**
     * authenticate the user based on token
     * @return
     */
    private Authentication authUserByToken(String token) {
        if(token==null) {
            return null;
        }
        try {
	        FacebookClient facebookClient = new DefaultFacebookClient(token, Version.VERSION_2_4);
	        User facebookUser = facebookClient.fetchObject("me", User.class);
	        return new FacebookToken(facebookUser.getId());
        } catch (Exception e) {
        	return null;
        }
    }

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		if(requiresAuthentication(request, response)) {
			logger.info("Attempt facebook authentication");
			Authentication authentication = attemptAuthentication(request,response);
		    if (authentication != null) {
		    	SecurityContextHolder.getContext().setAuthentication(authentication);
		    }
		}
	    filterChain.doFilter(request, response);
	}
	
	/**
	 * Indicates whether this filter should attempt to process a login request for the
	 * current invocation.
	 * <p>
	 * It strips any parameters from the "path" section of the request URL (such as the
	 * jsessionid parameter in <em>http://host/myapp/index.html;jsessionid=blah</em>)
	 * before matching against the <code>filterProcessesUrl</code> property.
	 * <p>
	 * Subclasses may override for special requirements, such as Tapestry integration.
	 *
	 * @return <code>true</code> if the filter should attempt authentication,
	 * <code>false</code> otherwise.
	 */
	protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
		return requiresAuthenticationRequestMatcher.matches(request);
	}
}
