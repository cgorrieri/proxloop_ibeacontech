package com.ibeacons._02_services.auth;

import java.util.Collection;
import java.util.logging.Logger;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons.dom.auth.DomSecurityUser;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
	private final Logger logger = Logger.getLogger(UserAuthenticationProvider.class.getCanonicalName());
    private SecurityUserDao userCatalog = SecurityUserDaoImpl.getInstance();

    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
    	logger.info("authenticate");
    	if(!supports(authentication.getClass())) return null;
		DomSecurityUser user;
		// Get the user
		try {
			user = userCatalog.getUser(authentication.getName());
		} catch(NotFoundException e) {
			throw new BadCredentialsException("Bad credentials");
		}
		// Compare the passwords
		final String password = String.valueOf(authentication.getCredentials());
		if (!password.equals(user.getPassword())) {
            throw new BadCredentialsException("Bad credentials");
        }
		
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
 
        return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }

    @Override
    public final boolean supports(Class<?> authentication) {
    	return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}