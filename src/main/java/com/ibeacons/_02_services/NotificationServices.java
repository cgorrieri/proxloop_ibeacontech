package com.ibeacons._02_services;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.ibeacons._03_dao.basic_data.BeaconDao;
import com.ibeacons._03_dao.basic_data.BeaconDaoImpl;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDao;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDaoImpl;
import com.ibeacons._03_dao.basic_data.NotificationDao;
import com.ibeacons._03_dao.basic_data.NotificationDaoImpl;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomBeaconNotificationsAssignment;
import com.ibeacons.dom.DomBeaconNotificationsAssignment.NotificationAssignment;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomBeaconSearchParameters;
import com.ibeacons.tools.CompanyHelper;

@Service
public class NotificationServices {
	
	private NotificationDao notificationDao;
	
	public NotificationServices() {
		notificationDao = NotificationDaoImpl.getInstance();
	}

	public DomNotification getNotification(Long id) {
		try {
			return notificationDao.getNotification(id);
		}catch (com.googlecode.objectify.NotFoundException e) {
			throw new ActionException(ErrorCode.Notification_ItemDoesNotExist);
		}
	}

	public List<DomNotification> getNotifications() {
		return notificationDao.getNotifications();
	}
	
	public List<DomNotification> getNotifications(String compnayCode) {
		return notificationDao.getNotifications(compnayCode);
	}
	
	public List<DomNotification> getAllNotifications(String companyCode) {
		CompanyHelper.checkCompany(companyCode);
		// TODO permission restriction
		
		// Otherwise retrieve the restricted list of facilities
		return notificationDao.getNotifications(companyCode);
	}

	public DomNotification addNotification(DomNotification notification) {
		if(notification.getId() != null)
			throw new ActionException(ErrorCode.Notification_NoIdAtCreation);

		checkCommon(notification);
		return notificationDao.insertNotification(notification);
	}

	public void updateNotification(DomNotification notification) {
		if(!notificationDao.doesNotificationExist(notification.getId()))
			throw new ActionException(ErrorCode.Notification_ItemDoesNotExist);

		checkCommon(notification);
		notificationDao.updateNotification(notification);
	}

	public boolean deleteNotification(Long id) {
		if(!notificationDao.doesNotificationExist(id))
			throw new ActionException(ErrorCode.Notification_ItemDoesNotExist);
		
		DomNotification theNotificaiton = notificationDao.getNotification(id);
		
		// Check if the notification is assigned to a beacon
		
		// Get all beacons that can have the notification assigned
		BeaconDao beaconDao = BeaconDaoImpl.getInstance();
		PomBeaconSearchParameters params = new PomBeaconSearchParameters();
		params.companyCode = theNotificaiton.getCompany().getKey().getName();
		
		List<DomBeacon> beacons = beaconDao.getAllBeacons(params);
		
		BeaconNotificationsAssignmentDao beaconAssignementDao = BeaconNotificationsAssignmentDaoImpl.getInstance();
		
		// For all the beacons, we remove the assignment to this notification (if any)
		for(DomBeacon beacon : beacons) {
			// Get the assignments
			DomBeaconNotificationsAssignment assignment = beaconAssignementDao.get(beacon.getId());
			
			// If the notification is assigned, remove it and update the assignment
			Iterator<NotificationAssignment> it = assignment.notifications.iterator();
			boolean isUpdated = false;
			while(it.hasNext()) {
				NotificationAssignment theNotif = it.next();
				// If notification found, remove it and set the flag to true
				if(theNotif.notificationId.equals(id)) {
					isUpdated = true;
					it.remove();
				}
			}
			
			if(isUpdated) {
				beaconAssignementDao.update(assignment);
			}
		}
		
		return notificationDao.deleteNotification(id);
	}
	
	/**
	 * Check the data of the notification
	 * @param notification
	 */
	private void checkCommon(DomNotification notification) {
		ActionException errors = new ActionException();
		
		// Check the facility attached
		if(notification.getFacility() != null) {
			DomFacility theFacility = notification.getFacility().get();
			if(theFacility == null) {
				errors.addError(ErrorCode.Facility_ItemDoesNotExist);
			} else if(theFacility.getCompany().compareTo(notification.getCompany()) != 0) {
				errors.addError(ErrorCode.Notification_InvalidFacility);
			}
		}
		
		// Check the name
		String theName = notification.getName();
		if(StringUtils.isEmpty(theName)) {
			errors.addError(ErrorCode.Notification_MandatoryName);
		}
		// TODO test with regex for specific letters
		else if(theName.length() < 3 || theName.length() > 50)
		{
			errors.addError(ErrorCode.Notification_InvalidName);
		}
		
		// Check the title
		String theTitle = notification.getTitle();
		if(StringUtils.isEmpty(theTitle)) {
			errors.addError(ErrorCode.Notification_MandatoryTitle);
		} 
		// TODO test with regex for specific letters
		else if(theTitle.length() < 3 || theTitle.length() > 50)
		{
			errors.addError(ErrorCode.Notification_InvalidTitle);
		}
		
		// Check the sub title
		String theSubTitle = notification.getSubTitle();
		// TODO test with regex for specific letters
		if(StringUtils.isNotEmpty(theSubTitle) && theSubTitle.length() > 50)
		{
			errors.addError(ErrorCode.Notification_InvalidSubTitle);
		}
		
		// Check the URL if provided
		// TODO find a regex to validate the URL
//		String theCustomWebUrl = notification.getCustomWebUrl();
//		if(StringUtils.isNotEmpty(theCustomWebUrl)
//				&& !UrlValidator.getInstance().isValid(theCustomWebUrl))
//		{
//			errors.addError(ErrorCode.Notification_InvalidCustomUrl);
//		}
		
		// If at least one error has been detected we throw the exception
		if(errors.hasError()) throw errors;
	}
}
