package com.ibeacons._02_services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ibeacons._03_dao.basic_data.BeaconDao;
import com.ibeacons._03_dao.basic_data.BeaconDaoImpl;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDao;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDaoImpl;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomBeaconSearchParameters;
import com.ibeacons.tools.CompanyHelper;

/**
 * Service handling administration for iBeacons
 * 
 * @author Cyril Gorrieri
 */
@Service
public class BeaconsServices {
	
//	private static Index index;
//	
//	static {
//		IndexSpec indexSpec = IndexSpec.newBuilder().setName("beacons").build(); 
//	    index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
//	}
	
	private BeaconDao beaconDao;
	
	public BeaconsServices() {
		beaconDao = BeaconDaoImpl.getInstance();
	}
	
	public List<DomBeacon> getAllBeacons(PomBeaconSearchParameters params) {
		return beaconDao.getAllBeacons(params);
	}

	public DomBeacon getBeacon(Long id) {
		try {
			return beaconDao.getBeacon(id);
		}catch (com.googlecode.objectify.NotFoundException e) {
			throw new ActionException(ErrorCode.Beacon_ItemDoesNotExist);
		}
	}

	public DomBeacon addBeacon(DomBeacon beacon) {
		checkParents(beacon);
		return beaconDao.insertBeacon(beacon);
	}
	
	public DomBeacon updateBeacon(DomBeacon newBeacon) {
		//DomBeacon oldBeacon = getBeacon(newBeacon.getId());
		checkParents(newBeacon);
		return beaconDao.insertBeacon(newBeacon);
	}
	
	private void checkParents(DomBeacon beacon) throws ActionException {
		CompanyHelper.checkCompany(beacon.getCompany().getKey().getName());
		//FacilityHelper.checkFacilityExists(beacon.getFacility().getKey().getName());
	}

	public boolean deleteBeacon(Long id) {
		// Delete beacon assignments
		BeaconNotificationsAssignmentDao assignmentDao = BeaconNotificationsAssignmentDaoImpl.getInstance();
		assignmentDao.delete(id);
		
		return beaconDao.removeBeacon(id);
	}

}
