package com.ibeacons._02_services;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;

@Service
public class EmailService {
	// TODO put in properties file
	private static final String SENDER_EMAIL = "pelagic-region-637@appspot.gserviceaccount.com";
	private static final String SENDER_NAME = "Proxloop";
	
	public void sendEmail(String subject, List<String> receivers, String text, String html) throws UnsupportedEncodingException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		try {
		    Message msg = new MimeMessage(session);
		    msg.setFrom(new InternetAddress(SENDER_EMAIL, SENDER_NAME));
		    for(String receiver : receivers) {
			    msg.addRecipient(Message.RecipientType.TO,
					     new InternetAddress(receiver));
		    }
		    msg.setSubject(subject);
		    msg.setText(text);
		    msg.setContent(html, "text/html");
		    Transport.send(msg);

		} catch (AddressException e) {
		    // ...
		} catch (MessagingException e) {
		    // ...
		}
	}
}
