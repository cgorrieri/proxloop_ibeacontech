package com.ibeacons._02_services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibeacons._03_dao.basic_data.BeaconDaoImpl;
import com.ibeacons._03_dao.basic_data.CompanyDao;
import com.ibeacons._03_dao.basic_data.CompanyDaoImpl;
import com.ibeacons._03_dao.basic_data.FacilityDaoImpl;
import com.ibeacons._03_dao.basic_data.NotificationDaoImpl;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomBeaconSearchParameters;
import com.ibeacons.tools.CompanyHelper;
import com.ibeacons.tools.Validator;

@Service
public class CompanyServices {
	private static final int COMPANY_NAME_MAX_LENGTH = 50;
	
	private CompanyDao companyDao;
	
	@Autowired
	private PermissionService permissionService;
	
	public CompanyServices() {
		companyDao = CompanyDaoImpl.getInstance();
	}
	
	/**
	 * Return a company by its Id
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public DomCompany getCompany(String code) throws ActionException {
		CompanyHelper.checkCode(code);
		try {
			return companyDao.getCompany(code);
		}catch (com.googlecode.objectify.NotFoundException e) {
			throw new ActionException(ErrorCode.Company_ItemDoesNotExist);
		}
	}
	
	/**
	 * Get all companies
	 * @return
	 */
	public List<DomCompany> getCompanies() {
		return companyDao.getCompanies();
	}
	
	/**
	 * Add a new company
	 * @param company
	 * @return
	 */
	public DomCompany addCompany(DomCompany company) {
		// Validate if code is valid
		CompanyHelper.checkCode(company.getCode());
		// If item already exists
		if(companyDao.doesCompanyExist(company.getCode()))
			throw new ActionException(ErrorCode.Company_ItemAlreadyExists);
		commonCheck(company);
		// Insert and return the result
		return companyDao.insertCompany(company);
	}
	
	/**
	 * Update an existed company
	 * @param company
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public DomCompany updateCompany(DomCompany company) throws ActionException {
		// To update the item must exist
		CompanyHelper.checkCode(company.getCode());
		CompanyHelper.checkCompanyExists(company.getCode());
		commonCheck(company);
		return companyDao.updateCompany(company);
	}
	
	/**
	 * Delete an existed company
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public boolean deleteCompany(String code) {
		CompanyHelper.checkCode(code);
		CompanyHelper.checkCompanyExists(code);
		
		// Check if there is any facility left
		List<DomFacility> facilities = FacilityDaoImpl.getInstance().getFacilities(code);
		if(!facilities.isEmpty()) {
			throw new ActionException(ErrorCode.Company_FacilitiesStillExist);
		}
		
		// Check if there is any beacon left
		// If there is no facility left, only beacons at company level remain
		PomBeaconSearchParameters params = new PomBeaconSearchParameters();
		params.companyCode = code;
		List<DomBeacon> beacons = BeaconDaoImpl.getInstance().getAllBeacons(params);
		if(!beacons.isEmpty()) {
			throw new ActionException(ErrorCode.Company_BeaconsStillExist);
		}
		
		// Check if there is any notification left
		// If there is no facility left, only notification at company level remain
		List<DomNotification> notifications = NotificationDaoImpl.getInstance().getNotifications(code);
		if(!notifications.isEmpty()) {
			throw new ActionException(ErrorCode.Company_NotificationsStillExist);
		}
		
		return companyDao.deleteCompany(code);
	}
	
	/**
	 * Check common fields for create and update
	 * @param company
	 * @throws ActionException
	 */
	private void commonCheck(DomCompany company) throws ActionException {
		if(! Validator.validateText(company.getName(), COMPANY_NAME_MAX_LENGTH)) throw new ActionException(ErrorCode.Company_InvalidName);
	}
	
}
