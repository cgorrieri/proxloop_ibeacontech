package com.ibeacons._02_services.cloud_storage;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.ObjectAccessControl;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;

public class CloudStorageService {
	private static Storage storageService;

	/**
	 * Returns an authenticated Storage object used to make service calls to
	 * Cloud Storage.
	 */
	private static Storage getService() throws IOException,
			GeneralSecurityException {
		if (null == storageService) {
			
			JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
			HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();	
			
			// Build service account credential.
			GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
			    .setJsonFactory(JSON_FACTORY)
			    .setServiceAccountId("339837264432-7u06tk8jevpdvaudfjeiiqkdfiu3fg15@developer.gserviceaccount.com")
			    .setServiceAccountScopes(Collections.singleton(StorageScopes.DEVSTORAGE_FULL_CONTROL))
			    .setServiceAccountPrivateKeyFromP12File(new File("WEB-INF/proxloop-4500c227e882.p12"))
			    .build();			

			storageService = new Storage.Builder(httpTransport, JSON_FACTORY, credential)
				.setApplicationName("proxloop-inc").build();
		}
		return storageService;
	}

	/**
	 * Fetches the metadata for the given bucket.
	 *
	 * @param bucketName
	 *            the name of the bucket to get metadata about.
	 * @return a Bucket containing the bucket's metadata.
	 */
	public static Bucket getBucket(String bucketName) throws IOException,
			GeneralSecurityException {
		Storage client = getService();

		Storage.Buckets.Get bucketRequest = client.buckets().get(bucketName);
		// Fetch the full set of the bucket's properties (e.g. include the ACLs
		// in the response)
		bucketRequest.setProjection("full");
		return bucketRequest.execute();
	}

	/**
	 * Fetch a list of the objects within the given bucket.
	 *
	 * @param bucketName  
	 *            the name of the bucket to list.
	 * @return a list of the contents of the specified bucket.
	 */
	public static List<StorageObject> listBucket(String bucketName)
			throws IOException, GeneralSecurityException {
		Storage client = getService();
		Storage.Objects.List listRequest = client.objects().list(bucketName);

		List<StorageObject> results = new ArrayList<StorageObject>();
		Objects objects;

		// Iterate through each page of results, and add them to our results list.
		do {
			objects = listRequest.execute();
			if(objects != null) {
				// Add the items in this page of results to the list we'll return.
				results.addAll(objects.getItems());
			}

			// Get the next page, in the next iteration of this loop.
			listRequest.setPageToken(objects.getNextPageToken());
		} while (null != objects.getNextPageToken());

		return results;
	}

	public static List<StorageObject> listObjects(String prefix,
			String bucketName) throws IOException, GeneralSecurityException {
		Storage client = getService();
		Storage.Objects.List listRequest = client.objects().list(bucketName)
				.setDelimiter("/").setPrefix(prefix);

		List<StorageObject> results = new ArrayList<StorageObject>();
		Objects objects;

		// Iterate through each page of results, and add them to our results list.
		do {
			objects = listRequest.execute();
			// Add the items in this page of results to the list we'll return.
			if(objects.getItems() != null)
				results.addAll(objects.getItems());

			// Get the next page, in the next iteration of this loop.
			listRequest.setPageToken(objects.getNextPageToken());
		} while (null != objects.getNextPageToken());

		return results;
	}

	/**
	 * Uploads data to an object in a bucket.
	 *
	 * @param name
	 *            the name of the destination object.
	 * @param contentType
	 *            the MIME type of the data.
	 * @param stream
	 *            the data - for instance, you can use a FileInputStream to
	 *            upload a file.
	 * @param bucketName
	 *            the name of the bucket to create the object in.
	 */
	public static void uploadStream(String name, String contentType,
			InputStream stream, String bucketName) throws IOException,
			GeneralSecurityException {
		InputStreamContent contentStream = new InputStreamContent(contentType,
				stream);
		StorageObject objectMetadata = new StorageObject()
		// Set the destination object name
				.setName(name)
				// Set the access control list to publicly read-only
				.setAcl(Arrays.asList(new ObjectAccessControl().setEntity(
						"allUsers").setRole("READER")));

		// Do the insert
		Storage client = getService();
		Storage.Objects.Insert insertRequest = client.objects().insert(
				bucketName, objectMetadata, contentStream);

		insertRequest.execute();
	}

	/**
	 * Deletes an object in a bucket.
	 *
	 * @param path
	 *            the path to the object to delete.
	 * @param bucketName
	 *            the bucket the object is contained in.
	 */
	public static void deleteObject(String path, String bucketName)
			throws IOException, GeneralSecurityException {
		Storage client = getService();
		client.objects().delete(bucketName, path).execute();
	}
}
