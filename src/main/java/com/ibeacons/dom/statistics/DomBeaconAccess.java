package com.ibeacons.dom.statistics;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class DomBeaconAccess {

	@Id
	public Long id;
	
	// Prefer to save the id to save some time not compute the key
	public Long beaconId;
	
	// Timestamp in seconds in the system time zone
	@Index
	public Long timestamp;
	
	public String username;
	
	public DomBeaconAccess() { }
	
	public DomBeaconAccess(Long beaconId, Long timestamp, String username) {
		this.beaconId = beaconId;
		this.timestamp = timestamp;
		this.username = username;
	}
}
