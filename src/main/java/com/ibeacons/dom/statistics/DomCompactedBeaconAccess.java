package com.ibeacons.dom.statistics;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class DomCompactedBeaconAccess {

	/**
	 * Id of the statistics is the Id of the referred beacon
	 */
	@Id
	public Long id;
	
	public byte[] protoBufContent;
	
	public DomCompactedBeaconAccess() { }
	
	public DomCompactedBeaconAccess(Long id) {
		this.id = id;
	}
}
