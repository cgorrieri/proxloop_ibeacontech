package com.ibeacons.dom;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class DomCompany extends DomBaseEntity {

	@Id
	private String code;
	
	private String name;
	
	private String logoUrl;

	// default constructor NEEDED for un-serialization
	public DomCompany() {}
	
	public DomCompany(String code, String name) {
		this.code = code;
		this.name = name;
	}

	//region Getters & Setters
	/**
	 * @return the id
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * @param name the name to set
	 */
	public void getCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the logo url
	 */
	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	//endregion
}
