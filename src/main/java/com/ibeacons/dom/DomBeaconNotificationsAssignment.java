package com.ibeacons.dom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class DomBeaconNotificationsAssignment {
	
	public static class NotificationAssignment {
		public Long notificationId;
		// ISO format date time with time zone
		public String startDate;
		public String endDate;
		
		@Override
		public String toString() {
			return "NotificationAssignment [notificationId=" + notificationId
					+ ", startDate=" + startDate + ", endDate=" + endDate + "]";
		}
	}
	
	@Id
	public Long beaconId;
	
	public List<NotificationAssignment> notifications;
	
	public DomBeaconNotificationsAssignment() {
		this.notifications = new ArrayList<DomBeaconNotificationsAssignment.NotificationAssignment>();
	}

	public DomBeaconNotificationsAssignment(Long beaconId) {
		this.beaconId = beaconId;
		this.notifications = new ArrayList<DomBeaconNotificationsAssignment.NotificationAssignment>();
	}

	@Override
	public String toString() {
		return "DomBeaconNotificationsAssignment [beaconId=" + beaconId
				+ ", notifications=" + Arrays.toString(notifications.toArray()) + "]";
	}
	
	
}
