package com.ibeacons.dom;

import java.util.List;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class DomFacility extends DomBaseEntity {

	/**
	 * Code is a concatenation of the company code and the code of the facility
	 */
	@Id
	private Long id;
	
	private String name;
	
	private String description;
	
	private String imageUrl;
	
	private List<String> categories;
	
	/**
	 * Address of the facility
	 */
	private DomAddress address;
	
	/**
	 * Id of the document where the position of facility is stored to enable the search by location
	 */
	private Long locationDocumentId;
	
	/**
	 * Id of the document that describe the plan of the facility
	 */
	private Long planDocumentId;

	/**
	 * The company holding the facility
	 */
	@Index
	private Ref<DomCompany> company;
	
	//region Getters & Setters
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the address
	 */
	public DomAddress getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(DomAddress address) {
		this.address = address;
	}

	/**
	 * @return the locationDocumentId
	 */
	public Long getLocationDocumentId() {
		return locationDocumentId;
	}

	/**
	 * @param locationDocumentId the locationDocumentId to set
	 */
	public void setLocationDocumentId(Long locationDocumentId) {
		this.locationDocumentId = locationDocumentId;
	}

	/**
	 * @return the planDocumentId
	 */
	public Long getPlanDocumentId() {
		return planDocumentId;
	}

	/**
	 * @param planDocumentId the planDocumentId to set
	 */
	public void setPlanDocumentId(Long planDocumentId) {
		this.planDocumentId = planDocumentId;
	}

	/**
	 * @return the company
	 */
	public Ref<DomCompany> getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Ref<DomCompany> company) {
		this.company = company;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the categories
	 */
	public List<String> getCategories() {
		return categories;
	}

	/**
	 * @param categories the categories to set
	 */
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}
	//endregion


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DomFacility [id=" + id + ", name=" + name + ", description="
				+ description + ", imageUrl=" + imageUrl + ", categories="
				+ categories + ", address=" + address + ", locationDocumentId="
				+ locationDocumentId + ", planDocumentId=" + planDocumentId
				+ ", company=" + company + "]";
	}
	
	
}
