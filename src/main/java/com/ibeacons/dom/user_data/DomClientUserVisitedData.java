package com.ibeacons.dom.user_data;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class DomClientUserVisitedData {
	
	class RecievedNotification {
		public Long notificationId;
		public Long expirationTimestamp;
	}
	
	@Id
    private String username;
	
	public List<RecievedNotification> receivedNotifications;
	
	public DomClientUserVisitedData() {
		this.receivedNotifications = new ArrayList<DomClientUserVisitedData.RecievedNotification>();
	}

	//region getters & setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public List<RecievedNotification> getRecievedNotifications() {
		return receivedNotifications;
	}

	public void addReceivedNotification(Long notificationId, Long expirationTimestamp) {
		RecievedNotification receivedNotification = new RecievedNotification();
		receivedNotification.notificationId = notificationId;
		receivedNotification.expirationTimestamp = expirationTimestamp;
		this.receivedNotifications.add(receivedNotification);
	}
	//endregion
}
