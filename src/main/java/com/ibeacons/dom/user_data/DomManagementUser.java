package com.ibeacons.dom.user_data;

import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.ibeacons.dom.auth.DomSecurityUser;

@Entity
public class DomManagementUser {
	
	@Id
    private String username;
	
	@Ignore DomSecurityUser sercurityUser;
    
    /* The user is attached to a specific company */
    private String companyCode;
    private List<Long> facilitiesId;
    
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public DomSecurityUser getSercurityUser() {
		return sercurityUser;
	}
	public void setSercurityUser(DomSecurityUser sercurityUser) {
		this.sercurityUser = sercurityUser;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public List<Long> getFacilitiesId() {
		return facilitiesId;
	}
	public void setFacilitiesId(List<Long> facilitiesId) {
		this.facilitiesId = facilitiesId;
	}
 
    
}
