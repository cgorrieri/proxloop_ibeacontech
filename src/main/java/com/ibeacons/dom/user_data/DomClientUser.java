package com.ibeacons.dom.user_data;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.ibeacons.dom.auth.DomSecurityUser;

@Entity
public class DomClientUser {
	@Id
    private String username;
	
	@Ignore DomSecurityUser sercurityUser;
	
	public String jsonContent;

	//region getters & setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public DomSecurityUser getSercurityUser() {
		return sercurityUser;
	}

	public void setSercurityUser(DomSecurityUser sercurityUser) {
		this.sercurityUser = sercurityUser;
	}

	public String getJsonContent() {
		return jsonContent;
	}

	public void setJsonContent(String jsonContent) {
		this.jsonContent = jsonContent;
	}
	//endregion

	@Override
	public String toString() {
		return "DomClientUser [username=" + username + ", jsonContent=" + jsonContent + "]";
	}
}
