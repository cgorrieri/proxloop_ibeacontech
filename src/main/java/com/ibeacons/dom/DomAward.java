package com.ibeacons.dom;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class DomAward {
	@Id
	public Long id;
	
	public String title;
	public String description;
	public String imageUrl;
	public int loops;
	public String vendor;
	
	// default constructor NEEDED for un-serialization
	public DomAward() {}
	
	public DomAward(Long id, String title, String description, String imageUrl,
			int loops, String vendor) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.imageUrl = imageUrl;
		this.loops = loops;
		this.vendor = vendor;
	}
	
	
}
