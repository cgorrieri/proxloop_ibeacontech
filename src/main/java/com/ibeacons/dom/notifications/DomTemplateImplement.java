package com.ibeacons.dom.notifications;

import java.util.ArrayList;
import java.util.List;


public class DomTemplateImplement {
	// TODO update to DomTemplate when implemented
	public Long templateId;
	
	public List<DomTemplateImplementValue> values;
	
	// default constructor NEEDED for un-serialization
	public DomTemplateImplement() { }

	public DomTemplateImplement(Long templateId) {
		this.templateId = templateId;
		this.values = new ArrayList<DomTemplateImplementValue>();
	}
	
}
