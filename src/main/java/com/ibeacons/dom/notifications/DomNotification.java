package com.ibeacons.dom.notifications;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.ibeacons.dom.DomBaseEntity;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;

@Entity
public class DomNotification extends DomBaseEntity {
	public enum NotificationType {
		TemplateBasedWebView,
		CustomWebUrl,
		Imbedded;
	}
	
	@Id
	private Long id;
	
	/**
	 * The company holding the notification
	 */
	@Index private Ref<DomCompany> company;
	
	/**
	 * If not null the notification should only be used in this facility
	 */
	@Index private Ref<DomFacility> facility;
	
	/**
	 * Name in the administration dashboard
	 */
	private String name;
	
	/**
	 * Title of the notification
	 */
	private String title;
	
	private String subTitle;
	
	private String shortDescription;
	
	private NotificationType notificationType;
	
	/**
	 * Not null if notification type is WebView
	 */
	private DomTemplateImplement templateImplement;
	
	private String customWebUrl;

	// default constructor NEEDED for un-serialization
	public DomNotification() {}
	
	public DomNotification(Long id, Ref<DomCompany> company,
			String name, String title, String shortDescription) {
		this.id = id;
		this.company = company;
		this.name = name;
		this.title = title;
		this.shortDescription = shortDescription;
	}

	//region Getters & Setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ref<DomCompany> getCompany() {
		return company;
	}

	public void setCompany(Ref<DomCompany> company) {
		this.company = company;
	}

	public Ref<DomFacility> getFacility() {
		return facility;
	}

	public void setFacility(Ref<DomFacility> facility) {
		this.facility = facility;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public DomTemplateImplement getTemplateImplement() {
		return templateImplement;
	}

	public void setTemplateImplement(DomTemplateImplement templateImplement) {
		this.templateImplement = templateImplement;
	}

	public String getCustomWebUrl() {
		return customWebUrl;
	}

	public void setCustomWebUrl(String customWebUrl) {
		this.customWebUrl = customWebUrl;
	}

	//endregion Getters & Setters
	

	@Override
	public String toString() {
		return "DomNotification [id=" + id + ", company=" + company
				+ ", facility=" + facility + ", name=" + name + ", title="
				+ title + ", subTitle=" + subTitle + ", shortDescription="
				+ shortDescription + ", notificationType=" + notificationType
				+ ", templateImplement=" + templateImplement
				+ ", customWebUrl=" + customWebUrl + "]";
	}
	
}
