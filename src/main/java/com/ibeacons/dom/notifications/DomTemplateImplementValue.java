package com.ibeacons.dom.notifications;

public class DomTemplateImplementValue {
	public String name;
	public String value;

	// default constructor NEEDED for un-serialization
	public DomTemplateImplementValue() {}
	
	public DomTemplateImplementValue(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
