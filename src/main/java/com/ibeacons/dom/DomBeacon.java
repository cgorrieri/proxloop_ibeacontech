package com.ibeacons.dom;

import java.util.Date;

import org.joda.time.DateTime;

import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class DomBeacon extends DomBaseEntity {
	@Id
	Long id;
	
	private String mac;
	
	private String name;
	
	// Beacon information
	@Index
	private String uuid;
	
	// Contains specification of the beacon
	@Index
	private int major;
	
	@Index
	private int minor;
	
	@Index
	private Ref<DomCompany> company;
	
	/**
	 * The facility where the beacon is put
	 */
	@Index
	private Ref<DomFacility> facility;
	
	// Current battery level, in %
	private byte batteryLevel;
	
	private Date estimatedEndOfBatteryDate;
	
	// Information on the range of the emit
	private int broadcastingPower;
	private int txThreshold; // in -dbm
	
	public DomBeacon() {}
	
	public DomBeacon(Long id, Ref<DomCompany> company, Ref<DomFacility> facility, String mac, String uuid, int major, String name) {
		this.id = id;
		this.company = company;
		this.facility = facility;
		this.mac = mac;
		this.uuid = uuid;
		this.major = major;
		this.name = name;
	}

	//region Getters & setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}
	
	public Ref<DomCompany> getCompany() {
		return company;
	}

	public void setCompany(Ref<DomCompany> company) {
		this.company = company;
	}

	public Ref<DomFacility> getFacility() {
		return facility;
	}

	public void setFacility(Ref<DomFacility> facility) {
		this.facility = facility;
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(byte batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public DateTime getEstimatedEndOfBatteryDate() {
		return new DateTime(estimatedEndOfBatteryDate);
	}

	public void setEstimatedEndOfBatteryDate(DateTime estimatedEndOfBatteryDate) {
		this.estimatedEndOfBatteryDate = estimatedEndOfBatteryDate.toDate();
	}
	
	public void setEstimatedEndOfBatteryDate(long estimatedEndOfBatteryDateTimestamp) {
		this.estimatedEndOfBatteryDate = new Date(estimatedEndOfBatteryDateTimestamp);
	}

	public int getBroadcastingPower() {
		return broadcastingPower;
	}

	public void setBroadcastingPower(int broadcastingPower) {
		this.broadcastingPower = broadcastingPower;
	}

	public int getTxThreshold() {
		return txThreshold;
	}

	public void setTxThreshold(int txThreshold) {
		this.txThreshold = txThreshold;
	}

	//endregion

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DomBeacon [mac=" + mac + ", name=" + name + ", uuid=" + uuid
				+ ", major=" + major + ", minor=" + minor + ", company="
				+ company + ", facility=" + facility + ", batteryLevel=" + batteryLevel
				+ ", estimatedEndOfBatteryDate=" + estimatedEndOfBatteryDate
				+ ", broadcastingPower=" + broadcastingPower + ", txThreshold="
				+ txThreshold + "]";
	}
	
}
