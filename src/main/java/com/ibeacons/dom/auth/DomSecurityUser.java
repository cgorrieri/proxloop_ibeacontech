package com.ibeacons.dom.auth;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.ibeacons.Permission;
import com.ibeacons.RoleData;
import com.ibeacons.UserRole;

@Entity
public class DomSecurityUser implements UserDetails   {
	private static final long serialVersionUID = 1L;
	
	@Id
    private String username;
    private String password;
    
    @Index
    private String email;
    private String firstName;
    private String lastName;
    
    @Index
    private String companyCode;
    
    //@Index
    private String facebookId;
 
    /* Spring Security fields*/
    private List<UserHighLevelRole> authorities;
    private Map<UserRole, List<RoleData>> roles = new HashMap<UserRole, List<RoleData>>();
    private Map<Permission, List<RoleData>> permissions;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;
	
    @Override
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
     
    @Override
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
     
    public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String comapnyCode) {
		this.companyCode = comapnyCode;
	}

	@Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }
     
    public void setAuthorities(List<UserHighLevelRole> authorities) {
        this.authorities = authorities;
    }
 
    public Map<UserRole, List<RoleData>> getRoles() {
		return roles;
	}

	public void setRoles(Map<UserRole, List<RoleData>> roles) {
		this.roles = roles;
	}
	
	public Map<Permission, List<RoleData>> getPermissions() {
		// If the permissions object id null, we build it for convenience
		if(permissions == null) {
			permissions = new HashMap<Permission, List<RoleData>>(roles.size()*2);
			for(Entry<UserRole, List<RoleData>> role: roles.entrySet()) {
				for(Permission perm : role.getKey().permissions) {
					List<RoleData> datas = permissions.get(perm);
					// If the permission already existed, we complete the values
					if(datas != null) {
						datas.addAll(role.getValue());
						permissions.put(perm, datas);
					}
					// Else we just add the permission to the list
					else {
						permissions.put(perm, role.getValue());
					}
				}
			}
		}
		return permissions;
	}
	

	@Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }
     
    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }
     
    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }
     
    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }
 
    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
 
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

	@Override
	public String toString() {
		return "DomSecurityUser [username=" + username + ", password="
				+ password + ", email=" + email + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", authorities=" + authorities
				+ ", permissions=" + roles + ", accountNonExpired="
				+ accountNonExpired + ", accountNonLocked=" + accountNonLocked
				+ ", credentialsNonExpired=" + credentialsNonExpired
				+ ", enabled=" + enabled + "]";
	}
 
    
}
