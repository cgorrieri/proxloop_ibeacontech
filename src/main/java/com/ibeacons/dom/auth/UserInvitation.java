package com.ibeacons.dom.auth;

import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.ibeacons.RoleData;
import com.ibeacons.UserRole;
import com.ibeacons.dom.DomBaseEntity;
import com.ibeacons.pom.PomCompany;

@Entity
public class UserInvitation extends DomBaseEntity {
	/**
	 * All possible actions for the invitation
	 */
	public enum Action {
		// To create a new company with a user with the full rights on it
		CREATE_COMPANY,
		// To create a facility inside a company, with a user that has the full right only on the facility
		CREATE_FACILITY,
		// To create a new user with a set of permissions
		CREATE_USER
	}
	
	/**
	 * Scope of the invitation
	 */
	public static class Scope {
		/**
		 * Refer to the company we want to create a user or a facility in
		 */
		public PomCompany company;
		
		/**
		 * In case of the invitation of a user
		 */
		public Map<UserRole, List<RoleData>> userPermissions;
	}
	
	/**
	 * Generated code to represent the notification
	 */
	@Id
	public String id;
	
	/**
	 * CompanyCode to group invitations
	 */
	@Index
	public String companyCode;
	
	/**
	 * User name of the sender, to notify when invitation done
	 */
	public String senderUsername;
	
	/**
	 * First name of the receiver to put in the mail
	 */
	public String receiverFirstName;
	
	/**
	 * email to send the notification to
	 */
	@Index
	public String email;
	
	public Action action;
	
	public Scope scope;
	
	/**
	 * Date when the invitation will be removed from the system
	 * TODO Cron task to remove the expired invitations
	 */
	public long expirationDate;

	@Override
	public String toString() {
		return "UserInvitation [id=" + id + ", companyCode=" + companyCode
				+ ", senderUsername=" + senderUsername + ", email=" + email
				+ ", action=" + action + ", scope=" + scope
				+ ", expirationDate=" + expirationDate + "]";
	}
	
	
}
