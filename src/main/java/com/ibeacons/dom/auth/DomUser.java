package com.ibeacons.dom.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.ibeacons.dom.DomBaseEntity;

@Entity
public class DomUser extends DomBaseEntity {

	@Id
	private String username;
	
	private Map<String, List<String>> permissions;
	
	public DomUser () {}
	
	public DomUser(String email) {
		this.username = email;
		this.permissions = new HashMap<String, List<String>>();
	}

	//region Getters & setters
	
	public String getEmail() {
		return username;
	}

	public void setEmail(String email) {
		this.username = email;
	}

	public Map<String, List<String>> getPermissions() {
		return permissions;
	}

	public void setPermissions(Map<String, List<String>> permissions) {
		this.permissions = permissions;
	}
	
}
