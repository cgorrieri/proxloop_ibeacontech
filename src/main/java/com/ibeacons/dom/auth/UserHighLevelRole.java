package com.ibeacons.dom.auth;

import org.springframework.security.core.GrantedAuthority;

public class UserHighLevelRole implements GrantedAuthority {
    private static final long serialVersionUID = 1L;
    
    private String name;
 
    //private List<Privilege> privileges;
    
    // default constructor NEEDED for un-serialization
    public UserHighLevelRole() {}
    
    public UserHighLevelRole(String name) {
    	this.name = name;
    }
    
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    @Override
    public String getAuthority() {
        return this.name;
    }
 
//    public List<Privilege> getPrivileges() {
//        return privileges;
//    }
// 
//    public void setPrivileges(List<Privilege> privileges) {
//        this.privileges = privileges;
//    }
 
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Role [name=");
        builder.append(name);
//        builder.append(", privileges=");
//        builder.append(privileges);
        builder.append("]");
        return builder.toString();
    }
}
