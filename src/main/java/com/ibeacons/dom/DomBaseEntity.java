package com.ibeacons.dom;

import java.util.Date;

import com.googlecode.objectify.annotation.OnSave;

public abstract class DomBaseEntity {

	private Date createdAt;
	private Date updatedAt;
	
	@OnSave
	void onSave() {
		if(createdAt == null) {
			createdAt = new Date();
		}
		updatedAt = new Date();
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
