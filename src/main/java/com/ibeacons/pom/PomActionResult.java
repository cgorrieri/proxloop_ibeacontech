package com.ibeacons.pom;

/**
 * Model returned to the client when it perform an action without result
 * @author Cyril Gorrieri
 *
 */
public class PomActionResult {
	/**
	 * If the action is well performed
	 */
	boolean status;
	
	/**
	 * If status false, contains the error
	 */
	String text;
	
	/**
	 * Constructor with only the status
	 * @param status
	 */
	public PomActionResult(boolean status) {
		this(status, null);
	}
	
	/**
	 * Constructor with the status and the explanation text
	 * @param status
	 * @param text
	 */
	public PomActionResult(boolean status, String text) {
		this.status = status;
		this.text = text;
	}

	/**
	 * @return the status
	 */
	public boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
}
