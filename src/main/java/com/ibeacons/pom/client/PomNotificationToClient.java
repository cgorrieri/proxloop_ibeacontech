package com.ibeacons.pom.client;

/**
 * Presentation Object Model representing a Web view based notification
 * @author Cyril Gorrieri
 *
 */
public class PomNotificationToClient {
	public enum NotificationType {
		WebUrl,
		Imbedded;
	}
	
	/**
	 * The real id of the notification (temporary send to the client app)
	 */
	public Long id;
	
	public String title;
	public String shortDescription;
	
	/**
	 * Not null if notification type is Imbedded
	 */
	public String subTitle;
	
	public NotificationType notificationType;
	
	/**
	 * Not null if notification type is WebUrl
	 */
	public String webUrl;
	
	// default constructor NEEDED for un-serialization
	public PomNotificationToClient() { }
	
	public PomNotificationToClient(Long id, String title, String shortDescription) {
		this.id = id;
		this.title = title;
		this.shortDescription = shortDescription;
	}
	
	// Date time in ISO-8601 (example 2013-02-08T09:30:26.123+07:00)
	public String startDate;
	public String endDate;
}
