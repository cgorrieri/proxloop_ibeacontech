package com.ibeacons.pom;

public class PomAddress {
	public String streetAddress;
	public String city;
	public String postalCode;
	public String country;
	
	// GPS location
	// If not provided the server has to compute them
	public Double longitude;
	public Double latitude;
	
	// default constructor NEEDED for un-serialization
	public PomAddress() {}
	
	public PomAddress(String streetAddress, String city,
			String postalCode, String country, Double longitude, Double latitude) {
		this.streetAddress = streetAddress;
		this.city = city;
		this.postalCode = postalCode;
		this.country = country;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	public PomAddress(String streetAddress, String city,
			String postalCode, String country) {
		this(streetAddress, city, postalCode, country, null, null);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PomAddress [streetAddress=" + streetAddress + ", city=" + city
				+ ", postalCode=" + postalCode + ", country=" + country
				+ ", longitude=" + longitude + ", latitude=" + latitude + "]";
	}
	
}
