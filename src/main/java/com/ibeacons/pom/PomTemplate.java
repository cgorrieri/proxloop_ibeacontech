package com.ibeacons.pom;

import java.util.List;

public class PomTemplate {
	public Long id;
	public String name;
	public String htmlContent;
	public String cssContent;
	public List<PomTemplateAttribute> attributes;
}
