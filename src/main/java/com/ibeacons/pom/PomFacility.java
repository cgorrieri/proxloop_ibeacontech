package com.ibeacons.pom;

import java.util.List;

public class PomFacility {
	public Long id;
	public String name;
	public String companyCode;
	public String description;
	public String imageUrl;
	public List<String> categories;
	public PomAddress physicalAddress;
	
	// default constructor NEEDED for un-serialization
	public PomFacility() {}

	public PomFacility(Long id, String name, String companyCode,
			String description, String imageUrl, List<String> categories,
			PomAddress physicalAddress) {
		this.id = id;
		this.name = name;
		this.companyCode = companyCode;
		this.description = description;
		this.imageUrl = imageUrl;
		this.categories = categories;
		this.physicalAddress = physicalAddress;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PomFacility [id=" + id + ", name=" + name + ", companyCode="
				+ companyCode + ", description=" + description + ", imageUrl="
				+ imageUrl + ", categories=" + categories
				+ ", physicalAddress=" + physicalAddress + "]";
	}
	
}
