package com.ibeacons.pom;


public class PomAward {
	public Long id;
	public String title;
	public String description;
	public String imageUrl;
	public int loops;
	public String vendor;
	

	// default constructor NEEDED for un-serialization
	public PomAward() {}
	
	public PomAward(Long id, String title, String description, String imageUrl,
			int loops, String vendor) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.imageUrl = imageUrl;
		this.loops = loops;
		this.vendor = vendor;
	}
}
