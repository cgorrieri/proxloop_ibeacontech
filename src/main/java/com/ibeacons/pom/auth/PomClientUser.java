package com.ibeacons.pom.auth;

public class PomClientUser {
	public String username;
	public String lastName;
	public String firstName;
	public String email;
	public String json;
	
	@Override
	public String toString() {
		return "PomClientUser [username=" + username + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", email=" + email + ", json="
				+ json + "]";
	}
		
}
