package com.ibeacons.pom.auth;

import java.util.ArrayList;
import java.util.List;

public class PomUser {
	public String email;
	public List<PomPermission> permissions;
	
	public PomUser() {}
	
	public PomUser(String email) {
		this.email = email;
		this.permissions = new ArrayList<PomPermission>();
	}
}
