package com.ibeacons.pom.auth;

import com.ibeacons.pom.PomCompany;

public class UserRegisterInvitation {
	public String username;
	public String lastName;
	public String firstName;
	public String email;

	public String password;
	public String verifiedPassword;
	
	public PomCompany company;
}
