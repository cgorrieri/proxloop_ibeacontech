package com.ibeacons.pom.auth;

import com.ibeacons.dom.auth.UserInvitation.Action;
import com.ibeacons.dom.auth.UserInvitation.Scope;

/**
 * Object that define the invitation for someone
 * @author Cyril Gorrieri
 */
public class UserInvitation {
	public String id;
	
	public String email;
	
	public Action action;
	
	public Scope scope;

	public String receiverFirstName;
}
