package com.ibeacons.pom;

import java.util.Comparator;


public class PomBeaconAccess implements Comparator<PomBeaconAccess> {
	public Long beginTime;
	public Long endTime;
	public int hits;
	
	@Override
	public int compare(PomBeaconAccess o1, PomBeaconAccess o2) {
		return o1.beginTime.compareTo(o2.beginTime);
	}
}
