package com.ibeacons.pom.notifications;

/**
 * Presentation Object Model representing a notification
 * @author Cyril Gorrieri
 *
 */
public class PomNotification {
	public enum NotificationType {
		TemplateBasedWebView,
		CustomWebUrl,
		Imbedded;
	}
	
	/**
	 *  Unique identifier the the web view
	 */
	public Long id;
	
	public String companyCode;
	
	/**
	 * Used to restrict the usage of this notification in this facility
	 * Can be null
	 */
	public Long facilityId;
	
	/**
	 * Facility name (used only for retrieve)
	 */
	public String facilityName;

	public String name;
	
	public String title;
	
	public String subTitle;
	
	public String shortDescription;
	
	public NotificationType notificationType;
	
	/**
	 * Not null if notification type is TemplateBasedWebView
	 */
	public PomTemplateImplement templateImplement;
	
	/**
	 * Not null if notification type is CustomWebUrl
	 */
	public String customWebUrl;
	
	// default constructor NEEDED for un-serialization
	public PomNotification() { }

	public PomNotification(Long id, String companyCode,
			String name, String title, String shortDescription) {
		this.id = id;
		this.companyCode = companyCode;
		this.name = name;
		this.title = title;
		this.shortDescription = shortDescription;
	}

	@Override
	public String toString() {
		return "PomNotification [id=" + id + ", companyCode=" + companyCode
				+ ", facilityId=" + facilityId + ", facilityName="
				+ facilityName + ", name=" + name + ", title=" + title
				+ ", subTitle=" + subTitle + ", shortDescription="
				+ shortDescription + ", notificationType=" + notificationType
				+ ", templateImplement=" + templateImplement
				+ ", customWebUrl=" + customWebUrl + "]";
	}

	
}
