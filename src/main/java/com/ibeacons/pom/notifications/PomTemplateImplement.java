package com.ibeacons.pom.notifications;

import java.util.ArrayList;
import java.util.List;

/**
 * Presentation Object Model representing a Web view based notification
 * @author Cyril Gorrieri
 *
 */
public class PomTemplateImplement {
	
	public Long templateId;
	
	public List<PomTemplateImplementValue> values;
	
	public String displayUrl;
	
	// default constructor NEEDED for un-serialization
	public PomTemplateImplement() {}
	
	public PomTemplateImplement(Long templateId) {
		this.templateId = templateId;
		this.values = new ArrayList<PomTemplateImplementValue>();
	}
}
