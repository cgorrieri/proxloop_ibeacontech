package com.ibeacons.pom.notifications;

public class PomTemplateImplementValue {
	public String name;
	public String value;

	// default constructor NEEDED for un-serialization
	public PomTemplateImplementValue() {}
	
	public PomTemplateImplementValue(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
