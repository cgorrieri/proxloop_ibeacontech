package com.ibeacons.pom;

/**
 * Object that gather all criteria to look for a beacon
 * 
 * @author Cyril Gorrieri
 */
public class PomBeaconSearchParameters {
	// TIP Use object to set to null if parameter is not used
	
	public String companyCode;
	public Long facilityId;
	public String uuid;
	public Short major;
	public Long batteryLevelLowerThan;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PomBeaconSearchParameters [companyCode=" + companyCode
				+ ", facilityId=" + facilityId + ", uuid=" + uuid + ", major="
				+ major + ", batteryLevelLowerThan=" + batteryLevelLowerThan
				+ "]";
	}
}
