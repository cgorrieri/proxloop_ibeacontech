package com.ibeacons.pom;

import java.io.Serializable;

/**
 * Presentation Object Model representing a company
 * @author Cyril Gorrieri
 *
 */
public class PomCompany implements Serializable {
	private static final long serialVersionUID = 7919595056447883580L;
	
	/**
	 *  Unique identifier the the company
	 */
	public String code;
	/**
	 * Name of the company
	 */
	public String name;
	
	public String logoUrl;
	
	// default constructor NEEDED for un-serialization
	public PomCompany() { }
	
	public PomCompany(String code, String name) {
		this.code = code;
		this.name = name;
	}
}
