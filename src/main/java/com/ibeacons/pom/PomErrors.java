package com.ibeacons.pom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;

public class PomErrors implements Serializable {
	private static final long serialVersionUID = -6991421940885619983L;
	
	public class PomError implements Serializable {
		private static final long serialVersionUID = 1928560292721417433L;
		
		public int code;
		public String message;
		public PomError(int code, String message) {
			this.code = code;
			this.message = message;
		}
	}
	
	@JsonProperty("errors")
	public List<PomError> _errors;
	
	
	public PomErrors(int code, String message) {
		_errors = new ArrayList<PomErrors.PomError>(1);
		_errors.add(new PomError(code, message));
	}

	public PomErrors(ActionException ex) {
		_errors = new ArrayList<PomErrors.PomError>(ex.getErrors().size());
		for(ErrorCode e : ex.getErrors()) {
			_errors.add(new PomError(e.getCode(), e.getDescription()));
		}
	}
}
