package com.ibeacons.pom;

public class PomBeacon {
	
	public Long id;

	public String companyId;
	public Long facilityId;
	public String facilityName;
	
	// Basic information
	public String mac;
	public String name;
	public String uuid;
	public Integer major;
	public Integer minor;
	// Battery level according to the beacon vendor software
	public Integer batteryLevel;
	// Estimated out of battery time by the vendor software
	public Long estimatedOutOfBatteryTime;
	// Information on the range of the emit
	public Integer broadcastingPower;
	public Integer txThreshold; // in -dbm
	
	public PomBeacon() {}
	
	public PomBeacon(Long id, String companyId, Long facilityId, String mac, String uuid,
			int major, Integer minor, String name, int batteryLevel,
			long estimatedOutOfBatteryTime, int broadcastingPower,
			int txThreshold) {
		this.id = id;
		this.companyId = companyId;
		this.facilityId = facilityId;
		this.mac = mac;
		this.name = name;
		this.uuid = uuid;
		this.major = major;
		this.minor = minor;
		this.batteryLevel = batteryLevel;
		this.estimatedOutOfBatteryTime = estimatedOutOfBatteryTime;
		this.broadcastingPower = broadcastingPower;
		this.txThreshold = txThreshold;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PomBeacon [id=" + id + ", companyId=" + companyId
				+ ", facilityId=" + facilityId + ", facilityName="
				+ facilityName + ", mac=" + mac + ", name=" + name + ", uuid="
				+ uuid + ", major=" + major + ", minor=" + minor
				+ ", batteryLevel=" + batteryLevel
				+ ", estimatedOutOfBatteryTime=" + estimatedOutOfBatteryTime
				+ ", broadcastingPower=" + broadcastingPower + ", txThreshold="
				+ txThreshold + "]";
	}

	
}
