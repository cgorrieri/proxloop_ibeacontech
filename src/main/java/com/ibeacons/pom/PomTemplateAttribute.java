package com.ibeacons.pom;

public class PomTemplateAttribute {
	public String name;
	public String attribute;
	public String type;
	
	public PomTemplateAttribute(String name, String attribute, String type) {
		super();
		this.name = name;
		this.attribute = attribute;
		this.type = type;
	}
	
	
}
