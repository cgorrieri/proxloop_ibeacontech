package com.ibeacons._00_presentation.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.protobuf.InvalidProtocolBufferException;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._03_dao.stats.BeaconAccessDao;
import com.ibeacons._03_dao.stats.BeaconAccessDaoImpl;
import com.ibeacons._03_dao.stats.CompactedBeaconAccessDao;
import com.ibeacons._03_dao.stats.CompactedBeaconAccessDaoImpl;
import com.ibeacons.dom.statistics.DomBeaconAccess;
import com.ibeacons.dom.statistics.DomCompactedBeaconAccess;
import com.ibeacons.proto.BeaonHitStatiscis.BeaconHitStat;
import com.ibeacons.proto.BeaonHitStatiscis.BeaconHitStat.HitCount;

@Controller
@RequestMapping("/tasks/beacons")
public class BeaconsTasksEndpoint extends BasicEndpoint {
	private static Logger LOGGER = Logger.getLogger(BeaconsTasksEndpoint.class.getCanonicalName());

	@RequestMapping(value = "/addAccess", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void addBeaconAccess(@RequestParam("beaconId") Long beaconId, @RequestParam("username") String username, @RequestParam("timestamp") Long timestamp) {
		LOGGER.info("Add Beacon access task");
		BeaconAccessDaoImpl.getInstance().insertObject(new DomBeaconAccess(beaconId,timestamp,username));
	}
	
	@RequestMapping(value = "/compact/{diff}", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public void compactBeaconAccess(@PathVariable Long diff) {
		LOGGER.info("Compact Beacon access task");
		
    	// Get the timestamp in second of the second before that the job execute
    	Long endTimestamp = new Date().getTime() / 1000 - 1;
    	// Diff is the time in second between 2 run of the task
    	Long startTimestamp = endTimestamp - diff;
    	
    	BeaconAccessDao beaconAccessDao = BeaconAccessDaoImpl.getInstance();
    	
		List<DomBeaconAccess> accesses = beaconAccessDao.getListBefore(endTimestamp);
		// prepare the list of id to remoe
		List<Long> idToRemove = new ArrayList<Long>(accesses.size());
		// Group accesses by beacon
		Map<Long, Integer> listByBeaconId = new HashMap<Long, Integer>();
		
		for(DomBeaconAccess ba : accesses) {
			// Add in the list to remove after the treatment
			idToRemove.add(ba.id);
			// Increase the count for this beacon
			Integer count = listByBeaconId.get(ba.beaconId);
			if(count == null) {
				count = 0;
			}
			listByBeaconId.put(ba.beaconId, count+1);
		}
		
		CompactedBeaconAccessDao compactedDataDao = CompactedBeaconAccessDaoImpl.getInstance();
		
		// Update statistics for beacons
		for(Long beaconId : listByBeaconId.keySet()) {
			BeaconHitStat beaconHitStat = BeaconHitStat.newBuilder().build();
			// Get the DomCompactedBeaconAccess
			DomCompactedBeaconAccess theCompactedData = compactedDataDao.get(beaconId);
			
			HitCount newCount = HitCount.newBuilder()
					.setStartTimestamp(startTimestamp)
					.setEndTimestamp(endTimestamp)
					.setCount(listByBeaconId.get(beaconId))
					.build();
			
			// If do not exist create one
			if(theCompactedData == null) {
				theCompactedData = new DomCompactedBeaconAccess(beaconId);
			}
			// Else get it from data
			else {
				try {
					beaconHitStat = BeaconHitStat.parseFrom(theCompactedData.protoBufContent);
				} catch (InvalidProtocolBufferException e) {
					LOGGER.severe("Cannot decode protobuf for beacon "+beaconId+" - skip action");
					continue;
				}
			}
			// Add the new count
			beaconHitStat = BeaconHitStat.newBuilder(beaconHitStat).addHits(newCount).build();
			
			LOGGER.info("Protobuf for beacon: "+beaconId+"\n"+beaconHitStat.toString());
			
			// Compact the data and save the new object
			theCompactedData.protoBufContent = beaconHitStat.toByteArray();
			compactedDataDao.insertObject(theCompactedData);
		}
		
		beaconAccessDao.deleteObjects(idToRemove);
	}
}
