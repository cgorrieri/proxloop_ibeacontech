package com.ibeacons._00_presentation;

import java.security.Principal;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ibeacons._03_dao.auth.ClientUserDaoImpl;
import com.ibeacons._03_dao.auth.ManagementUserDaoImpl;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.dom.user_data.DomClientUser;
import com.ibeacons.dom.user_data.DomManagementUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomErrors;

public class BasicEndpoint {
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public @ResponseBody PomErrors handleError(Exception ex)
	{
		if (ex instanceof ActionException) {
			ActionException actionException = (ActionException) ex;
			return new PomErrors(actionException);
		}
	    return new PomErrors(ErrorCode.Unknown.getCode(), ExceptionUtils.getStackTrace(ex));
	}
	
	protected static DomManagementUser getManagementUser(Principal user) {
		DomSecurityUser secUser = SecurityUserDaoImpl.getInstance().getUser(user.getName());
		DomManagementUser theUser = ManagementUserDaoImpl.getInstance().getUser(user.getName());
		theUser.setSercurityUser(secUser);
		return theUser;
	}
	
	protected static DomClientUser getClientUser(Principal user) {
		DomSecurityUser secUser = SecurityUserDaoImpl.getInstance().getUser(user.getName());
		DomClientUser theUser = ClientUserDaoImpl.getInstance().getUser(user.getName());
		theUser.setSercurityUser(secUser);
		return theUser;
	}
}
