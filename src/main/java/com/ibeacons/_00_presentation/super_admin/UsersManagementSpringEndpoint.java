package com.ibeacons._00_presentation.super_admin;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;

import com.google.apphosting.api.ApiProxy;
import com.ibeacons._02_services.EmailService;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons._03_dao.auth.UserInvitationDao;
import com.ibeacons._03_dao.auth.UserInvitationDaoImpl;
import com.ibeacons._03_dao.basic_data.CompanyDao;
import com.ibeacons._03_dao.basic_data.CompanyDaoImpl;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.auth.UserInvitation;

@Controller
@RequestMapping("/admin/users")
public class UsersManagementSpringEndpoint {
	private static final Logger LOGGER = Logger.getLogger(UsersManagementSpringEndpoint.class.getName());
	
// Temporary class to update the data of each user
//	@RequestMapping(value="/updateManagementUserPermission", method = RequestMethod.GET)
//	@ResponseStatus(code = HttpStatus.OK)
//	public void updateManagementUserPermission() {
//		// Get all management User of the app
//		
//		List<DomSecurityUser> secUsers = ObjectifyDAO.ofy().load().type(DomSecurityUser.class).list();
//		for(DomSecurityUser u : secUsers) {
//			// We want only manager user
//			boolean found = false;
//			for(GrantedAuthority ga : u.getAuthorities()) {
//				if(ga.getAuthority().equals("ROLE_MANAGER")) {
//					found = true;
//					break;
//				}
//			}
//			if(!found) continue;
//			
//			String companyCode = u.getRoles().get(UserRole.MANAGE_COMPANY).get(0);
//			
//			Map<String, List<String>> permissions = new HashMap<String, List<String>>(8);
//			List<String> companyLevelValues = Arrays.asList(companyCode);
//			// Permission to manage the user of the company
//			permissions.put(UserRole.MANAGE_USER, companyLevelValues);
//			permissions.put(UserRole.DELETE_USER, companyLevelValues);
//			// Permission to manage the company
//			permissions.put(UserRole.MANAGE_COMPANY, companyLevelValues);
//			permissions.put(UserRole.DELETE_COMPANY, companyLevelValues);
//			permissions.put(UserRole.RETRIEVE_COMPANY, companyLevelValues);
//			
//			// Permission to create object under the company
//			permissions.put(UserRole.CREATE_FACILITY, companyLevelValues);
//			permissions.put(UserRole.CREATE_COMPANY_NOTIFICATION, companyLevelValues);
//			permissions.put(UserRole.CREATE_COMPANY_BEACON, companyLevelValues);
//			
//			// Permission to manage all facilities of the company
//			String secondLevelValuePattern = companyCode + UserRole.SEPARATOR + UserRole.WILDCARD;
//			List<String> secondLevelValues = Arrays.asList(secondLevelValuePattern);
//			permissions.put(UserRole.MANAGE_FACILITY, secondLevelValues);
//			permissions.put(UserRole.DELETE_FACILITY, secondLevelValues);
//			permissions.put(UserRole.RETRIEVE_FACILITY, secondLevelValues);
//			
//			// Permissions to manage beacons under the company
//			permissions.put(UserRole.MANAGE_COMPANY_BEACON, secondLevelValues);
//			permissions.put(UserRole.DELETE_COMPANY_BEACON, secondLevelValues);
//			permissions.put(UserRole.RETRIEVE_COMPANY_BEACON, secondLevelValues);
//			
//			// Permissions to manage notification under the company
//			permissions.put(UserRole.MANAGE_COMPANY_NOTIFICATION, secondLevelValues);
//			permissions.put(UserRole.DELETE_COMPANY_NOTIFICATION, secondLevelValues);
//			permissions.put(UserRole.RETRIEVE_COMPANY_NOTIFICATION, secondLevelValues);
//			
//			// Permission to create object under a facility
//			permissions.put(UserRole.CREATE_FACILITY_BEACON, secondLevelValues);
//			permissions.put(UserRole.CREATE_FACILITY_NOTIFICATION, secondLevelValues);
//			
//			String thirdLevelValuePattern = secondLevelValuePattern + UserRole.SEPARATOR + UserRole.WILDCARD;
//			
//			// Permissions to manage notifications under a facility
//			List<String> thirdLevelValues = Arrays.asList(thirdLevelValuePattern);
//			permissions.put(UserRole.MANAGE_FACILITY_NOTIFICATION, thirdLevelValues);
//			permissions.put(UserRole.DELETE_FACILITY_NOTIFICATION, thirdLevelValues);
//			permissions.put(UserRole.RETRIEVE_FACILITY_NOTIFICATION, thirdLevelValues);
//			
//			// Permissions to manage beacons under a facility
//			permissions.put(UserRole.MANAGE_FACILITY_BEACON, thirdLevelValues);
//			permissions.put(UserRole.DELETE_FACILITY_BEACON, thirdLevelValues);
//			permissions.put(UserRole.RETRIEVE_FACILITY_BEACON, thirdLevelValues);
//			u.setRoles(permissions);
//			ObjectifyDAO.ofy().save().entity(u).now();
//		}
//	}
	
	/**
	 * Update all users with the same data to force the re-indexation
	 */
	@RequestMapping(value="/reindex", method = RequestMethod.GET)
	@ResponseStatus(code = HttpStatus.OK)
	public void reindexUsers() {
		SecurityUserDao dao = SecurityUserDaoImpl.getInstance();
		List<DomSecurityUser> allUsers = dao.getUsers();
		for(DomSecurityUser user : allUsers) {
			dao.updateUser(user);
		}
	}

	/**
	 * 30 days before the invitation is removed from the system
	 * TODO move to properties
	 */
	private static final long EXPIRATION_DELAY = 1000 * 60 * 60 * 24 * 30; 
	
	@Inject
	private EmailService emailService;
	
	@Inject
	private SpringResourceTemplateResolver templateResolver;
	
	/**
	 * Create an invitation
	 * @param invitation
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/invite", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.OK)
	public void invite(@RequestBody UserInvitation invitation) throws UnsupportedEncodingException {
		// Create the invitation
		com.ibeacons.dom.auth.UserInvitation dom = new com.ibeacons.dom.auth.UserInvitation();
		dom.id = RandomStringUtils.randomAlphanumeric(30);
		// Set username of the sender
		dom.senderUsername = "__ADMIN__";
		
		// Check if the email is already used by an user
		if(SecurityUserDaoImpl.getInstance().doesEmailExist(invitation.email)) {
			throw new ActionException(ErrorCode.User_EmailAlreadyExists);
		}
		// Check if an invitation has not been sent already
		if(UserInvitationDaoImpl.getInstance().doesEmailExist(invitation.email)) {
			throw new ActionException(ErrorCode.Invitation_EmailAlreadyExists);
		}
		dom.email = invitation.email;
		
		dom.action = invitation.action;
		// Scope
		dom.scope = invitation.scope;
		
		dom.expirationDate = new Date().getTime() + EXPIRATION_DELAY;
		UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
		invitationDao.insert(dom);
		
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		
		// Build URLs
		String baseUrl = "https://"+ApiProxy.getCurrentEnvironment().getAttributes().get("com.google.appengine.runtime.default_version_hostname");
		String linkUrl = baseUrl + "/platform/#/register-invitation/"+dom.id+"/"+dom.email;
		String logoUrl = baseUrl + "/platform/images/logo_name_small.png";
		
		// Add the link to the context for the templates
		Context ctx = new Context();
		ctx.setVariable("linkUrl", linkUrl);
		ctx.setVariable("logoUrl", logoUrl);
		ctx.setVariable("receiverFirstName", dom.receiverFirstName);
		
		// Instantiate the templates for the email
		// TODO improve templates
		String text = templateEngine.process("email_invite_company.txt", ctx);
		String html = templateEngine.process("email_invite_company.html", ctx);
		
		// Send the email with the link to the invitation
		emailService.sendEmail("Invitation", Arrays.asList(invitation.email), text, html);
	}
}