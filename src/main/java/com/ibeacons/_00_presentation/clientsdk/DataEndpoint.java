package com.ibeacons._00_presentation.clientsdk;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._03_dao.basic_data.CompanyDao;
import com.ibeacons._03_dao.basic_data.CompanyDaoImpl;
import com.ibeacons._03_dao.basic_data.FacilityDao;
import com.ibeacons._03_dao.basic_data.FacilityDaoImpl;
import com.ibeacons.adapters.PomCompanyAdapter;
import com.ibeacons.adapters.PomFacilityAdapter;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.pom.PomCompany;
import com.ibeacons.pom.PomFacility;

@Component
@RequestMapping(value="/client")
public class DataEndpoint {
	
	@RequestMapping(value="/companies/", method=RequestMethod.GET)
	public @ResponseBody List<PomCompany> getCompanies() {
		CompanyDao companyDao = CompanyDaoImpl.getInstance();
		List<DomCompany> doms = companyDao.getCompanies();
		PomCompanyAdapter companyAdapter = new PomCompanyAdapter();
		return companyAdapter.toPoms(doms);
	}
	
	@RequestMapping(value="/facilities/", method=RequestMethod.GET)
	public @ResponseBody List<PomFacility> getFacilities() {
		FacilityDao facilityDao = FacilityDaoImpl.getInstance();
		List<DomFacility> doms = facilityDao.getFacilities();
		PomFacilityAdapter facilityAdapter = new PomFacilityAdapter();
		return facilityAdapter.toPoms(doms);
	}
}
