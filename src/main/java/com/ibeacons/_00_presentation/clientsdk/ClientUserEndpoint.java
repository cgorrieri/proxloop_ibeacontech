package com.ibeacons._00_presentation.clientsdk;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._03_dao.auth.ClientUserDao;
import com.ibeacons._03_dao.auth.ClientUserDaoImpl;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons.adapters.PomClientUserAdapter;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.dom.auth.UserHighLevelRole;
import com.ibeacons.dom.user_data.DomClientUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.auth.PomClientUser;
import com.ibeacons.pom.auth.PomClientUserRegistration;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.User;

@Controller
public class ClientUserEndpoint extends BasicEndpoint {
	static PomClientUserAdapter userAdapter = new PomClientUserAdapter();
	
	@RequestMapping(value = "/client/users/authenticate", method = RequestMethod.GET)
	public @ResponseBody PomClientUser authenticate(Principal user) {
		return userAdapter.toPom(getClientUser(user));
	}

	/**
	 * Basic registration with email and password
	 * @param registredUser
	 * @return
	 */
	@RequestMapping(value = "/users/client/register", method = RequestMethod.POST)
	public @ResponseBody PomClientUser register(@RequestBody PomClientUserRegistration registredUser) {
		// TODO put this logic under layer
		
		// TODO add full checks for the login format, password, email etc
		if(!registredUser.password.equals(registredUser.verifiedPassword)) {
			throw new ActionException(ErrorCode.User_PasswordsDoNotMatch);
		}
		
		// Create the real user
		DomSecurityUser theSecUser = new DomSecurityUser();
		theSecUser.setUsername(registredUser.username);
		theSecUser.setPassword(registredUser.password);
		theSecUser.setEmail(registredUser.email);
		theSecUser.setFirstName(registredUser.firstName);
		theSecUser.setLastName(registredUser.lastName);
		List<UserHighLevelRole> roles = new ArrayList<UserHighLevelRole>(1);
		roles.add(new UserHighLevelRole("ROLE_CLIENT"));
		theSecUser.setAuthorities(roles);
		theSecUser.setUsername(registredUser.username);
		
		// Register the security user
		SecurityUserDao userSecDao = SecurityUserDaoImpl.getInstance();
		userSecDao.insertUser(theSecUser);
		
		// Register the data of the user
		DomClientUser theUser = new DomClientUser();
		theUser.setUsername(registredUser.username);
		theUser.setSercurityUser(theSecUser);
		ClientUserDao  userDao = ClientUserDaoImpl.getInstance();
		try {
			userDao.insertUser(theUser);
		} catch (Exception e) {
			// If the creation of the company fails, fall back the registration of the user
			userSecDao.deleteUser(theSecUser.getUsername());
			throw new ActionException(ErrorCode.ClientUser_DataNotCreated);
		}
		
		
		return userAdapter.toPom(theUser);
	}
	
	
	@RequestMapping(value = "/users/client/registerWithFacebook", method = RequestMethod.POST)
	public @ResponseBody PomClientUser register(@RequestBody String facebookToken) {
		User facebookUser = null;
		try {
	        FacebookClient facebookClient = new DefaultFacebookClient(facebookToken, Version.VERSION_2_4);
	        facebookUser = facebookClient.fetchObject("me", User.class, Parameter.with("fields", "id,first_name,last_name"));
        } catch (Exception e) {
        	throw new ActionException(ErrorCode.User_InvalidFacebookToken);
        }
		
		// Create the real user
		DomSecurityUser theSecUser = new DomSecurityUser();
		
		// Generate a username and check that it does not exist
		String username = "";
		do {
			username = RandomStringUtils.randomNumeric(15);
		} while (SecurityUserDaoImpl.getInstance().doesUserExist(username));
		theSecUser.setUsername(username);
		
		// No password as not a login/password user
		//theSecUser.setPassword();
		theSecUser.setFacebookId(facebookUser.getId());
		theSecUser.setEmail(facebookUser.getEmail());
		theSecUser.setFirstName(facebookUser.getFirstName());
		theSecUser.setLastName(facebookUser.getLastName());
		List<UserHighLevelRole> roles = new ArrayList<UserHighLevelRole>(1);
		roles.add(new UserHighLevelRole("ROLE_CLIENT"));
		theSecUser.setAuthorities(roles);
		
		// Register the security user
		SecurityUserDao userSecDao = SecurityUserDaoImpl.getInstance();
		userSecDao.insertUser(theSecUser);
		
		// Register the data of the user
		DomClientUser theUser = new DomClientUser();
		theUser.setUsername(username);
		theUser.setSercurityUser(theSecUser);
		// TODO add content about from the facebook profile
		ClientUserDao userDao = ClientUserDaoImpl.getInstance();
		try {
			userDao.insertUser(theUser);
		} catch (Exception e) {
			// If the creation of the company fails, fall back the registration of the user
			userSecDao.deleteUser(theSecUser.getUsername());
			throw new ActionException(ErrorCode.ClientUser_DataNotCreated);
		}
		
		
		return userAdapter.toPom(theUser);
	}
}
