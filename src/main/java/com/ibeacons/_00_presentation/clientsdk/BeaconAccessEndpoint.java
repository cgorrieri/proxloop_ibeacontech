package com.ibeacons._00_presentation.clientsdk;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._03_dao.basic_data.BeaconDaoImpl;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDaoImpl;
import com.ibeacons._03_dao.basic_data.NotificationDao;
import com.ibeacons._03_dao.basic_data.NotificationDaoImpl;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomBeaconNotificationsAssignment;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.pom.client.PomNotificationToClient;
import com.ibeacons.servlets.NotificationServlet;
import com.ibeacons.tools.EnvHelper;

@Controller
@RequestMapping(value="/client/beacons")
public class BeaconAccessEndpoint extends BasicEndpoint {
	private static final Logger LOGGER = Logger.getLogger(BeaconAccessEndpoint.class.getCanonicalName());

	@RequestMapping(value="/notification/{uuid}/{major}/{minor}", method=RequestMethod.GET)
	public @ResponseBody List<PomNotificationToClient> getNotificationForBeacon(
			@PathVariable("uuid") String uuid,
			@PathVariable("major") int major,
			@PathVariable("minor") int minor,
			Principal user)
	{
		LOGGER.info("getNotificationForBeacon: "+uuid+"/"+major+"/"+minor);
		// Find the beacon
		DomBeacon beacon =  BeaconDaoImpl.getInstance().getBeacon(uuid, major, minor);
		
		// If here, we found the beacon
		// Add a task for the statistics
		Queue queue = QueueFactory.getQueue("beacons-queue");
        queue.add(TaskOptions.Builder.withUrl("/tasks/beacons/addAccess")
        		.param("beaconId", String.valueOf(beacon.getId()))
        		.param("username", "user.getName()")
        		.param("timestamp", String.valueOf(System.currentTimeMillis()/1000)));
		
        // Look for the assigned notification
		DomBeaconNotificationsAssignment theDom = BeaconNotificationsAssignmentDaoImpl.getInstance().get(beacon.getId());
		
		// If here, we fount the assigned notification object
		// Build the output
		List<PomNotificationToClient> theResult = new ArrayList<PomNotificationToClient>(theDom.notifications.size());
		NotificationDao notificationDao = NotificationDaoImpl.getInstance();
		for(DomBeaconNotificationsAssignment.NotificationAssignment aNotifAssignment : theDom.notifications) {
			try {
				// Retrieve the associated notification
				DomNotification aNotification = notificationDao.getNotification(aNotifAssignment.notificationId);
			
				// Create the object to set in the result
				PomNotificationToClient aNotificationToClient = new PomNotificationToClient(aNotification.getId(), aNotification.getTitle(), aNotification.getShortDescription());
				
				// Set the notification data
				switch(aNotification.getNotificationType()) {
				case TemplateBasedWebView:
					aNotificationToClient.notificationType = PomNotificationToClient.NotificationType.WebUrl;
					aNotificationToClient.webUrl = EnvHelper.getRootUrl() + NotificationServlet.getUrlFor(aNotification.getId());
					break;
				case CustomWebUrl:
					aNotificationToClient.notificationType = PomNotificationToClient.NotificationType.WebUrl;
					aNotificationToClient.webUrl = aNotification.getCustomWebUrl();
					break;
				case Imbedded:
					aNotificationToClient.notificationType = PomNotificationToClient.NotificationType.Imbedded;
					aNotificationToClient.subTitle = aNotification.getSubTitle();
					aNotificationToClient.webUrl = aNotification.getCustomWebUrl();
					break;
				default:
					break;
				}
				
				// Set the date data
				aNotificationToClient.startDate = aNotifAssignment.startDate;
				aNotificationToClient.endDate = aNotifAssignment.endDate;
				
				theResult.add(aNotificationToClient);
			} catch (NotFoundException e) {
				continue;
			}
		}
		
		
		return theResult;
	}
}
