package com.ibeacons._00_presentation.utils;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * @author Cyril Gorrieri
 *
 * This class is a simple filter for CORS used in the process of the security
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE) // To be applied before the spring security
public class SecurityCorsFilter extends OncePerRequestFilter  {

	public void destroy() {}

	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		// Set headers for CORS with authentication
		// TODO: Get more info about security to check this setup
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS, PUT, POST, DELETE");
		//response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		response.setHeader("Access-Control-Allow-Credentials", "true");
		
		// If it is an OPTIONS request we have to return OK
		// Because it is asking for permission to target the backend from another url
		if (request.getMethod().equals("OPTIONS")) {
	        try {
	            response.getWriter().print("OK");
	            response.getWriter().flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    } else {
	    	// Other wise we just continue the filtering to the authentication
	    	filterChain.doFilter(request, response);
	    }
	}
}
