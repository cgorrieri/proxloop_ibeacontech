//package com.ibeacons._00_presentation.utils;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.stereotype.Component;
//
//@Component
//public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
//
//    @Override
//    public void commence(HttpServletRequest request, HttpServletResponse response,
//            AuthenticationException authException ) throws IOException, ServletException {
//        String contentType = request.getContentType();
//        System.out.println("RestAuthenticationEntryPoint: content type="+contentType);
//        response.sendError( HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized" );
//    }
// 
//}