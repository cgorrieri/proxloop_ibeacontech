package com.ibeacons._00_presentation.administration;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.NotificationCatalogController;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.notifications.PomNotification;

@Controller
@RequestMapping("/management/notifications")
public class NotificationManagementEndpoint extends BasicEndpoint {
	private static Logger LOGGER = Logger.getLogger(NotificationManagementEndpoint.class.getCanonicalName());
	
	@Inject
	private NotificationCatalogController notificationController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<PomNotification> getNotifications(@RequestParam Map<String, String> attributes) {
		if(attributes.containsKey("company")) {
			return notificationController.getAllNotifications(attributes.get("company"));
		}
		return null;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody PomNotification getNotification(@PathVariable("id") Long id) {
		return notificationController.getNotification(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody PomNotification addNotification(@RequestBody PomNotification notification) {
		LOGGER.log(Level.INFO, "addNotification:"+ notification);
		return notificationController.addNotification(notification);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody PomActionResult updateNotification(@PathVariable("id") Long id, @RequestBody PomNotification notification) {
		return notificationController.updateNotification(notification);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody PomActionResult deleteNotification(@PathVariable("id") Long id) {
		return notificationController.deleteNotification(id);
	}
}
