package com.ibeacons._00_presentation.administration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.CompanyCatalogController;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomCompany;

@Controller
@RequestMapping("/management/companies")
public class CompanyManagementEndpoint extends BasicEndpoint {
	
	@Inject
	private CompanyCatalogController companyController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<PomCompany> getCompanies() {
		return companyController.getCompanies();
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.GET)
	public @ResponseBody PomCompany getCompany(@PathVariable("code") String code) {
			return companyController.getCompany(code);
	}
	
//	@RequestMapping(value = "/", method = RequestMethod.POST)
//	public @ResponseBody PomCompany addCompany(@RequestBody PomCompany company) {
//			return companyController.addCompany(company);
//	}
	
	@RequestMapping(value = "/{code}", method = RequestMethod.PUT)
	public @ResponseBody PomCompany updateCompany(@PathVariable("code") String code, @RequestBody PomCompany company) {
			return companyController.updateCompany(company);
	}

	@RequestMapping(value = "/{code}", method = RequestMethod.DELETE)
	public @ResponseBody PomActionResult deleteCompany(@PathVariable("code") String code) {
			return companyController.deleteCompany(code);
	}
}
