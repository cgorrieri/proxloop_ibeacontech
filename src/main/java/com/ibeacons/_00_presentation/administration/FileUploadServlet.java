package com.ibeacons._00_presentation.administration;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.ibeacons._02_services.cloud_storage.CloudStorageService;
import com.ibeacons.servlets.CrossOriginServletHelper;
import com.ibeacons.tools.PropertyHolder;

public class FileUploadServlet extends HttpServlet {
	private static final long serialVersionUID = 4924825345334425211L;
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		CrossOriginServletHelper.checkOrigin(this, req, res);
		
		// Get application context to get the property
		// Need to do that because Servlets are managed outside of the Spring context
		ApplicationContext appContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		String picturesBucket = appContext.getBean("propertyHolder", PropertyHolder.class).getPicturesBucket();
		
		// folder path
		String prefix = "";
		
		ServletFileUpload upload;
		FileItemIterator iterator;
		FileItemStream item;
		InputStream stream = null;
		try {
			upload = new ServletFileUpload();
			res.setContentType("text/plain");

			iterator = upload.getItemIterator(req);
				
			item = iterator.next();
			stream = item.openStream();
			
			// If it is a form field and the prefix
			if (item.isFormField() && "prefix".equals(item.getFieldName())) {
				prefix = IOUtils.toString(stream);
				
				// We now get the next item
				item = iterator.next();
				stream = item.openStream();
			}
			
			// If the next token is actually the file
			if("file".equals(item.getFieldName())) {
				String name = prefix + item.getName();
				CloudStorageService.uploadStream(name, item.getContentType(), stream, picturesBucket);

				// Write the name in the output
				PrintWriter output = res.getWriter();
				output.append(name);
			}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
	}
}
