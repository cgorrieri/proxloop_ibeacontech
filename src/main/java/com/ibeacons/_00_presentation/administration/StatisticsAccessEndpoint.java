package com.ibeacons._00_presentation.administration;

import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.google.protobuf.InvalidProtocolBufferException;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._03_dao.stats.CompactedBeaconAccessDao;
import com.ibeacons._03_dao.stats.CompactedBeaconAccessDaoImpl;
import com.ibeacons.dom.statistics.DomCompactedBeaconAccess;
import com.ibeacons.pom.PomBeaconAccess;
import com.ibeacons.proto.BeaonHitStatiscis.BeaconHitStat;
import com.ibeacons.proto.BeaonHitStatiscis.BeaconHitStat.HitCount;

@Controller
@RequestMapping("/management/statistics")
public class StatisticsAccessEndpoint extends BasicEndpoint {
	CompactedBeaconAccessDao compactedBeaconAccessDao = CompactedBeaconAccessDaoImpl.getInstance();
	
	private static final ISO8601DateFormat DateTimeFormat = new ISO8601DateFormat();

	@RequestMapping(value = "/beacon/{id}/{begintime}/{endtime}", method = RequestMethod.GET)
	public @ResponseBody List<PomBeaconAccess> getBeaconHits(
			@PathVariable("id") Long id, @PathVariable("begintime") String beginTime, @PathVariable("endtime") String endTime, Principal user)
					throws ParseException, InvalidProtocolBufferException {
		//DomManagementUser theUser = getManagementUser(user);
		
		// Get local time from the dates
		Long beginTimestamp = getLocalTimestamp(beginTime);
		Long endTimestamp = getLocalTimestamp(endTime);
		
		// Get the compacted data
		DomCompactedBeaconAccess compatedData = compactedBeaconAccessDao.get(id);
		BeaconHitStat data = BeaconHitStat.parseFrom(compatedData.protoBufContent);
		
		// Filter the result to return only what is needed
		List<PomBeaconAccess> result = new ArrayList<PomBeaconAccess>();
		for(HitCount count : data.getHitsList()) {
				// If the begin date of the count is within the range
			if((count.getStartTimestamp() > beginTimestamp && count.getStartTimestamp() < endTimestamp)
				// Or if the begin date is in the range of the count 
				|| (count.getStartTimestamp() < beginTimestamp && count.getEndTimestamp() > beginTimestamp)) {
				PomBeaconAccess pom = new PomBeaconAccess();
				pom.beginTime = count.getStartTimestamp();
				pom.endTime = count.getEndTimestamp();
				pom.hits = count.getCount();
				result.add(pom);
			}
		}
		
		Collections.sort(result, new PomBeaconAccess());
		
		return result;
	}
	
	private Long getLocalTimestamp(String date) throws ParseException {
		DateTime dateTime = new DateTime(DateTimeFormat.parse(date));
		
		LocalDateTime localDateTime = dateTime.toLocalDateTime();
		return localDateTime.toDate().getTime() / 1000;
	}
}
