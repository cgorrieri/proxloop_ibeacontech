package com.ibeacons._00_presentation.administration;

import java.security.Principal;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.AwardCatalogController;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomAward;

@Controller
@RequestMapping(value="/management/rewards")
public class RewardManagementEndpoint extends BasicEndpoint {

	private AwardCatalogController awardController = new AwardCatalogController();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<PomAward> getAwards(Principal user) {
		return awardController.getAwards(getManagementUser(user));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody PomAward getAward(@PathVariable("id") long id) {
		return awardController.getAward(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody PomAward addAward(@RequestBody PomAward award) {
		return awardController.addAward(award);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody PomAward updateAward(@RequestBody PomAward award) {
		return awardController.updateAward(award);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody PomActionResult deleteAward(@PathVariable("id") long id) {
		return awardController.deleteAward(id);
	}
	
}
