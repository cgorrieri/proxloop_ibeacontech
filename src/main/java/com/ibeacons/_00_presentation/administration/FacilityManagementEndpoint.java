package com.ibeacons._00_presentation.administration;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.FacilityCatalogController;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomFacility;

@Controller
@RequestMapping("/management/facilities")
public class FacilityManagementEndpoint extends BasicEndpoint {
	private static final Logger LOGGER = Logger.getLogger(FacilityManagementEndpoint.class.getCanonicalName());
	
	@Inject
	private FacilityCatalogController facilityController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public @ResponseBody List<PomFacility> getFacilities(@RequestParam Map<String, String> attributes) {
		if(attributes.containsKey("company")) {
			return facilityController.getFacilities(attributes.get("company"));
		}
		return facilityController.getFacilities();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody PomFacility getFacility(@PathVariable("id") Long id) {
		return facilityController.getFacility(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody PomFacility addFacility(@RequestBody PomFacility facility) {
		return facilityController.addFacility(facility);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody PomFacility updateFacility(@PathVariable("id") Long id, @RequestBody PomFacility facility) {
		return facilityController.updateFacility(facility);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody PomActionResult deleteFacility(@PathVariable("id") Long id) {
		return facilityController.deleteFacility(id);
	}
}
