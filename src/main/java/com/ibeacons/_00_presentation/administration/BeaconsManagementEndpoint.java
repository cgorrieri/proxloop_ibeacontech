package com.ibeacons._00_presentation.administration;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.BeaconCatalogController;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDao;
import com.ibeacons._03_dao.basic_data.BeaconNotificationsAssignmentDaoImpl;
import com.ibeacons.dom.DomBeaconNotificationsAssignment;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomBeacon;
import com.ibeacons.pom.PomBeaconSearchParameters;

@Controller
@RequestMapping("/management/beacons")
public class BeaconsManagementEndpoint extends BasicEndpoint {
	private static Logger LOGGER = Logger.getLogger(BeaconsManagementEndpoint.class.getCanonicalName());
	
	@Inject
	private BeaconCatalogController beaconController;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody List<PomBeacon> getBeacons(@RequestBody PomBeaconSearchParameters filter, Principal user) {
		LOGGER.info("getBeacons: filter="+filter);
		return beaconController.getBeacons(filter);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody PomBeacon getBeacon(@PathVariable("id") Long id) {
		LOGGER.info("getBeacon: id="+id);
		return beaconController.getBeacon(id);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public @ResponseBody PomBeacon addBeacon(@RequestBody PomBeacon beacon) {
		return beaconController.addBeacon(beacon);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public @ResponseBody PomBeacon updateBeacon(@RequestBody PomBeacon beacon) {
		return beaconController.updateBeacon(beacon);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public @ResponseBody PomActionResult deleteBeacon(@PathVariable("id") Long id) {
		return beaconController.deleteBeacon(id);
	}
	

	@RequestMapping(value = "/{id}/assign", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.OK)
	public void assignNotificationsToBeacon(@PathVariable("id") Long beaconId, @RequestBody DomBeaconNotificationsAssignment.NotificationAssignment[] assignments) {
		BeaconNotificationsAssignmentDao theBeaconNotificationAssignmentDao = BeaconNotificationsAssignmentDaoImpl.getInstance();
		DomBeaconNotificationsAssignment theDom = null;
		boolean alreadyExist = false;
		try {
			theDom = theBeaconNotificationAssignmentDao.get(beaconId);
			alreadyExist = true;
		} catch (Exception e) {
			theDom = new DomBeaconNotificationsAssignment(beaconId);
		}
		theDom.notifications = Arrays.asList(assignments);
		if(alreadyExist) {
			theBeaconNotificationAssignmentDao.update(theDom);
		} else {
			theBeaconNotificationAssignmentDao.insert(theDom);
		}
	}
	
	@RequestMapping(value = "/{id}/assign", method = RequestMethod.GET)
	public @ResponseBody List<DomBeaconNotificationsAssignment.NotificationAssignment> getNotificationsAssignedToBeacon(@PathVariable("id") Long beaconId) {
		try {
			BeaconNotificationsAssignmentDao theBeaconNotificationAssignmentDao = BeaconNotificationsAssignmentDaoImpl.getInstance();
			DomBeaconNotificationsAssignment theDom = theBeaconNotificationAssignmentDao.get(beaconId);
			return theDom.notifications;
		} catch (Exception e) {
			return new ArrayList<DomBeaconNotificationsAssignment.NotificationAssignment>();
		}
	}
}
