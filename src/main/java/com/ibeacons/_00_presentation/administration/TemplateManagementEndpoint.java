package com.ibeacons._00_presentation.administration;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._01_controllers.administration.TemplateCatalogController;
import com.ibeacons.pom.PomTemplate;

@Controller
@RequestMapping("/management/templates")
public class TemplateManagementEndpoint extends BasicEndpoint {

	private TemplateCatalogController templatesController = new TemplateCatalogController();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<PomTemplate> getTemplates() {
		return templatesController.getTemplates();
	}
}
