package com.ibeacons._00_presentation.administration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ibeacons.Permission;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.pom.auth.PomManagementUser;

@Controller
public class ManagementUserEndpoint extends BasicEndpoint {
	
	//private static final Logger LOGGER = Logger.getLogger(ManagementUserEndpoint.class.getCanonicalName());
	
	@Inject
	private PermissionService permissionService;
	
	/**
	 * Main method to authenticate the user
	 * @return
	 */
	@RequestMapping(value = "/management/users/authenticate", method = RequestMethod.GET)
	public @ResponseBody PomManagementUser authenticate() {
		DomSecurityUser userSec = (DomSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return toPomManagementUser(userSec);
	}
	
	@RequestMapping(value = "/management/users", method = RequestMethod.GET)
	public @ResponseBody List<PomManagementUser> getUsers(@RequestParam Map<String, String> attributes) {
		String companyCode = attributes.get("company");

		if(companyCode != null) {
			permissionService.checkPermissionOnCompany(Permission.RETRIEVE_USERS, companyCode);
			
			Logger.getAnonymousLogger().info("company attribute provided: " + companyCode);
			SecurityUserDao userCatalog = SecurityUserDaoImpl.getInstance();
			List<DomSecurityUser> doms = userCatalog.getUsers(companyCode);
			List<PomManagementUser> poms = new ArrayList<PomManagementUser>(doms.size());
			for(DomSecurityUser dom : doms) {
				poms.add(toPomManagementUser(dom));
			}
			return poms;
		}
		return new ArrayList<PomManagementUser>(0);
	}
	
	// TODO move in an adapter
	private PomManagementUser toPomManagementUser(DomSecurityUser userSec) {
		PomManagementUser theResult = new PomManagementUser();
		// TODO return permission list when UI can use it
		theResult.username = userSec.getUsername();
		theResult.firstName = userSec.getFirstName();
		theResult.lastName = userSec.getLastName();
		theResult.email = userSec.getEmail();
		theResult.companyCode = userSec.getCompanyCode();
		return theResult;
	}
}
