package com.ibeacons._00_presentation.administration;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.HandlerMapping;

import com.google.api.services.storage.model.StorageObject;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._02_services.cloud_storage.CloudStorageService;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.PomMedia;

/**
 * Controller in charge of the picture management
 * @author Cyril Gorrieri
 *
 */
@Controller
@RequestMapping("/management/pictures")
public class PicturesManagementEndpoint extends BasicEndpoint {
//	private static Logger LOGGER = Logger.getLogger(PicturesManagementEndpoint.class.getCanonicalName());
	
	@Value("${buckets.pictures}")
	private String picturesBucket;

	/**
	 * Retrieve all pictures that have a the given prefix
	 * @param prefix path to look to
	 * @return
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	@RequestMapping(value = "/**", method = RequestMethod.GET)
	public @ResponseBody List<PomMedia> getPictures(HttpServletRequest request) throws IOException, GeneralSecurityException {
		// get the full path /management/pictures/rest/of/the/request
		String resource = (String) request.getAttribute( HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE );
		// remove the beginning to keep only what we need
		String prefix = resource.replace("/management/pictures/", "");
		
		// The list of storage object
		List<StorageObject> storageObjects = CloudStorageService.listObjects(prefix, picturesBucket);
		
		// Convert this object in our common objects
		List<PomMedia> result = new ArrayList<PomMedia>(storageObjects.size());
		for(StorageObject object : storageObjects) {
			PomMedia media = new PomMedia();
			media.resourceId = object.getName();
			media.name = media.resourceId.replace(prefix, "");
			media.publicUrl = "http://storage.googleapis.com/"+picturesBucket+"/"+media.resourceId;
			result.add(media);
		}
		return result;
	}
	
	/**
	 * Delete a resource by its full name
	 * the full name is all the path content
	 */
	@RequestMapping(value = "/**", method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.OK)
	public void getNotification(HttpServletRequest request) {
		// get the full path /management/pictures/rest/of/the/request
		String resource = (String) request.getAttribute( HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE );
		// remove the beginning to keep only what we need
		resource = resource.replace("/management/pictures/", "");
		// Actually delete the object in the 
		try {
			CloudStorageService.deleteObject(resource, picturesBucket);
		} catch (Exception e) {
			throw new ActionException(ErrorCode.Picture_UnableToDelete);
		}
	}
	
	// TODO: check how to create a webn service for upload
}
