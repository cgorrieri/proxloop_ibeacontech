package com.ibeacons._00_presentation.administration;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;

import com.google.apphosting.api.ApiProxy;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons.Permission;
import com.ibeacons.RoleData;
import com.ibeacons.UserRole;
import com.ibeacons._00_presentation.BasicEndpoint;
import com.ibeacons._02_services.EmailService;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons._03_dao.auth.SecurityUserDao;
import com.ibeacons._03_dao.auth.SecurityUserDaoImpl;
import com.ibeacons._03_dao.auth.UserInvitationDao;
import com.ibeacons._03_dao.auth.UserInvitationDaoImpl;
import com.ibeacons._03_dao.basic_data.CompanyDao;
import com.ibeacons._03_dao.basic_data.CompanyDaoImpl;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.dom.auth.UserHighLevelRole;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;
import com.ibeacons.pom.auth.UserInvitation;
import com.ibeacons.pom.auth.UserRegisterInvitation;
import com.ibeacons.tools.CompanyHelper;

@Controller
public class UserInvitationEndpoint extends BasicEndpoint {
	
	private static final Logger LOGGER = Logger.getLogger(UserInvitationEndpoint.class.getCanonicalName());
	
	/**
	 * 30 days before the invitation is removed from the system
	 * TODO move to properties
	 */
	private static final long EXPIRATION_DELAY = 1000 * 60 * 60 * 24 * 30; 
	
	@Inject
	private PermissionService permissionService;
	
	@Inject
	private EmailService emailService;
	
	@Inject
	private SpringResourceTemplateResolver templateResolver;
	
	/**
	 * Get all invitations
	 * @param attributes Filters
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/management/invitations", method = RequestMethod.GET)
	public @ResponseBody List<UserInvitation> getInvitationsList(@RequestParam Map<String, String> attributes) {
		String companyCode = (String) attributes.get("company");
		if(companyCode != null) {
			permissionService.checkPermissionOnCompany(Permission.RETRIEVE_INVITATIONS, companyCode);
			
			UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
			List<com.ibeacons.dom.auth.UserInvitation> doms = invitationDao.getList(companyCode);
			List<UserInvitation> resultList = new ArrayList<UserInvitation>(doms.size());
			for(com.ibeacons.dom.auth.UserInvitation dom : doms) {
				UserInvitation pom = new UserInvitation();
				pom.id = dom.id;
				pom.action = dom.action;
				pom.email = dom.email;

				resultList.add(pom);
			}
			return resultList;
		}
		
		return new ArrayList<UserInvitation>(0);
	}
	
	/**
	 * Create an invitation
	 * @param invitation
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/management/invitations", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.OK)
	public void createInvitation(@RequestBody UserInvitation invitation) throws UnsupportedEncodingException {
		// Check permission
		switch (invitation.action) {
		case CREATE_FACILITY:
			permissionService.checkPermission(Permission.CREATE_INVITATION_NEW_FACILITY_USER);
			break;

		case CREATE_USER:
			permissionService.checkPermission(Permission.CREATE_INVITATION_NEW_USER);
			break;
		// A user of a company can't invite another company
		case CREATE_COMPANY:
			throw new ActionException(ErrorCode.User_NotAuthorized);
		}
		
		// TODO verify invitation data
		
		// Check if the email is already used by an user
		if(SecurityUserDaoImpl.getInstance().doesEmailExist(invitation.email)) {
			throw new ActionException(ErrorCode.User_EmailAlreadyExists);
		}
		
		// Check if an invitation has not been sent already
		if(UserInvitationDaoImpl.getInstance().doesEmailExist(invitation.email)) {
			throw new ActionException(ErrorCode.Invitation_EmailAlreadyExists);
		}
		
		// Create the invitation
		com.ibeacons.dom.auth.UserInvitation dom = new com.ibeacons.dom.auth.UserInvitation();
		dom.id = RandomStringUtils.randomAlphanumeric(30);
		// Set company and username of the sender
		DomSecurityUser userSec = (DomSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		dom.companyCode = userSec.getCompanyCode();
		dom.senderUsername = userSec.getUsername();
		dom.receiverFirstName = invitation.receiverFirstName;
		dom.email = invitation.email;
		dom.action = invitation.action;
		// Scope
		dom.scope = invitation.scope;
		
		dom.expirationDate = new Date().getTime() + EXPIRATION_DELAY;
		UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
		invitationDao.insert(dom);
		
		sendInvitationEmail(dom);
	}
	
	/**
	 * Resend an invitation
	 * @param code
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/management/resend-invitation/{code}", method = RequestMethod.PUT)
	@ResponseStatus(code = HttpStatus.OK)
	public void resendInvitation(@PathVariable String code) throws UnsupportedEncodingException {
		com.ibeacons.dom.auth.UserInvitation dom = getDomUserInvitation(code);
		
		// Check if the user can manage the invitation
		permissionService.checkPermissionOnCompany(Permission.MANAGE_INVITATION, dom.companyCode);
		
		sendInvitationEmail(dom);
	}

	/**
	 * Send the invitation email
	 * @param dom
	 * @throws UnsupportedEncodingException
	 */
	private void sendInvitationEmail(com.ibeacons.dom.auth.UserInvitation dom) throws UnsupportedEncodingException {
		TemplateEngine templateEngine = new TemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		
		// Build URLs
		String baseUrl = "https://"+ApiProxy.getCurrentEnvironment().getAttributes().get("com.google.appengine.runtime.default_version_hostname");
		String linkUrl = baseUrl + "/platform/#/register-invitation/"+dom.id+"/"+dom.email;
		String logoUrl = baseUrl + "/platform/images/logo_name_small.png";
		
		DomSecurityUser userSec = (DomSecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		CompanyDao companyDao = CompanyDaoImpl.getInstance();
		DomCompany company = companyDao.getCompany(dom.companyCode);
		
		// Add the link to the context for the templates
		Context ctx = new Context();
		ctx.setVariable("linkUrl", linkUrl);
		ctx.setVariable("logoUrl", logoUrl);
		ctx.setVariable("receiverFirstName", dom.receiverFirstName);
		ctx.setVariable("senderFirstName", userSec.getFirstName());
		ctx.setVariable("senderLastName", userSec.getLastName());
		ctx.setVariable("companyName", company.getName());
		
		// Instantiate the templates for the email
		// TODO improve templates
		String text = templateEngine.process("email_invite_user.txt", ctx);
		LOGGER.info(text);
		String html = templateEngine.process("email_invite_user.html", ctx);
		LOGGER.info(html);
		
		// Send the email with the link to the invitation
		emailService.sendEmail("Invitation", Arrays.asList(dom.email), text, html);
	}
	
	/**
	 * Get invitation details as user
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/management/invitations/{code}", method = RequestMethod.GET)
	public @ResponseBody UserInvitation getInvitation(@PathVariable String code) {
		com.ibeacons.dom.auth.UserInvitation dom = getDomUserInvitation(code);
		
		// Check the permission
		permissionService.checkPermissionOnCompany(Permission.RETRIEVE_INVITATION, dom.companyCode);
		
		return toPom(dom);
	}
	
	/**
	 * Get invitation details
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/public/invitations/{code}/{email:.+}", method = RequestMethod.GET)
	public @ResponseBody UserInvitation getPublicInvitation(@PathVariable String code, @PathVariable String email) {
		com.ibeacons.dom.auth.UserInvitation dom = getDomUserInvitation(code);

		// If the email provided is not the same as the one in the invitation
		// Then it is not the right person that tries to access the invitation
		if(!dom.email.equals(email)) {
			throw new ActionException(ErrorCode.Invitation_ItemDoesNotExist);
		}
		
		return toPom(dom);
	}
	
	/**
	 * Get the DOM object of the invitation
	 * @param code
	 * @return
	 * @throws ActionException If the object does not exist with code ErrorCode.Invitation_ItemDoesNotExist
	 */
	private com.ibeacons.dom.auth.UserInvitation getDomUserInvitation(String code) {
		UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
		try {
			return invitationDao.get(code);
		} catch(NotFoundException e) {
			throw new ActionException(ErrorCode.Invitation_ItemDoesNotExist);
		}
	}
	
	/**
	 * Convert UserInvitation DOM to POM
	 * @param dom
	 * @return
	 */
	private UserInvitation toPom(com.ibeacons.dom.auth.UserInvitation dom) {
		UserInvitation pom = new UserInvitation();
		pom.email = dom.email;
		pom.scope = dom.scope;
		if(pom.scope != null && pom.scope.company != null) {
			CompanyDao companyDao = CompanyDaoImpl.getInstance();
			DomCompany theCompany = companyDao.getCompany(pom.scope.company.code);
			pom.scope.company.name = theCompany.getName();
			pom.scope.company.logoUrl = theCompany.getLogoUrl();
		}
		pom.action = dom.action;
		return pom;
	}
	
	/**
	 * Delete invitation
	 * @param code
	 */
	@RequestMapping(value = "/management/invitations/{code}", method = RequestMethod.DELETE)
	@ResponseStatus(code = HttpStatus.OK)
	public void deleteInvitation(@PathVariable String code) {
		com.ibeacons.dom.auth.UserInvitation dom = getDomUserInvitation(code);
		
		// Check if the user has the right to delete the invitation
		permissionService.checkPermissionOnCompany(Permission.DELETE_INVITATION, dom.companyCode);
		
		UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
		invitationDao.delete(code);
	}

	/**
	 * Fulfill the invitation
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/public/fulfill-invitation/{code}", method = RequestMethod.POST)
	@ResponseStatus(code = HttpStatus.OK)
	public void fullfilInvitation(@PathVariable String code, @RequestBody UserRegisterInvitation register) {
		// Get the invitation data from the database (avoid data corruption from client)
		com.ibeacons.dom.auth.UserInvitation dom = getDomUserInvitation(code);
		
		// Verify the user information
		verifyUser(register);
		
		switch(dom.action) {
		case CREATE_COMPANY:
			createUserAndCompany(register);
			break;
		case CREATE_FACILITY:
			break;
		case CREATE_USER:
			DomSecurityUser theSecUser = new DomSecurityUser();
			theSecUser.setUsername(register.username);
			theSecUser.setPassword(register.password);
			theSecUser.setEmail(register.email);
			theSecUser.setFirstName(register.firstName);
			theSecUser.setLastName(register.lastName);
			List<UserHighLevelRole> roles = new ArrayList<UserHighLevelRole>(1);
			roles.add(new UserHighLevelRole("ROLE_MANAGER"));
			theSecUser.setAuthorities(roles);			
			theSecUser.setRoles(dom.scope.userPermissions);
			theSecUser.setCompanyCode(dom.scope.company.code);
			SecurityUserDaoImpl.getInstance().insertUser(theSecUser);
			break;
		}
		
		// Now we can delete the invitation
		UserInvitationDao invitationDao = UserInvitationDaoImpl.getInstance();
		invitationDao.delete(code);
	}

	/**
	 * Create the company and the admin user
	 * @param registredUser
	 */
	private void createUserAndCompany(UserRegisterInvitation registredUser) {
		// Set the company code, also used in permission values
		String companyCode = CompanyHelper.generateValidCode();
		
		// Register the security user
		SecurityUserDao userSecDao = SecurityUserDaoImpl.getInstance();
		DomSecurityUser theSecUser = createUserAdminForCompany(registredUser, companyCode);
		userSecDao.insertUser(theSecUser);
			
		// create associated company
		CompanyDao companyDao = CompanyDaoImpl.getInstance();
		DomCompany theCompany = new DomCompany(companyCode, registredUser.company.name);

		try {
			companyDao.insertCompany(theCompany);
		} catch (Exception e) {
			// If the creation of the company fails, fall back the registration of the user
			userSecDao.deleteUser(theSecUser.getUsername());
			throw new ActionException(ErrorCode.ManagUser_CompanyNotCreated);
		}
	}

	/**
	 * Create the admin user for a company with all the roles and permissions
	 * @param registredUser
	 * @param companyCode
	 * @return
	 */
	private DomSecurityUser createUserAdminForCompany(UserRegisterInvitation registredUser, String companyCode) {
		DomSecurityUser theSecUser = new DomSecurityUser();
		theSecUser.setUsername(registredUser.username);
		theSecUser.setPassword(registredUser.password);
		theSecUser.setEmail(registredUser.email);
		theSecUser.setFirstName(registredUser.firstName);
		theSecUser.setLastName(registredUser.lastName);
		List<UserHighLevelRole> categories = new ArrayList<UserHighLevelRole>(1);
		categories.add(new UserHighLevelRole("ROLE_MANAGER"));
		theSecUser.setAuthorities(categories);
		theSecUser.setCompanyCode(companyCode);
		
		Map<UserRole, List<RoleData>> roles = new HashMap<UserRole, List<RoleData>>(5);
		
		// DATA VALUES
		
		// Value for company level
		RoleData companyLevelValue = new RoleData();
		companyLevelValue.companyCode = companyCode;
		// Value for company level notification
		RoleData companyNotificationLevelValue = new RoleData();
		companyNotificationLevelValue.companyCode = companyCode;
		companyNotificationLevelValue.notificationId = RoleData.ALL;
		// Value for facility level
		RoleData facilityLevelValue = new RoleData();
		facilityLevelValue.companyCode = companyCode;
		facilityLevelValue.facilityId = RoleData.ALL;
		// Value for facility level notification
		RoleData facilityNotificationLevelValue = new RoleData();
		facilityNotificationLevelValue.companyCode = companyCode;
		facilityNotificationLevelValue.facilityId = RoleData.ALL;
		facilityNotificationLevelValue.notificationId = RoleData.ALL;
		
		// DATA VALUES LISTS
		
		// Data for company level
		List<RoleData> companyLevelValues = Arrays.asList(companyLevelValue);
		// Data for company level notification
		List<RoleData> companyNotificationLevelValues = Arrays.asList(companyNotificationLevelValue);
		// Data for facility level
		List<RoleData> facilityLevelValues = Arrays.asList(facilityLevelValue);
		// Data for facility level notification
		List<RoleData> facilityNotificationLevelValues = Arrays.asList(facilityNotificationLevelValue);
		// Data for both facility and company level
		List<RoleData> bothLevelValues = Arrays.asList(companyLevelValue, facilityLevelValue);
		
		// PERMISSIONS
		
		// Permission to manage the user of the company
		roles.put(UserRole.USER_ADMIN, companyLevelValues);
		// Permission to manage the user of the company
		roles.put(UserRole.INVITATION_ADMIN, companyLevelValues);
		// Permission to manage the company
		roles.put(UserRole.COMPANY_ADMIN, companyLevelValues);
		// Permissions to manage notification under the company
		roles.put(UserRole.COMPANY_NOTIFICATION_ADMIN, companyNotificationLevelValues);
		// Permission to manage all facilities		
		roles.put(UserRole.FACILITY_ADMIN, facilityLevelValues);
		// Permission to manage notifications of a facility
		roles.put(UserRole.FACILITY_NOTIFICATION_ADMIN, facilityNotificationLevelValues);
		// Permissions to manage beacons under the company
		roles.put(UserRole.BEACON_ADMIN, bothLevelValues);
		
		theSecUser.setRoles(roles);
		
		return theSecUser;
	}

	/**
	 * Verify the user data
	 * @param registredUser
	 */
	private void verifyUser(UserRegisterInvitation registredUser) {
		ActionException errors = new ActionException();
		
		if(!StringUtils.isAlphanumeric(registredUser.username)
				|| registredUser.username.length() < 3
				|| registredUser.username.length() > 12)
		{
			errors.addError(ErrorCode.User_InvalidUsername);
		}
		
		// Verify the first name
		if(registredUser.firstName.isEmpty()
				|| !StringUtils.isAsciiPrintable(registredUser.firstName))
		{
			errors.addError(ErrorCode.User_InvalidFirstName);
		}
		
		// Verify the last name
		if(registredUser.lastName.isEmpty()
				|| !StringUtils.isAsciiPrintable(registredUser.lastName))
		{
			errors.addError(ErrorCode.User_InvalidLastName);
		}
		
		// Verify email
		if(!EmailValidator.getInstance().isValid(registredUser.email)) {
			errors.addError(ErrorCode.User_InvalidEmail);
		}
		
		// Verify password format
		if(!StringUtils.isAsciiPrintable(registredUser.password)
				|| registredUser.password.length() < 5
				|| registredUser.password.length() > 20)
		{
			errors.addError(ErrorCode.User_InvalidPassword);
		}
		// Verify that passwords are equals
		if(!registredUser.password.equals(registredUser.verifiedPassword)) {
			errors.addError(ErrorCode.User_PasswordsDoNotMatch);
		}
		
		if(errors.hasError()) {
			throw errors;
		}
		
		SecurityUserDao userSecDao = SecurityUserDaoImpl.getInstance();
		// Verify if the username is already taken
		if(userSecDao.doesUserExist(registredUser.username)) {
			throw new ActionException(ErrorCode.User_UsernameAlreadyExists);
		}
		// Verify if the email is already used
		if(userSecDao.doesEmailExist(registredUser.username)) {
			throw new ActionException(ErrorCode.User_EmailAlreadyExists);
		}
	}
	
}
