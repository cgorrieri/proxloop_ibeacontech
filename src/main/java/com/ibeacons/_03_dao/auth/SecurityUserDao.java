package com.ibeacons._03_dao.auth;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.auth.DomSecurityUser;

/**
 * Dao to manage Users
 * @author Cyril Gorrieri
 *
 */
public interface SecurityUserDao {

	/**
	 * Get all Companies
	 * @return
	 */
	List<DomSecurityUser> getUsers();
	
	/**
	 * Get user of a given company
	 * @return
	 */
	List<DomSecurityUser> getUsers(String companyCode);
	
	/**
	 * Get a user by its code
	 * @param username
	 * @return
	 * @throws NotFoundException 
	 */
	DomSecurityUser getUser(String username) throws NotFoundException;
	
	/**
	 * Get a user by its code
	 * @param username
	 * @return
	 * @throws NotFoundException 
	 */
	DomSecurityUser getFacebookUser(String facebookId) throws NotFoundException;
	
	/**
	 * Check if a user exists or not
	 * @param username
	 * @return <code>true</code> if the user exists, <code>false</code> otherwise
	 */
	boolean doesUserExist(String username);
	
	/**
	 * Check if a user already use this email
	 * @param email
	 * @return
	 */
	boolean doesEmailExist(String email);
	
	/**
	 * Insert a new user
	 * @param user
	 * @return
	 */
	DomSecurityUser insertUser(DomSecurityUser user);
	
	/**
	 * Update a user
	 * @param user
	 * @return
	 * @throws NotFoundException 
	 */
	DomSecurityUser updateUser(DomSecurityUser user);
	
	/**
	 * Delete a user
	 * @param username
	 * @return <code>true</code> if the user is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteUser(String username) throws NotFoundException;
}
