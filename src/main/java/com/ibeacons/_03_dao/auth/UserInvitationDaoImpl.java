package com.ibeacons._03_dao.auth;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.auth.UserInvitation;

public class UserInvitationDaoImpl implements UserInvitationDao {
	private static final Class<UserInvitation> OBJ_CLASS = UserInvitation.class;
	
	private static UserInvitationDaoImpl instance;

	public static UserInvitationDaoImpl getInstance() {
		if(instance == null) {
			instance = new UserInvitationDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private UserInvitationDaoImpl() { }
	
	@Override
	public List<UserInvitation> getList(String companyCode) {
		return ofy().load().type(OBJ_CLASS).filter("companyCode", companyCode).list();
	}

	@Override
	public UserInvitation get(String code) throws NotFoundException {
		return ofy().load().type(OBJ_CLASS).id(code).safe();
	}
	
	@Override
	public boolean doesExist(String code) {
		return ObjectifyDAO.objectExists(code, OBJ_CLASS);
	}
	
	@Override
	public boolean doesEmailExist(String email) {
		return ofy().load().type(OBJ_CLASS).filter("email", email).first().now() != null;
	}

	@Override
	public UserInvitation insert(UserInvitation user) {
		Key<UserInvitation> key = ofy().save().entity(user).now();
		return ofy().load().type(OBJ_CLASS).id(key.getName()).now();
	}

	@Override
	public UserInvitation update(UserInvitation user) {
		return insert(user);
	}

	@Override
	public boolean delete(String username) throws NotFoundException {
		ofy().delete().type(OBJ_CLASS).id(username).now();
		return true;
	}
}
