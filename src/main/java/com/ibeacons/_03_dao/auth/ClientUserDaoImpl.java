package com.ibeacons._03_dao.auth;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.user_data.DomClientUser;

public class ClientUserDaoImpl implements ClientUserDao {
	
	private static ClientUserDaoImpl instance;

	public static ClientUserDaoImpl getInstance() {
		if(instance == null) {
			instance = new ClientUserDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private ClientUserDaoImpl() { }

	@Override
	public List<DomClientUser> getUsers() {
		return ofy().load().type(DomClientUser.class).list();
	}
	
	@Override
	public DomClientUser getUser(String username) throws NotFoundException {
		return ofy().load().type(DomClientUser.class).id(username).safe();
	}
	
	@Override
	public boolean doesUserExist(String username) {
		return ObjectifyDAO.objectExists(username, DomClientUser.class);
	}

	@Override
	public DomClientUser insertUser(DomClientUser user) {
		Key<DomClientUser> key = ofy().save().entity(user).now();
		return ofy().load().type(DomClientUser.class).id(key.getName()).now();
	}

	@Override
	public DomClientUser updateUser(DomClientUser user) {
		return insertUser(user);
	}

	@Override
	public boolean deleteUser(String username) throws NotFoundException {
		ofy().delete().type(DomClientUser.class).id(username).now();
		return true;
	}
}
