package com.ibeacons._03_dao.auth;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.user_data.DomManagementUser;

/**
 * Dao to manage Users
 * @author Cyril Gorrieri
 *
 */
public interface ManagementUserDao {

	/**
	 * Get all Companies
	 * @return
	 */
	List<DomManagementUser> getUsers();
	
	/**
	 * Get a user by its code
	 * @param email Google email of the user
	 * @return
	 * @throws NotFoundException 
	 */
	DomManagementUser getUser(String email) throws NotFoundException;
	
	/**
	 * Check if a user exists or not
	 * @param email Google email of the user
	 * @return <code>true</code> if the user exists, <code>false</code> otherwise
	 */
	boolean doesUserExist(String email);
	
	/**
	 * Insert a new user
	 * @param user
	 * @return
	 */
	DomManagementUser insertUser(DomManagementUser user);
	
	/**
	 * Update a user
	 * @param user
	 * @return
	 * @throws NotFoundException 
	 */
	DomManagementUser updateUser(DomManagementUser user);
	
	/**
	 * Delete a user
	 * @param email Google email of the user
	 * @return <code>true</code> if the user is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteUser(String email) throws NotFoundException;
}
