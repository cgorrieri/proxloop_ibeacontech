package com.ibeacons._03_dao.auth;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.user_data.DomClientUser;

/**
 * Dao to manage Users
 * @author Cyril Gorrieri
 *
 */
public interface ClientUserDao {

	/**
	 * Get all Companies
	 * @return
	 */
	List<DomClientUser> getUsers();
	
	/**
	 * Get a user by its code
	 * @param username
	 * @return
	 * @throws NotFoundException 
	 */
	DomClientUser getUser(String username) throws NotFoundException;
	
	/**
	 * Check if a user exists or not
	 * @param username
	 * @return <code>true</code> if the user exists, <code>false</code> otherwise
	 */
	boolean doesUserExist(String username);
	
	/**
	 * Insert a new user
	 * @param user
	 * @return
	 */
	DomClientUser insertUser(DomClientUser user);
	
	/**
	 * Update a user
	 * @param user
	 * @return
	 * @throws NotFoundException 
	 */
	DomClientUser updateUser(DomClientUser user);
	
	/**
	 * Delete a user
	 * @param username
	 * @return <code>true</code> if the user is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteUser(String username) throws NotFoundException;
}
