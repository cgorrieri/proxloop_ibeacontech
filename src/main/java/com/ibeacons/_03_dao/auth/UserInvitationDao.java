package com.ibeacons._03_dao.auth;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.auth.UserInvitation;

/**
 * Dao to manage Users
 * @author Cyril Gorrieri
 *
 */
public interface UserInvitationDao {
	
	/**
	 * Get all invitation performed in the company
	 * @param companyCode
	 * @return
	 */
	List<UserInvitation> getList(String companyCode);
	
	/**
	 * Get an invitation by its code
	 * @param code Code of the invitation
	 * @return
	 * @throws NotFoundException 
	 */
	UserInvitation get(String code) throws NotFoundException;
	
	/**
	 * Check if an invitation exists or not
	 * @param code
	 * @return <code>true</code> if the invitation exists, <code>false</code> otherwise
	 */
	boolean doesExist(String code);
	
	/**
	 * Check if an invitation already exists with the given email
	 * @param email
	 * @return <code>true</code> if the invitation exists, <code>false</code> otherwise
	 */
	boolean doesEmailExist(String email);
	
	/**
	 * Insert a new invitation
	 * @param invitation
	 * @return
	 */
	UserInvitation insert(UserInvitation invitation);
	
	/**
	 * Update an invitation
	 * @param invitation
	 * @return
	 * @throws NotFoundException 
	 */
	UserInvitation update(UserInvitation invitation);
	
	/**
	 * Delete an invitation
	 * @param code Code of the invitation
	 * @return <code>true</code> if the invitation is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean delete(String code) throws NotFoundException;
}
