package com.ibeacons._03_dao.auth;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.user_data.DomManagementUser;

public class ManagementUserDaoImpl implements ManagementUserDao {
	
	private static ManagementUserDaoImpl instance;

	public static ManagementUserDaoImpl getInstance() {
		if(instance == null) {
			instance = new ManagementUserDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private ManagementUserDaoImpl() { }

	@Override
	public List<DomManagementUser> getUsers() {
		return ofy().load().type(DomManagementUser.class).list();
	}
	
	@Override
	public DomManagementUser getUser(String username) throws NotFoundException {
		return ofy().load().type(DomManagementUser.class).id(username).safe();
	}
	
	@Override
	public boolean doesUserExist(String username) {
		return ObjectifyDAO.objectExists(username, DomManagementUser.class);
	}

	@Override
	public DomManagementUser insertUser(DomManagementUser user) {
		Key<DomManagementUser> key = ofy().save().entity(user).now();
		return ofy().load().type(DomManagementUser.class).id(key.getName()).now();
	}

	@Override
	public DomManagementUser updateUser(DomManagementUser user) {
		return insertUser(user);
	}

	@Override
	public boolean deleteUser(String username) throws NotFoundException {
		ofy().delete().type(DomManagementUser.class).id(username).now();
		return true;
	}
}
