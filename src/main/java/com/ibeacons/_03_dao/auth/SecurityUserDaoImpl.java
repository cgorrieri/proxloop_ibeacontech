package com.ibeacons._03_dao.auth;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.auth.DomSecurityUser;

public class SecurityUserDaoImpl implements SecurityUserDao {
	
	private static final Class<DomSecurityUser> ENTITY_CLASS = DomSecurityUser.class;
	
	private static SecurityUserDaoImpl instance;

	public static SecurityUserDaoImpl getInstance() {
		if(instance == null) {
			instance = new SecurityUserDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private SecurityUserDaoImpl() { }

	@Override
	public List<DomSecurityUser> getUsers() {
		return ofy().load().type(ENTITY_CLASS).list();
	}
	
	@Override
	public List<DomSecurityUser> getUsers(String companyCode) {
		return ofy().load().type(ENTITY_CLASS)
				.filter("companyCode", companyCode).list();
	}
	
	@Override
	public DomSecurityUser getUser(String username) throws NotFoundException {
		return ofy().load().type(ENTITY_CLASS).id(username).safe();
	}
	
	@Override
	public DomSecurityUser getFacebookUser(String facebookId) throws NotFoundException {
		return ofy().load().type(ENTITY_CLASS)
				.filter("facebookId", facebookId)
				.first().safe();
	}
	
	@Override
	public boolean doesUserExist(String username) {
		return ObjectifyDAO.objectExists(username, ENTITY_CLASS);
	}
	
	@Override
	public boolean doesEmailExist(String email) {
		return ofy().load().type(ENTITY_CLASS).filter("email", email).first().now() != null;
	}

	@Override
	public DomSecurityUser insertUser(DomSecurityUser user) {
		Key<DomSecurityUser> key = ofy().save().entity(user).now();
		return ofy().load().type(ENTITY_CLASS).id(key.getName()).now();
	}

	@Override
	public DomSecurityUser updateUser(DomSecurityUser user) {
		return insertUser(user);
	}

	@Override
	public boolean deleteUser(String username) throws NotFoundException {
		ofy().delete().type(ENTITY_CLASS).id(username).now();
		return true;
	}
}
