package com.ibeacons._03_dao;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.ibeacons.dom.DomAward;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomBeaconNotificationsAssignment;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.auth.DomSecurityUser;
import com.ibeacons.dom.auth.UserInvitation;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.dom.statistics.DomBeaconAccess;
import com.ibeacons.dom.statistics.DomCompactedBeaconAccess;
import com.ibeacons.dom.user_data.DomClientUser;
import com.ibeacons.dom.user_data.DomManagementUser;

/**
 * DAO to interact with Objectify
 * 
 * @author Cyrius
 */
public class ObjectifyDAO
{
    static {
    	// TODO register all @Entity classes
    	
    	// Object management
    	ObjectifyService.register(DomCompany.class);
    	ObjectifyService.register(DomFacility.class);
    	ObjectifyService.register(DomBeacon.class);
    	ObjectifyService.register(DomBeaconNotificationsAssignment.class);
    	ObjectifyService.register(DomNotification.class);
    	ObjectifyService.register(DomAward.class);
    	
    	// Authentication
    	ObjectifyService.register(DomManagementUser.class);
    	ObjectifyService.register(DomClientUser.class);
    	ObjectifyService.register(DomSecurityUser.class);
    	ObjectifyService.register(UserInvitation.class);
    	
    	// Statistics
    	ObjectifyService.register(DomBeaconAccess.class);
    	ObjectifyService.register(DomCompactedBeaconAccess.class);
    }
    
    /**
     * Proxy for Objectify
     * @return Objectify proxy
     */
    public static Objectify ofy() { return ObjectifyService.ofy(); }
    
    public static boolean objectExists(String id, Class<?> objectClass) {
    	return ofy().load().filterKey(Key.create(objectClass, id)).keys().first().now() != null;
	}
    
    public static boolean objectExists(Long id, Class<?> objectClass) {
    	return ofy().load().filterKey(Key.create(objectClass, id)).keys().first().now() != null;
	}
}