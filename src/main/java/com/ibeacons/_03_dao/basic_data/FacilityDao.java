package com.ibeacons._03_dao.basic_data;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.DomFacility;

/**
 * Dao to manage Facilities
 * @author Cyril Gorrieri
 *
 */
public interface FacilityDao {

	/**
	 * Get all Facilities
	 * @return
	 */
	List<DomFacility> getFacilities();
	
	/**
	 * Get all facilities of a company
	 * @param companyCode
	 * @return
	 */
	List<DomFacility> getFacilities(String companyCode);

	/**
	 * Get restricted list of facilities of a company
	 * @param companyCode
	 * @param facilityIds List of restricted facilityId
	 * @return
	 */
	List<DomFacility> getFacilities(String companyCode, List<Long> facilityIds);
	
	/**
	 * Get a Facility by its id
	 * @param code
	 * @return
	 */
	DomFacility getFacility(Long id) throws NotFoundException;
	
	/**
	 * Check if a facility exists or not
	 * @param id Unique id representing the facility
	 * @return <code>true</code> if the facility exists, <code>false</code> otherwise
	 */
	boolean doesFacilityExist(Long id);
	
	/**
	 * Insert a new Facility
	 * @param facility
	 * @return
	 */
	DomFacility insertFacility(DomFacility facility);
	
	/**
	 * Update a Facility
	 * @param facility
	 * @return
	 */
	DomFacility updateFacility(DomFacility facility) throws NotFoundException;
	
	/**
	 * Delete a Facility
	 */
	boolean deleteFacility(Long id) throws NotFoundException;
}
