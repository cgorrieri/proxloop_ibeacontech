package com.ibeacons._03_dao.basic_data;

import com.ibeacons.dom.DomBeaconNotificationsAssignment;

public interface BeaconNotificationsAssignmentDao {

	DomBeaconNotificationsAssignment get(Long id);

	boolean insert(DomBeaconNotificationsAssignment notification);

	boolean update(DomBeaconNotificationsAssignment notification);

	boolean delete(Long id);

	boolean doesItemExist(Long id);

}
