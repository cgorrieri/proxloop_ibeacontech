package com.ibeacons._03_dao.basic_data;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.DomAward;

/**
 * Dao to manage Awards
 * @author Cyril Gorrieri
 *
 */
public interface AwardDao {

	/**
	 * Get all Awards
	 * @return
	 */
	List<DomAward> getAwards();
	
	/**
	 * Get all award of a company
	 * @param companyCode
	 * @return
	 */
	List<DomAward> getAwards(String companyCode);
	
	/**
	 * Get a award by its code
	 * @param id Unique id representing the award
	 * @return
	 * @throws NotFoundException 
	 */
	DomAward getAward(long id) throws NotFoundException;
	
	/**
	 * Check if a award exists or not
	 * @param id Unique id representing the award
	 * @return <code>true</code> if the award exists, <code>false</code> otherwise
	 */
	boolean doesAwardExist(long id);
	
	/**
	 * Insert a new award
	 * @param award
	 * @return
	 */
	DomAward insertAward(DomAward award);
	
	/**
	 * Update a award
	 * @param award
	 * @return
	 * @throws NotFoundException 
	 */
	DomAward updateAward(DomAward award);
	
	/**
	 * Delete a award
	 * @param id Unique id representing the award
	 * @return <code>true</code> if the award is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteAward(long id) throws NotFoundException;
}
