package com.ibeacons._03_dao.basic_data;

import java.util.List;

import com.ibeacons.dom.notifications.DomNotification;

public interface NotificationDao {

	DomNotification getNotification(Long id);

	List<DomNotification> getNotifications();
	
	List<DomNotification> getNotifications(String companyCode);
	
	/**
	 * Get all the notifications associated to a facility
	 * @param companyCode
	 * @param facilityId
	 * @return
	 */
	List<DomNotification> getNotifications(String companyCode, Long facilityId);

	DomNotification insertNotification(DomNotification notification);

	void updateNotification(DomNotification notification);

	boolean deleteNotification(Long id);

	boolean doesNotificationExist(Long id);

}
