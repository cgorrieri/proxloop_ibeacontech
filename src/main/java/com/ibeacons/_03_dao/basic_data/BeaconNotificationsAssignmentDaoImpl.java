package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import com.googlecode.objectify.Key;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomBeaconNotificationsAssignment;

public class BeaconNotificationsAssignmentDaoImpl implements BeaconNotificationsAssignmentDao {
	
	private static BeaconNotificationsAssignmentDaoImpl instance;
	
	public static BeaconNotificationsAssignmentDao getInstance() {
		if(instance == null) {
			instance = new BeaconNotificationsAssignmentDaoImpl();
		}
		return instance;
	}

	// private constructor for singleton
	private BeaconNotificationsAssignmentDaoImpl() {	}

	@Override
	public DomBeaconNotificationsAssignment get(Long id) {
		return ofy().load().type(DomBeaconNotificationsAssignment.class).id(id).safe();
	}

	@Override
	public boolean insert(DomBeaconNotificationsAssignment notification) {
		Key<DomBeaconNotificationsAssignment> key = ofy().save().entity(notification).now();
		return key != null;
	}

	@Override
	public boolean update(DomBeaconNotificationsAssignment notification) {
		Key<DomBeaconNotificationsAssignment> key = ofy().save().entity(notification).now();
		return key != null;
	}

	@Override
	public boolean delete(Long id) {
		ofy().delete().type(DomBeaconNotificationsAssignment.class).id(id).now();
		return true;
	}

	@Override
	public boolean doesItemExist(Long id) {
		return ObjectifyDAO.objectExists(id, DomBeaconNotificationsAssignment.class);
	}
	
}
