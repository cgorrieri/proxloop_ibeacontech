package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.notifications.DomNotification;

public class NotificationDaoImpl implements NotificationDao {
	
	private static NotificationDaoImpl instance;
	
	public static NotificationDao getInstance() {
		if(instance == null) {
			instance = new NotificationDaoImpl();
		}
		return instance;
	}

	// private constructor for singleton
	private NotificationDaoImpl() {	}

	@Override
	public DomNotification getNotification(Long id) {
		return ofy().load().type(DomNotification.class).id(id).safe();
	}

	@Override
	public List<DomNotification> getNotifications() {
		return ofy().load().type(DomNotification.class).list();
	}
	
	@Override
	public List<DomNotification> getNotifications(String companyCode) {
		Key<DomCompany> key = Key.create(DomCompany.class, companyCode);
		return ofy().load().type(DomNotification.class).filter("company", key).list();
	}
	
	@Override
	public List<DomNotification> getNotifications(String companyCode, Long facilityId) {
		Key<DomCompany> keyCompany = Key.create(DomCompany.class, companyCode);
		Key<DomFacility> keyFacility = Key.create(DomFacility.class, facilityId);
		return ofy().load().type(DomNotification.class)
				.filter("company", keyCompany)
				.filter("facility", keyFacility)
				.list();
	}

	@Override
	public DomNotification insertNotification(DomNotification notification) {
		Key<DomNotification> key = ofy().save().entity(notification).now();
		return ofy().load().type(DomNotification.class).id(key.getId()).now();
	}

	@Override
	public void updateNotification(DomNotification notification) {
		ofy().save().entity(notification).now();
	}

	@Override
	public boolean deleteNotification(Long id) {
		ofy().delete().type(DomNotification.class).id(id).now();
		return true;
	}

	@Override
	public boolean doesNotificationExist(Long id) {
		return ObjectifyDAO.objectExists(id, DomNotification.class);
	}
	
}
