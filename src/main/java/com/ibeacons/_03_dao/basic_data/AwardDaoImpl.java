package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomAward;
import com.ibeacons.dom.DomCompany;

public class AwardDaoImpl implements AwardDao {
	
	private static AwardDaoImpl instance;

	public static AwardDaoImpl getInstance() {
		if(instance == null) {
			instance = new AwardDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private AwardDaoImpl() { }

	@Override
	public List<DomAward> getAwards() {
		return ofy().load().type(DomAward.class).list();
	}
	
	@Override
	public List<DomAward> getAwards(String companyCode) {
		Key<DomCompany> key = Key.create(DomCompany.class, companyCode);
		return ofy().load().type(DomAward.class)
				.filter("company", key).list();
	}
	
	@Override
	public DomAward getAward(long id) throws NotFoundException {
		return ofy().load().type(DomAward.class).id(id).safe();
	}
	
	@Override
	public boolean doesAwardExist(long id) {
		return ObjectifyDAO.objectExists(id, DomAward.class);
	}

	@Override
	public DomAward insertAward(DomAward award) {
		Key<DomAward> key = ofy().save().entity(award).now();
		return ofy().load().type(DomAward.class).id(key.getId()).now();
	}

	@Override
	public DomAward updateAward(DomAward award) {
		return insertAward(award);
	}

	@Override
	public boolean deleteAward(long id) throws NotFoundException {
		ofy().delete().type(DomAward.class).id(id).now();
		return true;
	}
}
