package com.ibeacons._03_dao.basic_data;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.DomCompany;

/**
 * Dao to manage Companies
 * @author Cyril Gorrieri
 *
 */
public interface CompanyDao {

	/**
	 * Get all Companies
	 * @return
	 */
	List<DomCompany> getCompanies();
	
	// TODO use document to search by name
	
	/**
	 * Get a company by its code
	 * @param code Unique code representing the company
	 * @return
	 * @throws NotFoundException 
	 */
	DomCompany getCompany(String code) throws NotFoundException;
	
	/**
	 * Check if a company exists or not
	 * @param code Unique code representing the company
	 * @return <code>true</code> if the company exists, <code>false</code> otherwise
	 */
	boolean doesCompanyExist(String code);
	
	/**
	 * Insert a new company
	 * @param company
	 * @return
	 */
	DomCompany insertCompany(DomCompany company);
	
	/**
	 * Update a company
	 * @param company
	 * @return
	 * @throws NotFoundException 
	 */
	DomCompany updateCompany(DomCompany company);
	
	/**
	 * Delete a company
	 * @param code Unique code representing the company
	 * @return <code>true</code> if the company is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteCompany(String code) throws NotFoundException;
}
