package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.googlecode.objectify.cmd.Query;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.pom.PomBeaconSearchParameters;

public class BeaconDaoImpl implements BeaconDao {
	
	private static BeaconDaoImpl instance;

	public static BeaconDaoImpl getInstance() {
		if(instance == null) {
			instance = new BeaconDaoImpl();
		}
		return instance;
	}

	@Override
	public List<DomBeacon> getAllBeacons(PomBeaconSearchParameters params) {
		// TODO add filter according to search parameter
		Query<DomBeacon> query = ofy().load().type(DomBeacon.class).distinct(false);
		
		// Filter on company code
		if(params.companyCode != null) {
			Key<DomCompany> key = Key.create(DomCompany.class, params.companyCode);
			query = query.filter("company",key);
		}
		if(params.facilityId != null) {
			Key<DomFacility> key = Key.create(DomFacility.class, params.facilityId);
			query = query.filter("facility", key);
		}
		
		return query.list();
	}

	@Override
	public DomBeacon getBeacon(Long id) throws NotFoundException {
		return ofy().load().type(DomBeacon.class).id(id).safe();
	}
	
	@Override
	public DomBeacon getBeaconByMac(String mac) throws NotFoundException {
		return ofy().load().type(DomBeacon.class)
				.filter("mac", mac)
				.first().safe();
	}

	@Override
	public DomBeacon getBeacon(String uuid, int major, int minor) throws NotFoundException {
		return ofy().load().type(DomBeacon.class)
				.filter("uuid", uuid)
				.filter("minor", minor)
				.filter("major", major)
				.first().safe();
	}

	@Override
	public DomBeacon insertBeacon(DomBeacon beacon) {
		Key<DomBeacon> key = ofy().save().entity(beacon).now();
		return ofy().load().type(DomBeacon.class).id(key.getId()).now();
	}

	@Override
	public DomBeacon updateBeacon(DomBeacon beacon) {
		Key<DomBeacon> key = ofy().save().entity(beacon).now();
		return ofy().load().type(DomBeacon.class).id(key.getId()).now();
	}

	@Override
	public boolean removeBeacon(Long id) {
		ofy().delete().type(DomBeacon.class).id(id).now();
		return true;
	}

	@Override
	public boolean doesBeaconExist(Long id) {
		return ObjectifyDAO.objectExists(id, DomBeacon.class);
	}
}
