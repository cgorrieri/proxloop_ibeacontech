package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomCompany;

public class CompanyDaoImpl implements CompanyDao {
	
	private static CompanyDaoImpl instance;

	public static CompanyDaoImpl getInstance() {
		if(instance == null) {
			instance = new CompanyDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private CompanyDaoImpl() { }

	@Override
	public List<DomCompany> getCompanies() {
		return ofy().load().type(DomCompany.class).list();
	}
	
	@Override
	public DomCompany getCompany(String code) throws NotFoundException {
		return ofy().load().type(DomCompany.class).id(code).safe();
	}
	
	@Override
	public boolean doesCompanyExist(String code) {
		return ObjectifyDAO.objectExists(code, DomCompany.class);
	}

	@Override
	public DomCompany insertCompany(DomCompany company) {
		Key<DomCompany> key = ofy().save().entity(company).now();
		return ofy().load().type(DomCompany.class).id(key.getName()).now();
	}

	@Override
	public DomCompany updateCompany(DomCompany company) {
		return insertCompany(company);
	}

	@Override
	public boolean deleteCompany(String code) throws NotFoundException {
		ofy().delete().type(DomCompany.class).id(code).now();
		return true;
	}
}
