package com.ibeacons._03_dao.basic_data;

import java.util.List;

import com.ibeacons.dom.DomBeacon;
import com.ibeacons.pom.PomBeaconSearchParameters;

public interface BeaconDao {

	/**
	 * Get all beacons according to search parameter
	 * @return
	 */
	List<DomBeacon> getAllBeacons(PomBeaconSearchParameters params);
	
	/**
	 * Get a beacon according to its id
	 * @param id
	 * @return
	 */
	DomBeacon getBeacon(Long id);
	
	/**
	 * Get a beacon according to its MAC address
	 * @param mac
	 * @return
	 */
	DomBeacon getBeaconByMac(String mac);
	
	/** 
	 * Get a beacon according to its broadcasted informations
	 * @param uuid
	 * @param major
	 * @param minor
	 * @return
	 */
	DomBeacon getBeacon(String uuid, int major, int minor);

	/**
	 * Add a new beacon in the database
	 * @param beacon
	 * @return
	 */
	DomBeacon insertBeacon(DomBeacon beacon);

	/**
	 * Update a beacon
	 * @param beacon
	 * @return
	 */
	DomBeacon updateBeacon(DomBeacon beacon);
	
	/**
	 * Remove a beacon according to its MAC address
	 * @param id
	 * @return
	 */
	boolean removeBeacon(Long id);
	
	/**
	 * Test if a beacon exist or not
	 * @param id
	 * @return True if the beacon exists, false otherwise
	 */
	boolean doesBeaconExist(Long id);
}
