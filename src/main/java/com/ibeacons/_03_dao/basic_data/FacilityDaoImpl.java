package com.ibeacons._03_dao.basic_data;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.NotFoundException;
import com.ibeacons._03_dao.ObjectifyDAO;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;

public class FacilityDaoImpl implements FacilityDao {
	
	private static FacilityDaoImpl instance;

	public static FacilityDaoImpl getInstance() {
		if(instance == null) {
			instance = new FacilityDaoImpl();
		}
		return instance;
	}
	
	// private constructor for singleton
	private FacilityDaoImpl() {	}

	@Override
	public List<DomFacility> getFacilities() {
		return ofy().load().type(DomFacility.class).list();
	}
	
	@Override
	public List<DomFacility> getFacilities(String companyCode) {
		Key<DomCompany> key = Key.create(DomCompany.class, companyCode);
		return ofy().load().type(DomFacility.class)
				.filter("company", key)
				.list();
	}
	
	@Override
	public List<DomFacility> getFacilities(String companyCode, List<Long> facilityIds) {
		Key<DomCompany> key = Key.create(DomCompany.class, companyCode);
		return ofy().load().type(DomFacility.class)
				.filter("company", key)
				.filter("facility IN", facilityIds)
				.list();
	}
	
	@Override
	public DomFacility getFacility(Long id) throws NotFoundException {
		return ofy().load().type(DomFacility.class).id(id).safe();
	}
	
	@Override
	public boolean doesFacilityExist(Long id) {
		return ObjectifyDAO.objectExists(id, DomFacility.class);
	}

	@Override
	public DomFacility insertFacility(DomFacility facility) {
		Key<DomFacility> key = ofy().save().entity(facility).now();
		return ofy().load().type(DomFacility.class).id(key.getId()).now();
	}

	@Override
	public DomFacility updateFacility(DomFacility facility) {
		Key<DomFacility> key = ofy().save().entity(facility).now();
		return ofy().load().type(DomFacility.class).id(key.getId()).now();
	}

	@Override
	public boolean deleteFacility(Long id) {
		ofy().delete().type(DomFacility.class).id(id).now();
		return true;
	}
}
