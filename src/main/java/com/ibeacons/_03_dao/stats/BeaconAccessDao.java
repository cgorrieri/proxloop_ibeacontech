package com.ibeacons._03_dao.stats;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.statistics.DomBeaconAccess;

/**
 * Dao to manage beacon access history
 * @author Cyril Gorrieri
 *
 */
public interface BeaconAccessDao {

	/**
	 * Get all beacon access
	 * @return
	 */
	List<DomBeaconAccess> getList();
	
	/**
	 * Get all beacon access for a beacon after a given time
	 * @param beaconId
	 * @param timestamp
	 * @return
	 */
	List<DomBeaconAccess> getList(Long beaconId, Long timestamp);
	
	/**
	 * Get all beacon access before a given time
	 * @param timestamp
	 * @return
	 */
	List<DomBeaconAccess> getListBefore(Long timestamp);
	
	/**
	 * Insert a new beacon access
	 * @param beaconAccess
	 */
	void insertObject(DomBeaconAccess beaconAccess);
	
	/**
	 * Delete a beacon access
	 * @param id
	 * @return <code>true</code> if the beacon access is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteObject(Long id) throws NotFoundException;

	/**
	 * Delete a set of objects id
	 * @param ids
	 * @return
	 * @throws NotFoundException
	 */
	boolean deleteObjects(List<Long> ids) throws NotFoundException;
}
