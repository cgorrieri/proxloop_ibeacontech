package com.ibeacons._03_dao.stats;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.statistics.DomCompactedBeaconAccess;

public class CompactedBeaconAccessDaoImpl implements CompactedBeaconAccessDao {
	
	private static CompactedBeaconAccessDaoImpl instance;

	public static CompactedBeaconAccessDaoImpl getInstance() {
		if(instance == null) {
			instance = new CompactedBeaconAccessDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private CompactedBeaconAccessDaoImpl() { }

	@Override
	public List<DomCompactedBeaconAccess> getList() {
		return ofy().load().type(DomCompactedBeaconAccess.class).list();
	}
	
	@Override
	public DomCompactedBeaconAccess get(Long beaconId) {
		return ofy().load().type(DomCompactedBeaconAccess.class).id(beaconId).now();
	}
	
	@Override
	public void insertObject(DomCompactedBeaconAccess beaconAccess) {
		ofy().save().entity(beaconAccess).now();
	}
	
	@Override
	public void updateObject(DomCompactedBeaconAccess beaconAccess) {
		insertObject(beaconAccess);
	}

	@Override
	public boolean deleteObject(Long id) throws NotFoundException {
		ofy().delete().type(DomCompactedBeaconAccess.class).id(id).now();
		return true;
	}
}
