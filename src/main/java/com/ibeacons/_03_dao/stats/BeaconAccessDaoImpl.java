package com.ibeacons._03_dao.stats;

import static com.ibeacons._03_dao.ObjectifyDAO.ofy;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.statistics.DomBeaconAccess;

public class BeaconAccessDaoImpl implements BeaconAccessDao {
	
	private static BeaconAccessDaoImpl instance;

	public static BeaconAccessDaoImpl getInstance() {
		if(instance == null) {
			instance = new BeaconAccessDaoImpl();
		}
		return instance;
	}
	
	// Private constructor for singleton
	private BeaconAccessDaoImpl() { }

	@Override
	public List<DomBeaconAccess> getList() {
		return ofy().load().type(DomBeaconAccess.class).list();
	}
	
	@Override
	public List<DomBeaconAccess> getListBefore(Long timestamp) {
		return ofy().load().type(DomBeaconAccess.class)
				.filter("timestamp <", timestamp)
				.list();
	}
	

	@Override
	public List<DomBeaconAccess> getList(Long beaconId, Long timestamp) {
		return ofy().load().type(DomBeaconAccess.class)
				.filter("beaconId", beaconId)
				.filter("timestamp >=", timestamp)
				.list();
	}

	@Override
	public void insertObject(DomBeaconAccess beaconAccess) {
		ofy().save().entity(beaconAccess).now();
	}

	@Override
	public boolean deleteObject(Long id) throws NotFoundException {
		ofy().delete().type(DomBeaconAccess.class).id(id).now();
		return true;
	}
	
	@Override
	public boolean deleteObjects(List<Long> ids) throws NotFoundException {
		ofy().delete().type(DomBeaconAccess.class).ids(ids).now();
		return true;
	}
}
