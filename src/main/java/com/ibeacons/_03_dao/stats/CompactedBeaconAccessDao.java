package com.ibeacons._03_dao.stats;

import java.util.List;

import com.googlecode.objectify.NotFoundException;
import com.ibeacons.dom.statistics.DomCompactedBeaconAccess;

/**
 * Dao to manage compacted beacon access history
 * @author Cyril Gorrieri
 *
 */
public interface CompactedBeaconAccessDao {

	/**
	 * Get all beacon access
	 * @return
	 */
	List<DomCompactedBeaconAccess> getList();
	
	/**
	 * Get a compacted access for a beacon
	 * @param beaconId Id of the beacon
	 * @return
	 */
	DomCompactedBeaconAccess get(Long beaconId);
	
	/**
	 * Insert a new beacon access
	 * @param beaconAccess
	 */
	void insertObject(DomCompactedBeaconAccess beaconAccess);
	
	/**
	 * Update a beacon access
	 * @param beaconAccess
	 */
	void updateObject(DomCompactedBeaconAccess beaconAccess);
	
	/**
	 * Delete a beacon access
	 * @param id
	 * @return <code>true</code> if the beacon access is deleted <code>false</code> otherwise
	 * @throws NotFoundException 
	 */
	boolean deleteObject(Long id) throws NotFoundException;
}
