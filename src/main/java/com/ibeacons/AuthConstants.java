package com.ibeacons;

public class AuthConstants {
	public static final String WEB_CLIENT_ID = "300823059214-q9iq43jn54dbklktamuu77g8val29ams.apps.googleusercontent.com";
	public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
}