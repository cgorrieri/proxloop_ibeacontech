package com.ibeacons.adapters;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.googlecode.objectify.Ref;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.pom.PomBeacon;
import com.ibeacons.tools.CompanyHelper;
import com.ibeacons.tools.FacilityHelper;

@Component
public class PomBeaconAdapter extends PomAdapter<PomBeacon, DomBeacon>{
	private static Logger LOGGER = Logger.getLogger(PomBeaconAdapter.class.getCanonicalName());
	
	@Override
	public PomBeacon toPom(DomBeacon dom) {
		LOGGER.log(Level.FINE, "toPom: " + dom);
		// Get the company id
		String companyId = dom.getCompany().getKey().getName();
		
		PomBeacon pom = new PomBeacon(dom.getId(), companyId, null, dom.getMac(), dom.getUuid(), dom.getMajor(), dom.getMinor(), dom.getName(),
				dom.getBatteryLevel(), dom.getEstimatedEndOfBatteryDate().toDateTime().getMillis(), dom.getBroadcastingPower(), dom.getTxThreshold());
		
		// Get the facility Id
		if(dom.getFacility() != null) {
			DomFacility facility = dom.getFacility().get();
			if(facility != null) {
				pom.facilityId = facility == null ? null : facility.getId();
				pom.facilityName = facility.getName();
			}
		}
		
		LOGGER.log(Level.FINE, "toPom: result -> " + pom);
		
		return pom;
	}
	
	@Override
	public DomBeacon toDom(PomBeacon pom) {
		LOGGER.log(Level.FINE, "toDom: " + pom);
		
		Ref<DomCompany> companyRef = null;
		if(pom.companyId != null)
			companyRef = CompanyHelper.getCompanyRef(pom.companyId);
		Ref<DomFacility> facilityRef = null;
		if(pom.facilityId != null)
			facilityRef = FacilityHelper.getFacilityRef(pom.facilityId);
		// Build with mandatory fields
		DomBeacon dom = new DomBeacon(pom.id, companyRef, facilityRef, pom.mac, pom.uuid, pom.major, pom.name);
		// Optional fields
		if(pom.minor != null) dom.setMinor(pom.minor);
		if(pom.batteryLevel != null) dom.setBatteryLevel(pom.batteryLevel.byteValue());
		if(pom.estimatedOutOfBatteryTime != null) dom.setEstimatedEndOfBatteryDate(pom.estimatedOutOfBatteryTime);
		if(pom.broadcastingPower != null) dom.setBroadcastingPower(pom.broadcastingPower);
		if(pom.txThreshold != null) dom.setTxThreshold(pom.txThreshold);
		
		LOGGER.log(Level.FINE, "toDom: result -> " + dom);
		
		return dom;
	}
}
