package com.ibeacons.adapters;

import org.springframework.stereotype.Component;

import com.ibeacons.dom.DomCompany;
import com.ibeacons.pom.PomCompany;

@Component
public class PomCompanyAdapter extends PomAdapter<PomCompany, DomCompany>{
	@Override
	public PomCompany toPom(DomCompany dom) {
		PomCompany pom = new PomCompany(dom.getCode(), dom.getName());
		pom.logoUrl = dom.getLogoUrl();
		return pom;
	}
	
	@Override
	public DomCompany toDom(PomCompany pom) {
		DomCompany dom = new DomCompany(pom.code, pom.name);
		dom.setLogoUrl(pom.logoUrl);
		return dom;
	}
}
