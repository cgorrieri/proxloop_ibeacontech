package com.ibeacons.adapters;

import com.ibeacons.dom.DomAddress;
import com.ibeacons.pom.PomAddress;

public class PomAddressAdapter extends PomAdapter<PomAddress, DomAddress>{
	@Override
	public PomAddress toPom(DomAddress dom) {
		return new PomAddress(dom.streetAddress, dom.city, dom.postalCode, dom.country, dom.longitude, dom.latitude);
	}
	
	@Override
	public DomAddress toDom(PomAddress pom) {
		return new DomAddress(pom.streetAddress, pom.city, pom.postalCode, pom.country, pom.longitude, pom.latitude);
	}
}
