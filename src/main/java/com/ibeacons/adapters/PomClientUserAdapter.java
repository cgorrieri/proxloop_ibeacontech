package com.ibeacons.adapters;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.NotImplementedException;

import com.ibeacons.dom.user_data.DomClientUser;
import com.ibeacons.pom.auth.PomClientUser;

public class PomClientUserAdapter extends PomAdapter<PomClientUser, DomClientUser> {
	private static Logger LOGGER = Logger.getLogger(PomClientUserAdapter.class.getCanonicalName());
	
	@Override
	public PomClientUser toPom(DomClientUser dom) {
		LOGGER.log(Level.INFO, "toPom: " + dom);
		PomClientUser pom = new PomClientUser();
		pom.username = dom.getUsername();
		pom.json = dom.getJsonContent();
		pom.lastName = dom.getSercurityUser().getLastName();
		pom.firstName = dom.getSercurityUser().getFirstName();
		pom.email = dom.getSercurityUser().getEmail();
		LOGGER.log(Level.INFO, "toPom: result -> " + pom);
		return pom;
	}
	
	@Override
	public DomClientUser toDom(PomClientUser pom) {
		throw new NotImplementedException("PomClientUserAdapter.toDom");
	}
}
