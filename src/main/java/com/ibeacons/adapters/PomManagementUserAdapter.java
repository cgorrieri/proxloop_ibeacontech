package com.ibeacons.adapters;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

import com.ibeacons.dom.user_data.DomManagementUser;
import com.ibeacons.pom.auth.PomManagementUser;

@Component
public class PomManagementUserAdapter extends PomAdapter<PomManagementUser, DomManagementUser>{
	@Override
	public PomManagementUser toPom(DomManagementUser dom) {
		PomManagementUser pom = new PomManagementUser();
		pom.username = dom.getUsername();
		pom.lastName = dom.getSercurityUser().getLastName();
		pom.firstName = dom.getSercurityUser().getFirstName();
		pom.companyCode = dom.getCompanyCode();
		return pom;
	}
	
	@Override
	public DomManagementUser toDom(PomManagementUser pom) {
		throw new NotImplementedException("PomManagementUserAdapter.toDom");
	}
}
