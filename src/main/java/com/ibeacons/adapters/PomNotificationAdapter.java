package com.ibeacons.adapters;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.googlecode.objectify.Ref;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.dom.notifications.DomTemplateImplement;
import com.ibeacons.dom.notifications.DomTemplateImplementValue;
import com.ibeacons.pom.notifications.PomNotification;
import com.ibeacons.pom.notifications.PomTemplateImplement;
import com.ibeacons.pom.notifications.PomTemplateImplementValue;
import com.ibeacons.servlets.NotificationServlet;
import com.ibeacons.tools.CompanyHelper;
import com.ibeacons.tools.EnvHelper;
import com.ibeacons.tools.FacilityHelper;

@Component
public class PomNotificationAdapter extends PomAdapter<PomNotification, DomNotification> {
	private static Logger LOGGER = Logger.getLogger(PomNotificationAdapter.class.getCanonicalName());
	
	@Override
	public PomNotification toPom(DomNotification dom) {
		LOGGER.log(Level.INFO, "toPom: " + dom);
		PomNotification pom = new PomNotification(dom.getId(), dom.getCompany().getKey().getName(), dom.getName(), dom.getTitle(), dom.getShortDescription());
		// If a facility is set
		if(dom.getFacility() != null) {
			DomFacility facility = dom.getFacility().get();
			if(facility != null) {
				pom.facilityId = facility.getId();
				pom.facilityName = facility.getName();
			}
		}
		// Set notification type
		if(dom.getNotificationType() != null) {
			switch(dom.getNotificationType()) {
			case TemplateBasedWebView:
				pom.notificationType = PomNotification.NotificationType.TemplateBasedWebView;
				break;
			case CustomWebUrl:
				pom.notificationType = PomNotification.NotificationType.CustomWebUrl;
				break;
			case Imbedded:
				pom.notificationType = PomNotification.NotificationType.Imbedded;
			}
		}
		// if the web view is given
		if(dom.getTemplateImplement() != null) {
			pom.templateImplement = toPom(dom.getTemplateImplement());
			pom.templateImplement.displayUrl = EnvHelper.getRootUrl() + NotificationServlet.getUrlFor(pom.id);
		}
		pom.customWebUrl = dom.getCustomWebUrl();
		pom.subTitle = dom.getSubTitle();
		LOGGER.log(Level.INFO, "toPom: result ->" + pom);
		return pom;
	}
	
	@Override
	public DomNotification toDom(PomNotification pom) {
		LOGGER.log(Level.INFO, "toDom: " + pom);
		Ref<DomCompany> company = CompanyHelper.getCompanyRef(pom.companyCode);
		DomNotification dom = new DomNotification(pom.id, company, pom.name, pom.title, pom.shortDescription);
		if(pom.facilityId != null) {
			dom.setFacility(FacilityHelper.getFacilityRef(pom.facilityId));
		}
		// Set notification type
		if(pom.notificationType != null)
			switch(pom.notificationType) {
			case TemplateBasedWebView:
				dom.setNotificationType(DomNotification.NotificationType.TemplateBasedWebView);
				break;
			case CustomWebUrl:
				dom.setNotificationType(DomNotification.NotificationType.CustomWebUrl);
				break;
			case Imbedded:
				dom.setNotificationType(DomNotification.NotificationType.Imbedded);
				break;
			}
		// if the web view is given
		if(pom.templateImplement != null) {
			dom.setTemplateImplement(toDom(pom.templateImplement));
		}
		dom.setCustomWebUrl(pom.customWebUrl);
		dom.setSubTitle(pom.subTitle);
		LOGGER.log(Level.INFO, "toDom: result ->" + dom);
		return dom;
	}
	
	private DomTemplateImplement toDom(PomTemplateImplement pom) {
		DomTemplateImplement dom = new DomTemplateImplement(pom.templateId);
		for(PomTemplateImplementValue val : pom.values) {
			dom.values.add(new DomTemplateImplementValue(val.name, val.value));
		}
		return dom;
	}
	
	private PomTemplateImplement toPom(DomTemplateImplement dom) {
		PomTemplateImplement pom = new PomTemplateImplement(dom.templateId);
		for(DomTemplateImplementValue val : dom.values) {
			pom.values.add(new PomTemplateImplementValue(val.name, val.value));
		}
		return pom;
	}
}
