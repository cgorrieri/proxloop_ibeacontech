package com.ibeacons.adapters;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.ibeacons.dom.DomFacility;
import com.ibeacons.pom.PomAddress;
import com.ibeacons.pom.PomFacility;
import com.ibeacons.tools.CompanyHelper;

@Component
public class PomFacilityAdapter extends PomAdapter<PomFacility, DomFacility> {
	private static Logger LOGGER = Logger.getLogger(PomFacilityAdapter.class.getCanonicalName());
	private PomAddressAdapter addressAdapter = new PomAddressAdapter();
	
	@Override
	public PomFacility toPom(DomFacility dom) {
		LOGGER.log(Level.INFO, "toPom: " + dom);
		PomAddress address = null;
		if(dom.getAddress() != null)
			address = addressAdapter.toPom(dom.getAddress());
		String companyCode = dom.getCompany().getKey().getName();
		return new PomFacility(dom.getId(), dom.getName(), companyCode, dom.getDescription(), dom.getImageUrl(), dom.getCategories(), address);
	}
	
	@Override
	public DomFacility toDom(PomFacility pom) {
		LOGGER.log(Level.INFO, "toDom: " + pom);
		DomFacility dom = new DomFacility();
		dom.setId(pom.id);
		dom.setName(pom.name);
		dom.setCompany(CompanyHelper.getCompanyRef(pom.companyCode));
		dom.setDescription(pom.description);
		dom.setImageUrl(pom.imageUrl);
		dom.setCategories(pom.categories);
		if(pom.physicalAddress != null);
			dom.setAddress(addressAdapter.toDom(pom.physicalAddress));
		return dom;
	}
}
