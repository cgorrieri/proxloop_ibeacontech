package com.ibeacons.adapters;

import java.util.ArrayList;
import java.util.List;

public abstract class PomAdapter<Pom, Dom> {
	public abstract Pom toPom(Dom dom);
	
	public abstract Dom toDom(Pom pom);
	
	public List<Pom> toPoms(List<Dom> doms) {
		List<Pom> poms = new ArrayList<Pom>(doms.size());
		for(Dom dom : doms) {
			poms.add(toPom(dom));
		}
		return poms;
	}
	
	public List<Dom> toDoms(List<Pom> poms) {
		List<Dom> doms = new ArrayList<Dom>(poms.size());
		for(Pom pom : poms) {
			doms.add(toDom(pom));
		}
		return doms;
	}
}
