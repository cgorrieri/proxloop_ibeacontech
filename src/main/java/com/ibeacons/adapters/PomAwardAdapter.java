package com.ibeacons.adapters;

import com.ibeacons.dom.DomAward;
import com.ibeacons.pom.PomAward;

public class PomAwardAdapter extends PomAdapter<PomAward, DomAward>{
	@Override
	public PomAward toPom(DomAward dom) {
		return new PomAward(dom.id, dom.title, dom.description, dom.imageUrl, dom.loops, dom.vendor);
	}
	
	@Override
	public DomAward toDom(PomAward pom) {
		return new DomAward(pom.id, pom.title, pom.description, pom.imageUrl, pom.loops, pom.vendor);
	}
}
