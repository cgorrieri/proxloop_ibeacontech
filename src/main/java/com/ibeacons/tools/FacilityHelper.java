package com.ibeacons.tools;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.ibeacons._03_dao.basic_data.FacilityDao;
import com.ibeacons._03_dao.basic_data.FacilityDaoImpl;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;

public class FacilityHelper {
	private static FacilityDao facilityDao = FacilityDaoImpl.getInstance();
	
	public static Ref<DomFacility> getFacilityRef(Long id) {
		return Ref.create(Key.create(DomFacility.class, id));
	}
	
	/**
	 * Check if the facility exists.
	 * The code must be checked before by calling checkCode
	 * @param code
	 * @throws ActionException with code Facility_ItemDoesNotExist
	 */
	public static void checkFacilityExists(Long id) throws ActionException {
		if(!facilityDao.doesFacilityExist(id)){
			throw new ActionException(ErrorCode.Facility_ItemDoesNotExist);
		}
	}
}
