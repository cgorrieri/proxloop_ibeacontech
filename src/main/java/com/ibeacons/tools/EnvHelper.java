package com.ibeacons.tools;

import com.google.apphosting.api.ApiProxy;
import com.google.apphosting.api.ApiProxy.Environment;

public class EnvHelper {
	/**
	 * Get the root url of the application
	 * @return
	 */
	public static String getRootUrl() {
		Environment env = ApiProxy.getCurrentEnvironment();
		return "http://" + (String) env.getAttributes().get("com.google.appengine.runtime.default_version_hostname");
	}
	
	/**
	 * Get the secured root url of the application
	 * @return
	 */
	public static String getSecuredRootUrl() {
		Environment env = ApiProxy.getCurrentEnvironment();
		return "https://" + (String) env.getAttributes().get("com.google.appengine.runtime.default_version_hostname");
	}
}
