package com.ibeacons.tools;

import org.springframework.beans.factory.annotation.Value;

public class PropertyHolder {

    @Value("${buckets.pictures}") private String PICTURES_BUCKET;
    public String getPicturesBucket() {
    	return PICTURES_BUCKET;
    }
}
