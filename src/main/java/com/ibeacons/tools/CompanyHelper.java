package com.ibeacons.tools;

import org.apache.commons.lang3.RandomStringUtils;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.ibeacons._03_dao.basic_data.CompanyDao;
import com.ibeacons._03_dao.basic_data.CompanyDaoImpl;
import com.ibeacons.dom.DomCompany;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;

public class CompanyHelper {
	private static CompanyDao companyDao = CompanyDaoImpl.getInstance();
	
	public static Ref<DomCompany> getCompanyRef(String code) {
		return Ref.create(Key.create(DomCompany.class, code));
	}
	
	public static String generateValidCode() {
		String code = "";
		do {
			code = RandomStringUtils.randomAlphabetic(5).toUpperCase();
		} while (companyDao.doesCompanyExist(code));
		return code;
	}
	
	public static void checkCompany(String companyCode) throws ActionException {
		checkCode(companyCode);
		checkCompanyExists(companyCode);
	}
	
	/**
	 * Check the company code
	 * @param code
	 * @throws ActionException with code Company_InvalidCode
	 */
	public static void checkCode(String code) throws ActionException {
		if(! Validator.validateCode(code)) throw new ActionException(ErrorCode.Company_InvalidCode);
	}
	
	/**
	 * Check if the company exists.
	 * The code must be checked before by calling checkCode
	 * @param code
	 * @throws ActionException with code Company_ItemDoesNotExist
	 */
	public static void checkCompanyExists(String code) throws ActionException {
		if(!companyDao.doesCompanyExist(code)) throw new ActionException(ErrorCode.Company_ItemDoesNotExist);
	}
}
