package com.ibeacons.tools;


public class UserHelper {
	private static final String ALL_PERMISSION_WILDCARD = "*";
	private static final String PERMISSION_SEPARATOR = "/";
	private static final int COMPANY_LEVEL = 0;
	private static final int FACILITY_LEVEL = 1;
	
//	public static User checkUser(User user) throws InternalServerErrorException {
//		if (user == null) throw new InternalServerErrorException("User is Not Valid");
//		return user;
//	}
//
//	/**
//	 * The email must be a google one
//	 * @param email
//	 */
//	public static void checkEmail(String email) {
//		/*if(!StringUtils.endsWith(email, "@gmail.com")) {
//			throw new ActionException(ErrorCode.User_InvalidEmail);
//		}*/
//	}
//
//	
//	public static void checkUserExists(String email) {
//		if(!UserDaoImpl.getInstance().doesUserExist(email)) {
//			throw new ActionException(ErrorCode.User_ItemDoesNotExist);
//		}
//	}
//
//	/**
//	 * Check the permission for company level
//	 * @param email
//	 * @param permissionName
//	 */
//	public static void checkPermission(String email, String permissionName) {
//		try {
//			DomUser theUser = UserDaoImpl.getInstance().getUser(email);
//			if(!theUser.getPermissions().containsKey(permissionName)) {
//				throw new ActionException(ErrorCode.User_NotAuthorized);
//			}
//		} catch (Exception e) {
//			throw new ActionException(ErrorCode.User_NotAuthorized);
//		}
//	}
//	
//	/**
//	 * Check the permission for company level
//	 * @param email
//	 * @param companyCode Code of the company
//	 * @param permissionName
//	 */
//	public static void checkPermission(String email, String permissionName, String companyCode) {
//		try {
//			DomUser user = UserDaoImpl.getInstance().getUser(email);
//			List<String> permissionApplications = user.getPermissions().get(permissionName);
//			// If the permission is found we check that it is allowed for the given code
//			if(permissionApplications != null) {
//				for(String application : permissionApplications) {
//					if(ALL_PERMISSION_WILDCARD.equals(application)
//							|| companyCode.equals(application)) {
//						return;
//					}
//				}
//			}
//			throw new ActionException(ErrorCode.User_NotAuthorized);
//		} catch (Exception e) {
//			throw new ActionException(ErrorCode.User_NotAuthorized);
//		}
//	}
//
//	/**
//	 * Check the permission for company level
//	 * @param email
//	 * @param companyCode Code of the company
//	 * @param facilityCode Code of the facility
//	 * @param permissionName
//	 */
//	public static void checkPermission(String email, String permissionName, String companyCode, String facilityCode) {
//		try {
//			DomUser user = UserDaoImpl.getInstance().getUser(email);
//			List<String> permissionApplications = user.getPermissions().get(permissionName);
//			// If the permission is found we check that it is allowed for the given code
//			if(permissionApplications != null) {
//				for(String application : permissionApplications) {
//					String[] applicationParts = application.split(PERMISSION_SEPARATOR);
//					if(applicationParts.length == 2 
//						&& (ALL_PERMISSION_WILDCARD.equals(applicationParts[COMPANY_LEVEL])
//								|| companyCode.equals(applicationParts[COMPANY_LEVEL]))
//						&& (ALL_PERMISSION_WILDCARD.equals(applicationParts[FACILITY_LEVEL])
//								|| facilityCode.equals(applicationParts[FACILITY_LEVEL]))) {
//						return;
//					}
//				}
//			}
//			throw new ActionException(ErrorCode.User_NotAuthorized);
//		} catch (Exception e) {
//			throw new ActionException(ErrorCode.User_NotAuthorized);
//		}
//	}
}
