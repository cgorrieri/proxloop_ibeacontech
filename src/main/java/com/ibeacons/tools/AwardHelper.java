package com.ibeacons.tools;

import com.ibeacons._03_dao.basic_data.AwardDao;
import com.ibeacons._03_dao.basic_data.AwardDaoImpl;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.exceptions.ErrorCode;

public class AwardHelper {
	private static AwardDao awardDao = AwardDaoImpl.getInstance();
	
	/**
	 * Check if the company exists.
	 * The code must be checked before by calling checkCode
	 * @param id
	 * @throws ActionException with code Award_ItemDoesNotExist
	 */
	public static void checkAwardExists(long id) throws ActionException {
		if(!awardDao.doesAwardExist(id)) throw new ActionException(ErrorCode.Award_ItemDoesNotExist);
	}
}
