package com.ibeacons.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * Object meant to collect errors
 * @author Cyril Gorrieri
 *
 */
public class ErrorsBuilder {
	List<String> errors = new ArrayList<String>();
	
	/**
	 * Add an error to the list
	 * @param message
	 */
	void addError(String message) {
		errors.add(message);
	}
	
	/**
	 * Get all error messages into one string
	 * @return
	 */
	String getMessages() {
		return errors.toString();
	}
}
