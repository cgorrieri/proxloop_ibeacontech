package com.ibeacons.tools;

import com.ibeacons.dom.DomAddress;

public class AddressTools {
	private static final String _separator = ", ";
	
	public static String stringifyAddress(DomAddress address) {
		return address.streetAddress + _separator
			+ address.postalCode + _separator
			+ address.city + _separator
			+ address.country;
	}
}
