package com.ibeacons.tools;


import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;

import com.ibeacons.dom.DomBeacon;

/**
 * Tools for Beacons object
 * @author Cyrius
 *
 */
public class BeaconTools {
	
	// January 1 2014
	public static final DateTime startDate = new DateTime(2014, 1, 1, 0, 0, DateTimeZone.getDefault());

	/**
	 * Get the date encrypted in the minor field
	 * @param minor
	 * @return The date encrypted
	 */
	public static DateTime getDateInMinor(int minor) {
		return startDate.plusDays(minor);
	}
	
	/**
	 * Get the integer to put in the minor field for the specified date
	 * @param date
	 * @return Interger to put in the minor field
	 */
	public static short dateToMinor(DateTime date) {
		return (short) Days.daysBetween(startDate, date).getDays();
		// Other way : if any problem with the selected solution,  try this one
		//return Days.daysBetween(startDate.withTimeAtStartOfDay(), date.withTimeAtStartOfDay()).getDays();
	}

	/**
	 * Determine the end date of the battery life
	 * @param beacon The beacon with all parameters
	 */
	public static void estimateEndOfBatteryDate(DomBeacon beacon) {
		// TODO implement the method
		throw new UnsupportedOperationException("Not implemented yet");
	}

	/**
	 * Estimate the next refresh date according to the estimated end date of battery life
	 * @param beacon
	 */
	public static void estimateNextRefreshDate(DomBeacon beacon) {
		Days days = Days.daysBetween(DateTime.now(), beacon.getEstimatedEndOfBatteryDate());
		
		// Establish ratio
		int ratio = 2;
		if		(days.getDays() > 100) ratio = 10;
		else if	(days.getDays() > 50) ratio = 7;
		else if (days.getDays() > 10) ratio = 4;
		days = days.dividedBy(ratio);
		
		// Set new date
		DateTime estimatedRefreshDate = DateTime.now().plus(days);
		beacon.setMinor(dateToMinor(estimatedRefreshDate));
	}
}
