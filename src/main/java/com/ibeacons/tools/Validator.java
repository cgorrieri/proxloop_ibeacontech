package com.ibeacons.tools;

import org.apache.commons.lang3.StringUtils;

public final class Validator {
	
	public static boolean validatePositiveNumber(long number) {
		return number > 0;
	}
	
	/**
	 * Returns if a string is a validate Code
	 * @param code
	 * @return <code>true</code> if the code is valid, <code>false</code> otherwise
	 */
	public static boolean validateCode(String code) {
		return code.length() <= 5 && StringUtils.isAlphanumeric(code);
	}
	
	/**
	 * Returns if a string is a validate alphanumeric text with space of size <b>length</b>
	 * @param text
	 * @param length
	 * @return <code>true</code> if the text is valid, <code>false</code> otherwise
	 */
	public static boolean validateText(String text, int length) {
		return text != null && !text.isEmpty() && text.length() <= length && StringUtils.isAlphanumericSpace(text);
	}
	
	
}
