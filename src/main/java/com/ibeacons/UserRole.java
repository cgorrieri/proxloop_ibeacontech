	package com.ibeacons;

import java.util.Arrays;
import java.util.List;

/**
 * All basic roles of the users and the mapping to their permissions
 * @author Cyril Gorrieri
 *
 */
public enum UserRole {
	// Company data
	COMPANY_VIEWER			(Arrays.asList(Permission.RETRIEVE_COMPANY)),
	
	COMPANY_EDITOR			(Arrays.asList(Permission.MANAGE_COMPANY,
										   Permission.RETRIEVE_COMPANY)),
										   
	COMPANY_ADMIN			(Arrays.asList(Permission.MANAGE_COMPANY,
										   Permission.RETRIEVE_COMPANY)),

	// Notification at company level data
	COMPANY_NOTIFICATION_VIEWER		(Arrays.asList(Permission.RETRIEVE_COMPANY_NOTIFICATION)),
	
	COMPANY_NOTIFICATION_EDITOR		(Arrays.asList(Permission.RETRIEVE_COMPANY_NOTIFICATION,
												   Permission.MANAGE_COMPANY_NOTIFICATION)),
												   
	COMPANY_NOTIFICATION_ADMIN		(Arrays.asList(Permission.CREATE_COMPANY_NOTIFICATION,
												   Permission.RETRIEVE_COMPANY_NOTIFICATION,
		   										   Permission.MANAGE_COMPANY_NOTIFICATION,
												   Permission.DELETE_COMPANY_NOTIFICATION)),
	
	// Facility Data
	FACILITY_VIEWER			(Arrays.asList(Permission.RETRIEVE_FACILITY)),
	
	FACILITY_EDITOR			(Arrays.asList(Permission.MANAGE_FACILITY,
			   							   Permission.RETRIEVE_FACILITY)),
			   							   
	FACILITY_ADMIN			(Arrays.asList(Permission.CREATE_FACILITY,
										   Permission.MANAGE_FACILITY,
			                               Permission.RETRIEVE_FACILITY,
			   							   Permission.DELETE_FACILITY)),
	
	// Notification at facility level
	FACILITY_NOTIFICATION_VIEWER	(Arrays.asList(Permission.RETRIEVE_FACILITY_NOTIFICATION)),
	
	FACILITY_NOTIFICATION_EDITOR	(Arrays.asList(Permission.RETRIEVE_FACILITY_NOTIFICATION,
												   Permission.MANAGE_FACILITY_NOTIFICATION)),
												   
	FACILITY_NOTIFICATION_ADMIN		(Arrays.asList(Permission.RETRIEVE_FACILITY_NOTIFICATION,
			   									   Permission.CREATE_FACILITY_NOTIFICATION,
		   										   Permission.MANAGE_FACILITY_NOTIFICATION,
												   Permission.DELETE_FACILITY_NOTIFICATION)),
	
    // User management
	USER_ADMIN				(Arrays.asList(Permission.MANAGE_USER,
			   							   Permission.RETRIEVE_USERS,
			   							   Permission.RETRIEVE_USER,
										   Permission.DELETE_USER)),
										   
    // User management
	INVITATION_ADMIN				(Arrays.asList(Permission.MANAGE_INVITATION,
			   							   Permission.RETRIEVE_INVITATIONS,
			   							   Permission.RETRIEVE_INVITATION,
			   							   Permission.CREATE_INVITATION_NEW_USER,
			   							   Permission.CREATE_INVITATION_NEW_FACILITY_USER,
										   Permission.DELETE_INVITATION)),
	
	BEACON_DATA_EDITOR		(Arrays.asList(Permission.MANAGE_BEACON_DATA)),
	
	// Beacon admin
	BEACON_ADMIN			(Arrays.asList(Permission.CREATE_COMPANY_BEACON,Permission.MANAGE_COMPANY_BEACON,
			   							   Permission.DELETE_COMPANY_BEACON,
										   Permission.RETRIEVE_COMPANY_BEACON,
										   Permission.CREATE_FACILITY_BEACON,Permission.DELETE_FACILITY_BEACON,
										   Permission.RETRIEVE_FACILITY_BEACON,
										   Permission.MANAGE_FACILITY_BEACON));
	
	public List<Permission> permissions;
	
	UserRole(List<Permission> permissions) {
		this.permissions = permissions;
	}
}
