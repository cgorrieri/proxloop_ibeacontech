package com.ibeacons.exceptions;

/**
 * Error codes and description of the application
 * 
 * @author Cyril Gorrieri
 */
public enum ErrorCode {
	//********** Codes **********
	
	// Company
	Company_ItemDoesNotExist			(100, 	"The company does not exist"),
	Company_ItemAlreadyExists			(101, 	"The company already exists"),
	Company_InvalidCode					(102, 	"Invalid company code"),
	Company_InvalidName					(103, 	"Invalid company name"),
	Company_FacilitiesStillExist		(104,	"Facilities still exist under this company"),
	Company_BeaconsStillExist			(105,	"Beacons still exist under this company"),
	Company_NotificationsStillExist		(106,	"Notifications still exist under this company"),
	
	// Facility
	Facility_DataInconsistency			(200, 	"Data inconsitency in facility"),
	Facility_ItemDoesNotExist			(201, 	"The facility does not exist"),
	Facility_ItemAlreadyExists			(202, 	"The facility already exists"),
	Facility_InvalidName				(204, 	"Invalid name, must be alphanumeric"),
	Facility_NoAddress					(205, 	"No address provided"),
	Facility_InvalidAddress				(206, 	"Invalid address"),
	Facility_InvalidPostalCode			(207, 	"Invalid postal code"),
	Facility_InvalidCity				(208, 	"Invalid city"),
	Facility_InvalidCountry				(209, 	"Invalid country"),
	Facility_BeaconsStillExist			(210,	"Beacons still exist under this facility"),
	Facility_NotificationsStillExist	(211,	"Notifications still exist under this facility"),
	
	// Beacon
	Beacon_EstimatedDatiInThePast		(300, 	"Invalid estimated end of battery life date"),
	Beacon_ItemDoesNotExist				(301, 	"The beacon does not exist"),

	// Notification
	Notification_ItemDoesNotExist		(400, 	"The notification does not exist"),
	Notification_NoIdAtCreation			(401, 	"The Id must not be provided at creation time"),
	Notification_MandatoryName			(402, 	"The name is mandatory"),
	Notification_InvalidName			(403, 	"The name is invalid"),
	Notification_MandatoryTitle			(404, 	"The title is mandatory"),
	Notification_InvalidTitle			(405, 	"The title is invalid"),
	Notification_MandatorySubTitle		(406, 	"The sub title is mandatory"),
	Notification_InvalidSubTitle		(407, 	"The Id must not be provided at creation time"),
	Notification_InvalidCustomUrl		(408, 	"The Url is invalid"),
	Notification_InvalidFacility		(409, 	"The Facility is invalid"),
	
	
	// Award
	Award_ItemDoesNotExist				(500, 	"The award does not exist"),
	Award_ItemAlreadyExists				(501, 	"The award already exists"),
	Award_InvalidTitle					(502, 	"Invalid award title"),
	Award_InvalidDescription			(503, 	"Invalid award description"),
	
	// Pictures
	Picture_UnableToDelete				(600,	"Unable to delete the resource"),
	Picture_UnableToUpload				(601,	"Unable to upload the resource"),
	Picture_FileIsEmpty					(602,   "There is no file to upload"),
	
	// Invitations
	Invitation_EmailAlreadyExists		(700,	"An invitation has already been sent to this email"),
	
	// User
	User_NotAuthorized					(1000, 	"The user is not authorized to perform this action"),
	User_ItemDoesNotExist				(1001, 	"The user does not exist"),
	User_UsernameAlreadyExists			(1002, 	"The username is already taken"),
	User_InvalidUsername 				(1003, 	"The username is invalid"),
	User_InvalidFirstName 				(1004, 	"The first name is invalid"),
	User_InvalidLastName 				(1005, 	"The last name is invalid"),
	User_EmailAlreadyExists				(1006, 	"The email is already used"),
	User_InvalidEmail					(1007,	"Invalid email"),
	User_InvalidPassword				(1008,	"Invalid password"),
	User_PasswordsDoNotMatch			(1009,	"Passwords does not match"),
	User_InvalidFacebookToken			(1010,	"Invalid Facebook token"),
	
	ManagUser_DataNotCreated			(1101, "Data of the user cannot be created"),
	ManagUser_CompanyNotCreated			(1102, "The company of the user cannot be created"),
	
	ClientUser_DataNotCreated			(1201, "Data of the user cannot be created"),
	
	Invitation_ItemDoesNotExist			(1300, "The invitation does not exist, has been used or has expired"),
	
	Unknown								(9999, 	"Unknown error");
	
	
	//********** Functional **********
	private int code;
	private String description = "";
   
	// Constructor
	ErrorCode(int code, String description){
		this.code = code;
		this.description = description;
	}
   
	public int getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public String toString() {
		return this.code + ": " + this.description;
	}
}
