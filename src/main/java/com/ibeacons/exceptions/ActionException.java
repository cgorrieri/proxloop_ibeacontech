package com.ibeacons.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ActionException extends RuntimeException {
	private static final long serialVersionUID = -6258525663073675658L;
	private List<ErrorCode> _errors = new ArrayList<ErrorCode>();
	
	public ActionException(ErrorCode error) {
		_errors.add(error);
	}

	public ActionException() {}

	public List<ErrorCode> getErrors() {
		return _errors;
	}

	public void setErrors(List<ErrorCode> errors) {
		this._errors = errors;
	}
	
	public void addError(ErrorCode error) {
		_errors.add(error);
	}
	
	public boolean hasError() {
		return !_errors.isEmpty();
	}
}
