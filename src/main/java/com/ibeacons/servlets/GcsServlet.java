package com.ibeacons.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.ibeacons._02_services.cloud_storage.CloudStorageService;

public class GcsServlet extends HttpServlet {
	private static final long serialVersionUID = 4924825345334425211L;

	private String bucketName = "proxloop-test";

	/**
	 * Used below to determine the size of chucks to read in. Should be > 1kb
	 * and < 10MB
	 */
	private static final int BUFFER_SIZE = 2 * 1024 * 1024;

//	/**
//	 * Retrieves a file from GCS and returns it in the http response. If the
//	 * request path is /gcs/Foo/Bar this will be interpreted as a request to
//	 * read the GCS file named Bar in the bucket Foo.
//	 */
//	@Override
//	public void doGet(HttpServletRequest req, HttpServletResponse resp)
//			throws IOException {
//		CrossOriginServletHelper.checkOrigin(this, req, resp);
//
//		// Get the code of the file
//		int positionToStart = req.getRequestURI().lastIndexOf('/') + 1;
//		String code = req.getRequestURI().substring(positionToStart);
//		System.out.println("Query");
//		System.out.println(req.getQueryString());
//		System.out.println("loalAddr");
//		System.out.println(req.getLocalAddr());
//		System.out.println("Sevlet path");
//		System.out.println(req.getServletPath());
//
//		// Transform it to GCS filename
//		GcsFilename gcsfileName = new GcsFilename(bucketName, code);
//
//		// Get the file
//		GcsInputChannel readChannel = gcsService.openPrefetchingReadChannel(gcsfileName, 0, BUFFER_SIZE);
//		copy(Channels.newInputStream(readChannel), resp.getOutputStream());
//	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		CrossOriginServletHelper.checkOrigin(this, req, res);
		
		String prefix = "";
		ServletFileUpload upload;
		FileItemIterator iterator;
		FileItemStream item;
		InputStream stream = null;
		try {
			upload = new ServletFileUpload();
			res.setContentType("text/plain");

			iterator = upload.getItemIterator(req);
				
			item = iterator.next();
			stream = item.openStream();
			
			// If it is a form field and the prefix
			if (item.isFormField() && "prefix".equals(item.getFieldName())) {
				prefix = IOUtils.toString(stream);
				
				// We now get the next item
				item = iterator.next();
				stream = item.openStream();
			}
			
			// If the next token is actually the file
			if("file".equals(item.getFieldName())) {
				String name = prefix + item.getName();
				CloudStorageService.uploadStream(name, item.getContentType(), stream, bucketName);

				// Write the name in the output
				PrintWriter output = res.getWriter();
				output.append(name);
			}
		} catch (Exception ex) {
			throw new ServletException(ex);
		}
	}

	private void copy(InputStream input, OutputStream output)
			throws IOException {
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = input.read(buffer);
			while (bytesRead != -1) {
				output.write(buffer, 0, bytesRead);
				bytesRead = input.read(buffer);
			}
		} finally {
			input.close();
			output.close();
		}
	}
}
