package com.ibeacons.servlets;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CrossOriginServletHelper extends HttpServlet {
	private static final long serialVersionUID = -1905056851251699536L;

	/**
	 * Check cross-site calls
	 * 
	 * @param req
	 * @param res
	 */
	public static void checkOrigin(HttpServlet s, HttpServletRequest req, HttpServletResponse res) {
		// List of allowed origins
//		List<String> incomingURLs = Arrays.asList(s.getServletContext()
//				.getInitParameter("incomingURLs").trim().split(","));

		// Get client's origin
//		String clientOrigin = req.getHeader("origin");

		// // Get client's IP address
		// String ipAddress = req.getHeader("x-forwarded-for");
		// if (ipAddress == null) {
		// ipAddress = req.getRemoteAddr();
		// }

//		int myIndex = incomingURLs.indexOf(clientOrigin);
		// if the client origin is found in our list then give access
		// if you don't want to check for origin and want to allow access
		// to all incoming req then change the line to this
		res.setHeader("Access-Control-Allow-Origin", "*");
//		if (myIndex != -1) {
//			res.setHeader("Access-Control-Allow-Origin", clientOrigin);
//			res.setHeader("Access-Control-Allow-Methods", "POST");
//			res.setHeader("Access-Control-Allow-Headers", "Content-Type");
//			res.setHeader("Access-Control-Max-Age", "86400");
//		}
	}
}
