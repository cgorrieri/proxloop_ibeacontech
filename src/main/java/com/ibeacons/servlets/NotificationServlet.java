package com.ibeacons.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class NotificationServlet extends HttpServlet {
	public static final String URL = "/notification";
	
	public static String getUrlFor(Long id) {
		return URL + "?id=" + id;
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		CrossOriginServletHelper.checkOrigin(this, request, response);
		try {
			request.getRequestDispatcher("/WEB-INF/notification.jsp").forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
}
