package com.ibeacons._01_controllers.administration;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.ibeacons.UserRole;
import com.ibeacons._02_services.BeaconsServices;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons.adapters.PomBeaconAdapter;
import com.ibeacons.dom.DomBeacon;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomBeacon;
import com.ibeacons.pom.PomBeaconSearchParameters;

/**
 * Interface that displays all the methods for the administration of the beacons
 * @author Cyril Gorrieri
 *
 */
@Component
public class BeaconCatalogController {
	private static Logger LOGGER = Logger.getLogger(BeaconCatalogController.class.getCanonicalName());
	
	@Inject
	private BeaconsServices beaconService;
	@Inject
	private PomBeaconAdapter beaconAdapter;
	@Inject
	private PermissionService permissionService;
	
	/**
	 * Get a beacon from its id
	 * @param id
	 * @return
	 */
	public PomBeacon getBeacon(Long id) throws ActionException {
		DomBeacon beacon = beaconService.getBeacon(id);
		permissionService.checkRole(UserRole.BEACON_ADMIN);
//		checkPermissionOnBeacon(Permission.RETRIEVE_COMPANY_BEACON, Permission.RETRIEVE_FACILITY_BEACON, beacon);
		return beaconAdapter.toPom(beacon);
	}

	/**
	 * Get all beacons
	 * @return
	 */
	public List<PomBeacon> getBeacons(PomBeaconSearchParameters params) throws ActionException {
		permissionService.checkRole(UserRole.BEACON_ADMIN);
		return beaconAdapter.toPoms(beaconService.getAllBeacons(params));
	}
	
	/**
	 * Add a beacon in the system
	 * @param beacon
	 * @param facilityId
	 * @return
	 */
	public PomBeacon addBeacon(PomBeacon beacon) throws ActionException {
//		if(beacon.facilityId != null) {
//			String facilityId = String.valueOf(beacon.facilityId);
//			permissionService.checkPermissionOnObject(Permission.CREATE_FACILITY_BEACON, 2, beacon.companyId, facilityId);
//		}
//		else {
//			permissionService.checkPermissionOnObject(Permission.CREATE_COMPANY_BEACON, 1, beacon.companyId);
//		}

		permissionService.checkRole(UserRole.BEACON_ADMIN);
		return beaconAdapter.toPom(beaconService.addBeacon(beaconAdapter.toDom(beacon)));
	}
	
	/**
	 * Update the data of a beacon
	 * @param beacon
	 * @return
	 */
	public PomBeacon updateBeacon(PomBeacon beacon) throws ActionException {
		DomBeacon theBeacon = beaconAdapter.toDom(beacon);

		permissionService.checkRole(UserRole.BEACON_ADMIN);
//		checkPermissionOnBeacon(Permission.MANAGE_COMPANY_BEACON, Permission.MANAGE_FACILITY_BEACON, theBeacon);
		return beaconAdapter.toPom(beaconService.updateBeacon(theBeacon));
	}
	
	/**
	 * Delete a beacon according to the MAC address
	 * @param id
	 * @return
	 */
	public PomActionResult deleteBeacon(Long id) throws ActionException {
		DomBeacon beacon = beaconService.getBeacon(id);

		permissionService.checkRole(UserRole.BEACON_ADMIN);
//		checkPermissionOnBeacon(Permission.DELETE_COMPANY_BEACON, Permission.DELETE_FACILITY_BEACON, beacon);
		beaconService.deleteBeacon(id);
		return new PomActionResult(true);
	}
	
//	private void checkPermissionOnBeacon(Permission permissionCompanyBeacon,
//			Permission permissionFacilityBeacon, DomBeacon beacon) {
//		String companyCode = beacon.getCompany().getKey().getName();
//		String beaconId = String.valueOf(beacon.getId());
//		if(beacon.getFacility() != null) {
//			String facilityId = String.valueOf(beacon.getFacility().getKey().getId());
//			permissionService.checkPermissionOnObject(permissionFacilityBeacon, 3, companyCode, facilityId, beaconId);
//		}
//		else {
//			permissionService.checkPermissionOnObject(permissionCompanyBeacon, 2, companyCode, beaconId);
//		}
//	}
}
