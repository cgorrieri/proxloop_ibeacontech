package com.ibeacons._01_controllers.administration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.ibeacons.Permission;
import com.ibeacons._02_services.CompanyServices;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons.adapters.PomCompanyAdapter;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomCompany;

@Component
public class CompanyCatalogController {
	
	@Inject
	private CompanyServices companyServices;
	@Inject
	private PomCompanyAdapter companyAdapter;
	@Inject
	private PermissionService permissionService;
	
	/**
	 * Return a company by its Id
	 * @param code Unique code representing the company
	 * @return the company
	 * @throws ActionException 
	 */
	public PomCompany getCompany(String code) throws ActionException {
		permissionService.checkPermissionOnCompany(Permission.RETRIEVE_COMPANY, code);
		return companyAdapter.toPom(companyServices.getCompany(code));
	}
	
	/**
	 * Get all companies
	 * @return
	 */
	public List<PomCompany> getCompanies() {
		permissionService.hasAuthority("ROLE_ADMIN");
		return companyAdapter.toPoms(companyServices.getCompanies());
	}
	
//	/**
//	 * Add a new company
//	 * @param company
//	 * @return
//	 */
//	public PomCompany addCompany(PomCompany company) throws ActionException {
//		permissionService.checkPermissionOnObject(Permissions.CREATE_COMPANY, 1, company.code);
//		return companyAdapter.toPom(companyServices.addCompany(companyAdapter.toDom(company)));
//	}
	
	/**
	 * Update an existed company
	 * @param company
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomCompany updateCompany(PomCompany company) throws ActionException {
		permissionService.checkPermissionOnCompany(Permission.MANAGE_COMPANY, company.code);
		return companyAdapter.toPom(companyServices.updateCompany(companyAdapter.toDom(company)));
	}
	
	/**
	 * Delete an existed company
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomActionResult deleteCompany(String code) throws ActionException {
		permissionService.checkPermissionOnCompany(Permission.DELETE_COMPANY, code);
		companyServices.deleteCompany(code);
		return new PomActionResult(true);
	}
}
