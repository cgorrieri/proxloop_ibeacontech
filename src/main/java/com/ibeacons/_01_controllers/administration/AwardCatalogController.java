package com.ibeacons._01_controllers.administration;

import java.util.List;

import com.ibeacons._02_services.AwardServices;
import com.ibeacons.adapters.PomAwardAdapter;
import com.ibeacons.dom.user_data.DomManagementUser;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomAward;

public class AwardCatalogController {
	
	private AwardServices awardServices;
	private PomAwardAdapter awardAdapter;
	
	public AwardCatalogController() {
		awardServices = new AwardServices();
		awardAdapter = new PomAwardAdapter();
	}

	/**
	 * Return a award by its Id
	 * @param id Unique id representing the award
	 * @return the award
	 * @throws ActionException 
	 */
	public PomAward getAward(long id) throws ActionException {
//		UserHelper.checkPermission(email, PermissionNames.RETRIEVE_COMPANY, code);
		return awardAdapter.toPom(awardServices.getAward(id));
	}
	
	/**
	 * Get all awards
	 * @return
	 */
	public List<PomAward> getAwards(DomManagementUser user) {
//		UserHelper.checkPermission(email, PermissionNames.RETRIEVE_COMPANY);
		return awardAdapter.toPoms(awardServices.getAwards(user));
	}
	
	/**
	 * Add a new award
	 * @param award
	 * @param email 
	 * @return
	 */
	public PomAward addAward(PomAward award) {
//		UserHelper.checkPermission(email, PermissionNames.MANAGE_COMPANY, award.code);
		return awardAdapter.toPom(awardServices.addAward(awardAdapter.toDom(award)));
	}
	
	/**
	 * Update an existed award
	 * @param award
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomAward updateAward(PomAward award) {
//		UserHelper.checkPermission(email, PermissionNames.MANAGE_COMPANY, award.code);
		return awardAdapter.toPom(awardServices.updateAward(awardAdapter.toDom(award)));
	}
	
	/**
	 * Delete an existed award
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomActionResult deleteAward(long id) {
//		UserHelper.checkPermission(email, PermissionNames.DELETE_COMPANY, code);
		awardServices.deleteAward(id);
		return new PomActionResult(true);
	}
}
