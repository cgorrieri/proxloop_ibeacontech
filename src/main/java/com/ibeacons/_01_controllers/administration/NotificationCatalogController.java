package com.ibeacons._01_controllers.administration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.ibeacons.Permission;
import com.ibeacons._02_services.NotificationServices;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons.adapters.PomNotificationAdapter;
import com.ibeacons.dom.notifications.DomNotification;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.notifications.PomNotification;

@Component
public class NotificationCatalogController {
	@Inject
	private NotificationServices notificationServices;
	@Inject
	private PomNotificationAdapter notificationAdapter;
	@Inject
	private PermissionService permissionService;

	/**
	 * Return a company by its Id
	 * @param code Unique code representing the company
	 * @return the company
	 * @throws ActionException 
	 */
	public PomNotification getNotification(Long id) throws ActionException {
		DomNotification theNotification = notificationServices.getNotification(id);
		checkPermissionOnNotification(Permission.RETRIEVE_COMPANY_NOTIFICATION, Permission.RETRIEVE_FACILITY_NOTIFICATION, theNotification);
		return notificationAdapter.toPom(theNotification);
	}

	/**
	 * Get all notifications
	 * @return
	 */
	public List<PomNotification> getAllNotifications(String companyCode) {
		permissionService.checkPermission(Permission.RETRIEVE_COMPANY_NOTIFICATION);
//		List<String> values = permissionService.getValuesForPermission(UserRole.RETRIEVE_COMPANY_NOTIFICATION);
//		for(String value : values) {
//			String[] tokens = value.split(UserRole.SEPARATOR);
//			if(tokens[0].equals(companyCode) || tokens[0].equals(UserRole.WILDCARD)) {
//				return notificationAdapter.toPoms(notificationServices.getAllNotifications(companyCode));
//			}
//		}
//		permissionService.throwPermissionException();
		return notificationAdapter.toPoms(notificationServices.getAllNotifications(companyCode));
	}
	
	/**
	 * Add a new company
	 * @param notification
	 * @param email 
	 * @return
	 */
	public PomNotification addNotification(PomNotification notification) throws ActionException {
		if(notification.facilityId != null) {
			permissionService.checkPermissionOnCompanyFacility(Permission.CREATE_FACILITY_NOTIFICATION, notification.companyCode, notification.facilityId);
		}
		else {
			permissionService.checkPermissionOnCompany(Permission.CREATE_COMPANY_NOTIFICATION, notification.companyCode);
		}
		return notificationAdapter.toPom(notificationServices.addNotification(notificationAdapter.toDom(notification)));
	}
	
	/**
	 * Update an existed company
	 * @param notification
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomActionResult updateNotification(PomNotification notification) throws ActionException {
		DomNotification theNotification = notificationAdapter.toDom(notification);
		checkPermissionOnNotification(Permission.MANAGE_COMPANY_NOTIFICATION, Permission.MANAGE_FACILITY_NOTIFICATION, theNotification);
		notificationServices.updateNotification(theNotification);
		return new PomActionResult(true);
	}
	
	/**
	 * Delete an existed company
	 * @param id
	 * @return
	 * @throws NoEntityFoundException 
	 */
	public PomActionResult deleteNotification(Long id) throws ActionException {
		DomNotification theNotification = notificationServices.getNotification(id);
		checkPermissionOnNotification(Permission.DELETE_COMPANY_NOTIFICATION, Permission.DELETE_FACILITY_NOTIFICATION, theNotification);
		notificationServices.deleteNotification(id);
		return new PomActionResult(true);
	}
	
	private void checkPermissionOnNotification(
			Permission permissionCompanyNotification, Permission permissionFacilityNotification, DomNotification theNotification) {
		String companyCode = theNotification.getCompany().getKey().getName();
		// If it is a company facility notification level
		if(theNotification.getFacility() != null) {
			Long facilityId = theNotification.getFacility().getKey().getId();
			permissionService.checkPermissionOnCompanyFacilityNotification(permissionFacilityNotification, companyCode, facilityId, theNotification.getId());
		}
		// Otherwise it is a company notification level
		else {
			permissionService.checkPermissionOnCompanyNotificaiton(permissionCompanyNotification, companyCode, theNotification.getId());
		}
	}
}
