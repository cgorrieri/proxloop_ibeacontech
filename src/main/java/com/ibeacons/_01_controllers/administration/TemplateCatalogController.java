package com.ibeacons._01_controllers.administration;

import java.util.ArrayList;
import java.util.List;

import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomTemplate;
import com.ibeacons.pom.PomTemplateAttribute;

/**
 * Interface that displays all the methods for the administration of the beacons
 * @author Cyril Gorrieri
 *
 */
public class TemplateCatalogController {
	
	public static List<PomTemplate> templates = new ArrayList<PomTemplate>(2);
	
	static {
		PomTemplate template1 = new PomTemplate();
		template1.id = (long) 1;
        template1.name = "Basic Template 1";
        template1.htmlContent = "<h2>{{ template.header }}</h2><img width=\"100%\" ng-src=\"{{ template.picture.url }}\"/><p>{{ template.content }}</p>";
        template1.cssContent = ".template h2 {color:red}";
        template1.attributes = new ArrayList<PomTemplateAttribute>(3);
        template1.attributes.add(new PomTemplateAttribute("Header","header","text"));
        template1.attributes.add(new PomTemplateAttribute("Content","content","textarea"));
        template1.attributes.add(new PomTemplateAttribute("Picture","picture","image"));
        templates.add(template1);

        PomTemplate template2 = new PomTemplate();
        template2.id = (long) 2;
        template2.name = "Basic Template 2";
        template2.htmlContent = "<h1>{{ template.title }}</h1><h2>{{ template.subTitle }}</h2><p>{{ template.content }}</p>";
        template2.cssContent = ".template h2 {color:red}";
        template2.attributes = new ArrayList<PomTemplateAttribute>(3);
        template2.attributes.add(new PomTemplateAttribute("Title","title","text"));
        template2.attributes.add(new PomTemplateAttribute("Sub Title","subTitle","text"));
        template2.attributes.add(new PomTemplateAttribute("Content","content","textarea"));
        templates.add(template2);
	}
	
	
	/**
	 * Get all templates
	 * @return
	 */
	public List<PomTemplate> getTemplates() throws ActionException {
		return templates;
	}

}
