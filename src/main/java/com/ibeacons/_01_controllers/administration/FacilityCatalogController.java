package com.ibeacons._01_controllers.administration;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.ibeacons.Permission;
import com.ibeacons._02_services.FacilityServices;
import com.ibeacons._02_services.PermissionService;
import com.ibeacons.adapters.PomFacilityAdapter;
import com.ibeacons.dom.DomFacility;
import com.ibeacons.exceptions.ActionException;
import com.ibeacons.pom.PomActionResult;
import com.ibeacons.pom.PomFacility;

@Component
public class FacilityCatalogController {
	@Inject
	private FacilityServices facilityServices;
	@Inject
	private PomFacilityAdapter facilityAdapter;
	@Inject
	private PermissionService permissionService;
	
	/**
	 * Return a facility by its Id
	 * @param id
	 * @return 
	 */
	public PomFacility getFacility(Long id)  throws ActionException {
		// First retrieve the facility to get the company
		DomFacility theFacility = facilityServices.getFacility(id);
		// Check the permission
		checkPermissionOnFacility(Permission.RETRIEVE_FACILITY, theFacility);
		
		return facilityAdapter.toPom(theFacility);
	}
	
	/**
	 * Get all facilities
	 * @return
	 */
	public List<PomFacility> getFacilities() {
		permissionService.checkAuthority("ROLE_ADMIN");
		return facilityAdapter.toPoms(facilityServices.getFacilities());
	}
	
	/**
	 * Get facilities of a company
	 * @return
	 */
	public List<PomFacility> getFacilities(String companyCode) throws ActionException {
		// No permission checked here, the list of allowed facility to retrieve is created after
		return facilityAdapter.toPoms(facilityServices.getFacilities(companyCode));
	}
	
	/**
	 * Add a new facility
	 * @param facility
	 * @return
	 */
	public PomFacility addFacility(PomFacility facility) throws ActionException {
		permissionService.checkPermissionOnCompany(Permission.CREATE_FACILITY, facility.companyCode);
		return facilityAdapter.toPom(facilityServices.addFacility(facilityAdapter.toDom(facility)));
	}
	
	/**
	 * Update an existed Facility
	 * @param facility
	 * @return
	 */
	public PomFacility updateFacility(PomFacility facility) throws ActionException {
		checkPermissionOnFacility(Permission.MANAGE_FACILITY, facility);
		return facilityAdapter.toPom(facilityServices.updateFacility(facilityAdapter.toDom(facility)));
	}
	
	/**
	 * Delete an existed Facility
	 * @param code
	 * @return
	 */
	public PomActionResult deleteFacility(Long id)  throws ActionException {
		// Retrieve the facility to get the company
		DomFacility theFacility = facilityServices.getFacility(id);
		
		// Check the permission
		checkPermissionOnFacility(Permission.DELETE_FACILITY, theFacility);
		
		facilityServices.deleteFacility(id);
		return new PomActionResult(true);
	}
	
	private void checkPermissionOnFacility(Permission permissionName, DomFacility facility) {
		String companyCode = facility.getCompany().getKey().getName();
		permissionService.checkPermissionOnCompanyFacility(permissionName, companyCode, facility.getId());
	}
	
	private void checkPermissionOnFacility(Permission permissionName, PomFacility facility) {
		String companyCode = facility.companyCode;
		permissionService.checkPermissionOnCompanyFacility(permissionName, companyCode, facility.id);
	}
}
