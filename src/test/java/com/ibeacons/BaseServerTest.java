package com.ibeacons;

import org.junit.After;
import org.junit.Before;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

public class BaseServerTest {
	
	protected static final String EMAIL = "test@gmail.com";

	private static final LocalServiceTestHelper helper = new LocalServiceTestHelper(
			new LocalDatastoreServiceTestConfig()).setEnvIsLoggedIn(true)
			.setEnvAuthDomain("localhost").setEnvEmail(EMAIL);

	@Before
    public void setUp() {
        helper.setUp();
        setUpData();
    }
	
	public void setUpData() {}

    @After
    public void tearDown() {
        helper.tearDown();
    }
}
